package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_AuthUser;
import com.mayora.mypaydrc.model.Obj_VerificationDevice;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 17/08/2017.
 */

public class VerifikasiDevice extends AppCompatActivity {

    private EditText editKode;
    private Button btnSubmit;
    ProgressDialog progress;
    private Obj_VerificationDevice verificationDevice;
    private Toolbar mToolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verifikasi_device);

        InitControl();
    }

    void InitControl(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editKode = (EditText) findViewById(R.id.editKode);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VerifikasiDevice();
            }
        });

    }

    private void VerifikasiDevice(){
        progress = ProgressDialog.show(this, "", "Proses Verifikasi", true);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("updatedevice", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();
        String code = editKode.getText().toString().trim();

        try{
            Obj_VerificationDevice param = new Obj_VerificationDevice(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI, code);
            Call<Obj_VerificationDevice> call = NetworkManager.getNetworkService(this).verifikasiDevice(param);
            call.enqueue(new Callback<Obj_VerificationDevice>() {
                @Override
                public void onResponse(Call<Obj_VerificationDevice> call, Response<Obj_VerificationDevice> response) {
                    int code = response.code();

                    if (code == 200){
                        verificationDevice = response.body();
                        progress.dismiss();

                        if (verificationDevice.rc.equals("00")){
                            Obj_AuthUser objAuthUser = AppController.getInstance().getSessionManager().getUserProfileTemp();
                            String sPin = AppController.getInstance().getSessionManager().getStringData("pin_temp");

                            AppController.getInstance().getSessionManager().setUserAccount(null);
                            AppController.getInstance().getSessionManager().setUserAccount(objAuthUser);
                            AppController.getInstance().getSessionManager().putStringData("pin", sPin);

                            successDialog(verificationDevice.message);
                        }else{
                            AppController.getInstance().errorDialog(VerifikasiDevice.this, verificationDevice.message);
                        }
                    }else{
                        AppController.getInstance().errorDialog(VerifikasiDevice.this, "Verifikasi Perangkat Baru Gagal!");
                        progress.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Obj_VerificationDevice> call, Throwable t) {
                    progress.dismiss();
                    AppController.getInstance().errorDialog(VerifikasiDevice.this, "Verifikasi Perangkat Baru Gagal!");
                }
            });
        }catch (Exception e){
            progress.dismiss();
            Log.v("Exception", e.toString());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            //optionmenu
            case android.R.id.home:
                finish();
                break;

        }
        return true;
    }

    public void successDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialog));
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent intent = new Intent (getApplicationContext(), Main.class);
                        startActivity(intent);
                        finish();
                    }
                }).show();
    }
}
