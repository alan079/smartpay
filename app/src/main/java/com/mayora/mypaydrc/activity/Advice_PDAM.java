package com.mayora.mypaydrc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Advice_PDAM;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Advice_PDAM extends AppCompatActivity{

    private Button btnAdvice;
    private EditText editNopel, editNoref;
    private NestedScrollView scroll;

    ProgressDialog progress;
    private Obj_Advice_PDAM objAdvice;
    private String MSG="", SN="", NOPEL="", NAMA = "", BIAYA_ADMIN="", NOREF="", TAGIHAN="", REGION="",
            PERIODE ="", IDPEL="", STANDMETER="", TOTTAGIHAN = "", HARGA="", REFFID = "", RECEIPT="", PRODUCT_ID="", DENDA="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.advice_activity);

        InitControl();

        btnAdvice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdviceProccess();
            }
        });

    }

    void InitControl(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Advice PDAM");

        scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        editNopel = (EditText) findViewById(R.id.editNopel);
        editNoref = (EditText) findViewById(R.id.editNoref);
        btnAdvice = findViewById(R.id.btn_advice);
    }

    private void AdviceProccess() {
        progress = ProgressDialog.show(this, "Info", "Checking data", true);

        String Idpel = editNopel.getText().toString().trim();
        String Noref = editNoref.getText().toString().trim();
        String version = AppController.getInstance().getVersionName();
        String decryptPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decryptPin);
        Log.e("decrypt", sPin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("payment", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Advice_PDAM param = new Obj_Advice_PDAM(AppConfig.IMEI, AppConfig.KEY, AppConfig.REQID, AppConfig.USERID, version, "", Idpel, Noref, "1", "1");
            Call<Obj_Advice_PDAM> call = NetworkManager.getNetworkService(this).advicePDAM(param);
            call.enqueue(new Callback<Obj_Advice_PDAM>() {
                @Override
                public void onResponse(Call<Obj_Advice_PDAM> call, Response<Obj_Advice_PDAM> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objAdvice = response.body();
                        if (objAdvice != null) {
                            if (objAdvice.rc.equals("00")) {
                                try {
                                    MSG = objAdvice.message;
                                    SN = objAdvice.display.SN;
                                    IDPEL = objAdvice.subscriber;
                                    NAMA = objAdvice.display.NAMA;
                                    PERIODE = objAdvice.display.PERIODE;
                                    STANDMETER = objAdvice.display.STAND_METER;
                                    REGION = objAdvice.display.REGION;
                                    TAGIHAN = objAdvice.display.TAGIHAN;
                                    TOTTAGIHAN = objAdvice.display.TOTAL_TAGIHAN;
                                    BIAYA_ADMIN = objAdvice.display.BIAYA_ADMIN;
                                    HARGA = objAdvice.display.HARGA;
                                    NOREF = objAdvice.reffnum;
                                    RECEIPT = objAdvice.receipt;
                                    PRODUCT_ID = objAdvice.product;
                                    DENDA = objAdvice.display.DENDA;

                                    Intent mIntent = new Intent(getApplicationContext(), Advice_PDAM_Status.class);
                                    mIntent.putExtra("MSG", MSG);
                                    mIntent.putExtra("SN", SN);
                                    mIntent.putExtra("IDPEL", IDPEL);
                                    mIntent.putExtra("NAMA", NAMA);
                                    mIntent.putExtra("BULAN", PERIODE);
                                    mIntent.putExtra("STANDMETER", STANDMETER);
                                    mIntent.putExtra("REGION", REGION);
                                    mIntent.putExtra("RPTAGIHAN", TAGIHAN);
                                    mIntent.putExtra("TOTTAGIHAN", TOTTAGIHAN);
                                    mIntent.putExtra("ADMIN", BIAYA_ADMIN);
                                    mIntent.putExtra("TOTBAYAR", HARGA);
                                    mIntent.putExtra("NOREF", NOREF);
                                    mIntent.putExtra("RECEIPT", RECEIPT);
                                    mIntent.putExtra("PRODUCTID", PRODUCT_ID);
                                    mIntent.putExtra("DENDA", DENDA);
                                    startActivity(mIntent);
                                    finish();

                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), "Pembayaran Tagihan PDAM gagal!", Toast.LENGTH_LONG).show();
                                    Log.e("ErrorAdvicePayment", e.toString());
                                }
                            } else {
                                NOREF = objAdvice.reference;
                                AppController.getInstance().errorDialog(Advice_PDAM.this, objAdvice.message);
                            }
                        }else{
                            Toast.makeText(getApplicationContext(), "Pembayaran Tagihan PDAM gagal!", Toast.LENGTH_LONG).show();
                            Log.e("ErrorAdvicePayment", "null");
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Pembayaran Tagihan PDAM gagal!", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Obj_Advice_PDAM> call, Throwable t) {
                    progress.dismiss();
                    Toast.makeText(getApplicationContext(), "Pembayaran Tagihan PDAM gagal!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            Toast.makeText(getApplicationContext(), "Pembayaran Tagihan PDAM gagal!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
