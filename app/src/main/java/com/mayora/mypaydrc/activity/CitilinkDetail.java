package com.mayora.mypaydrc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Payment_Citilink;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CitilinkDetail extends AppCompatActivity {

    private TextView txtKodebooking, txtNama, txtNoTerbang, txtJmlTerbang, txtKelas, txtCarrier, txtDari, txtTujuan, txtTglBrgkt,
            txtJamBrgkt, txtJmlPenumpang, txtHarga, txtAdmin, txtTotalTagihan;
    private Button btnBayar;
    private LinearLayout linearLayout;

    ProgressDialog progress;
    Obj_Payment_Citilink objPaymentCitilink;
    private String sSubscriber = "", sProduct = "";
    private String MSG = "", TGL_BAYAR = "", NOPEL = "", NOREF = "", INQUIRYNUM = "", NAMA= "", HARGA ="", TOTAL_TAGIHAN="", BIAYA_ADMIN="", JML_PENUMPANG= "", DARI = "",
            TUJUAN = "", TGL_BRGKT = "", JAM_BRGKT = "", PRODUCT = "", RECEIPT= "";
    private String KODE_BOOKING = "", NO_TERBANG = "", JML_TERBANG = "", KELAS = "", CARRIER = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.citilink_detail);

        InitControl();
        GetExtrasData();

        btnBayar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Payment();
            }
        });
    }

    void InitControl(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        NestedScrollView scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        linearLayout = findViewById(R.id.layout_detail);

        txtKodebooking = findViewById(R.id.txt_kodebooking);
        txtNama = findViewById(R.id.txt_nama);
        txtNoTerbang = findViewById(R.id.txt_no_terbang);
        txtJmlTerbang = findViewById(R.id.txt_jml_terbang);
        txtKelas = findViewById(R.id.txt_kelas);
        txtCarrier = findViewById(R.id.txt_carrier);
        txtDari = findViewById(R.id.txt_dari);
        txtTujuan = findViewById(R.id.txt_tujuan);
        txtTglBrgkt = findViewById(R.id.txt_tglbrgkt);
        txtJamBrgkt = findViewById(R.id.txt_jambrgkt);
        txtJmlPenumpang = findViewById(R.id.txt_jmlpenumpang);
        txtHarga = findViewById(R.id.txt_harga);
        txtAdmin = findViewById(R.id.txt_admin);
        txtTotalTagihan = findViewById(R.id.txt_totaltagihan);

        btnBayar = findViewById(R.id.btnBayar);
    }

    void GetExtrasData(){
        Intent intent = getIntent();
        KODE_BOOKING = intent.getStringExtra("KODE_BOOKING");
        NAMA = intent.getStringExtra("NAMA");
        NO_TERBANG = intent.getStringExtra("NO_TERBANG");
        JML_TERBANG = intent.getStringExtra("JML_TERBANG");
        KELAS = intent.getStringExtra("KELAS");
        CARRIER = intent.getStringExtra("CARRIER");
        DARI = intent.getStringExtra("DARI");
        TUJUAN = intent.getStringExtra("TUJUAN");
        TGL_BRGKT = intent.getStringExtra("TGL_BRGKT");
        JAM_BRGKT = intent.getStringExtra("JAM_BRGKT");
        JML_PENUMPANG = intent.getStringExtra("JML_PENUMPANG");
        HARGA = intent.getStringExtra("HARGA");
        BIAYA_ADMIN = intent.getStringExtra("BIAYA_ADMIN");
        TOTAL_TAGIHAN = intent.getStringExtra("TOTAL_TAGIHAN");
        PRODUCT = intent.getStringExtra("PRODUCT");
        INQUIRYNUM = intent.getStringExtra("INQUIRYNUM");

        txtKodebooking.setText(KODE_BOOKING);
        txtNama.setText(NAMA);
        txtNoTerbang.setText(NO_TERBANG);
        txtJmlTerbang.setText(JML_TERBANG);
        txtKelas.setText(KELAS);
        txtCarrier.setText(CARRIER);
        txtDari.setText(DARI);
        txtTujuan.setText(TUJUAN);
        txtTglBrgkt.setText(TGL_BRGKT);
        txtJamBrgkt.setText(JAM_BRGKT);
        txtJmlPenumpang.setText(JML_PENUMPANG);
        txtHarga.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(HARGA)));
        txtAdmin.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(BIAYA_ADMIN)));
        txtTotalTagihan.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTAL_TAGIHAN)));
    }

    private void Payment() {
        progress = ProgressDialog.show(CitilinkDetail.this, "Info", "Checking data", true);

        sProduct = PRODUCT;
        String subscriber = txtKodebooking.getText().toString();
        sSubscriber = subscriber;
        String version = AppController.getInstance().getVersionName();
        String decryptPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decryptPin);
        Log.e("PIN", sPin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("payment", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Payment_Citilink param = new Obj_Payment_Citilink(AppConfig.IMEI, AppConfig.KEY, sProduct, AppConfig.REQID, sSubscriber, AppConfig.USERID, version, INQUIRYNUM);
            Call<Obj_Payment_Citilink> call = NetworkManager.getNetworkService(this).paymentCitilink(param);
            call.enqueue(new Callback<Obj_Payment_Citilink>() {
                @Override
                public void onResponse(Call<Obj_Payment_Citilink> call, Response<Obj_Payment_Citilink> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objPaymentCitilink = response.body();
                        if (objPaymentCitilink.rc.equals("00")) {

                            MSG = objPaymentCitilink.message;
                            TGL_BAYAR = objPaymentCitilink.display.TGL_BAYAR;
                            NOPEL = objPaymentCitilink.display.NOPEL;
                            NOREF = objPaymentCitilink.reffnum;
                            NAMA = objPaymentCitilink.display.NAMA;
                            NO_TERBANG = objPaymentCitilink.display.NO_PENERBANGAN;
                            JML_TERBANG = objPaymentCitilink.display.JML_PENERBANGAN;
                            KELAS = objPaymentCitilink.display.KELAS;
                            CARRIER = objPaymentCitilink.display.CARRIER;
                            DARI = objPaymentCitilink.display.DARI;
                            TUJUAN = objPaymentCitilink.display.TUJUAN;
                            TGL_BRGKT = objPaymentCitilink.display.TGL_BRGKT;
                            JAM_BRGKT = objPaymentCitilink.display.JAM_BRGKT;
                            JML_PENUMPANG = objPaymentCitilink.display.JML_PENUMPANG;
                            HARGA = objPaymentCitilink.display.HARGA;
                            BIAYA_ADMIN = objPaymentCitilink.display.BIAYA_ADMIN;
                            TOTAL_TAGIHAN = objPaymentCitilink.display.TOTAL_TAGIHAN;
                            PRODUCT = objPaymentCitilink.product;
                            RECEIPT = objPaymentCitilink.receipt;

                            Intent intent = new Intent(CitilinkDetail.this, TiketKeretaStatus.class);
                            intent.putExtra("MSG", MSG);
                            intent.putExtra("TGL_BAYAR",TGL_BAYAR);
                            intent.putExtra("NOPEL",NOPEL);
                            intent.putExtra("NOREF",NOREF);
                            intent.putExtra("NAMA",NAMA);
                            intent.putExtra("NO_TERBANG",NO_TERBANG);
                            intent.putExtra("JML_TERBANG",JML_TERBANG);
                            intent.putExtra("KELAS",KELAS);
                            intent.putExtra("CARRIER",CARRIER);
                            intent.putExtra("DARI",DARI);
                            intent.putExtra("TUJUAN",TUJUAN);
                            intent.putExtra("TGL_BRGKT",TGL_BRGKT);
                            intent.putExtra("JAM_BRGKT",JAM_BRGKT);
                            intent.putExtra("JML_PENUMPANG",JML_PENUMPANG);
                            intent.putExtra("HARGA",HARGA);
                            intent.putExtra("BIAYA_ADMIN",BIAYA_ADMIN);
                            intent.putExtra("TOTAL_TAGIHAN",TOTAL_TAGIHAN);
                            intent.putExtra("PRODUCT",PRODUCT);
                            intent.putExtra("RECEIPT", RECEIPT);
                            startActivity(intent);

                        } else {
                            AppController.getInstance().errorDialog(CitilinkDetail.this, objPaymentCitilink.message);
                        }
                    } else {
                        Toast.makeText(CitilinkDetail.this, "Inquiry gagal!", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Obj_Payment_Citilink> call, Throwable t) {
                    progress.dismiss();
                    Toast.makeText(CitilinkDetail.this, "Inquiry gagal!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            Toast.makeText(CitilinkDetail.this, "Inquiry gagal!", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
