package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Inquiry_BPJS;
import com.mayora.mypaydrc.model.Obj_Jenis_Type;
import com.mayora.mypaydrc.model.Obj_SubCategory;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BPJSFragment extends Fragment {
    private EditText editIDpel;
    private Button btnInquiry, btnAdvice;
    private Spinner spinJenis;
    private Spinner spinMonth;

    ProgressDialog progress;
    private Obj_Inquiry_BPJS objInquiry;
    private String sProduct ="", sSubscriber = "";
    private String IDPEL = "", NAMA = "", BULAN ="", ADMIN="", TUNGGAKAN="", JUMPESERTA="", TOTTAGIHAN = "", REFFID = "", TOTBAYAR="";
    private ArrayList<Obj_Jenis_Type> arrJenis = new ArrayList<Obj_Jenis_Type>();

    ProgressDialog progressDialog;
    private Obj_SubCategory objSubCategory;
    private String productCode = "";
    private String NO = "", CATEGORY = "";
    private String arrMonth[] = {"1","2","3","4","5","6","7","8","9","10","11","12"};


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getArguments() != null) {
            NO = getArguments().getString("NO");
            CATEGORY = getArguments().getString("CATEGORY");
        }

        View view = inflater.inflate(R.layout.frag_bpjs, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);

        if(CATEGORY != null) {
            if (CATEGORY.equals("2")) {
                editIDpel.setText(NO);
            }
        }

        editIDpel.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        try {
            FillJenis();
        }catch(Exception e){
            e.printStackTrace();
        }

        editIDpel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editIDpel.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    return true;
                }
                return false;
            }
        });

        spinJenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Obj_Jenis_Type jenis = (Obj_Jenis_Type) parent.getSelectedItem();
                productCode = jenis.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // hide virtual keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editIDpel.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN);

                if(editIDpel.getText().length() > 0){
                    Inquiry();
                }
            }
        });

        btnAdvice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Advice_BPJS.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private void InitControl(View v) {
        editIDpel = v.findViewById(R.id.edit_no);
        btnInquiry = v.findViewById(R.id.btnInquiry);
        btnAdvice = v.findViewById(R.id.btnAdvice);
        spinJenis = v.findViewById(R.id.spin_jenis);
        spinMonth = v.findViewById(R.id.spin_month);
        adapterMonth(arrMonth);
    }

    void FillJenis(){

        progressDialog = ProgressDialog.show(getActivity(),"Informasi", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("productinquiry", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_SubCategory param = new Obj_SubCategory(AppConfig.USERID, AppConfig.REQID, "2", AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_SubCategory> call = NetworkManager.getNetworkService(getActivity()).getSubCategoryProd(param);
            call.enqueue(new Callback<Obj_SubCategory>() {
                @Override
                public void onResponse(Call<Obj_SubCategory> call, Response<Obj_SubCategory> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        String jName = "";
                        String jId = "";
                        objSubCategory = response.body();
                        if (objSubCategory.rc.equals("00")){
                            for (Obj_SubCategory.Datum dat : objSubCategory.data){
                                jName = dat.name;
                                jId = dat.product_id;
                                arrJenis.add(new Obj_Jenis_Type(jId, jName));
                            }
                            adapterJenis(arrJenis);

                        }else if (objSubCategory.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            AppController.getInstance().errorDialog(getContext(), "Layanan BPJS tidak tersedia!");
                        }else if (objSubCategory.rc.equals("78")){
                            updateDeviceDialog(objSubCategory.message);
                        }else{
                            AppController.getInstance().errorDialog(getContext(), "Layanan BPJS tidak tersedia!");
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_SubCategory> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
        }
    }

    void adapterJenis(ArrayList<Obj_Jenis_Type> data){
        ArrayAdapter<Obj_Jenis_Type> adapter = new ArrayAdapter<Obj_Jenis_Type>(getContext(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinJenis.setAdapter(adapter);
    }

    void adapterMonth(String[] data){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinMonth.setAdapter(adapter);
    }

    private void Inquiry() {
        progress = ProgressDialog.show(getActivity(), "Info", "Checking data", true);

        sProduct = productCode;
        sSubscriber = editIDpel.getText().toString();
        String month = spinMonth.getSelectedItem().toString();
        String version = AppController.getInstance().getVersionName();
        String decryptPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decryptPin);
        Log.d("USERID", AppConfig.USERID);
        Log.d("PIN", sPin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("inquiry", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Inquiry_BPJS param = new Obj_Inquiry_BPJS(AppConfig.IMEI, AppConfig.KEY, AppConfig.REQID, AppConfig.USERID, version, sProduct, sSubscriber, month);
            Call<Obj_Inquiry_BPJS> call = NetworkManager.getNetworkService(getContext()).inquiryBPJS(param);
            call.enqueue(new Callback<Obj_Inquiry_BPJS>() {
                @Override
                public void onResponse(Call<Obj_Inquiry_BPJS> call, Response<Obj_Inquiry_BPJS> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objInquiry = response.body();
                        if (objInquiry.rc.equals("00")) {

                            IDPEL = objInquiry.display.IDPEL;
                            NAMA = objInquiry.display.NAMA;
                            BULAN = objInquiry.display.TAGIHAN_BULAN;
                            JUMPESERTA = objInquiry.display.JUMLAH_PESERTA;
                            TOTTAGIHAN = objInquiry.display.TOTAL_TAGIHAN;
                            ADMIN = objInquiry.display.BIAYA_ADMIN;
                            TUNGGAKAN = objInquiry.display.TUNGGAKAN;
                            TOTBAYAR = objInquiry.display.HARGA;
                            REFFID = objInquiry.inquiry_num;

                            successDialog(objInquiry.message);

                        } else {
                            AppController.getInstance().errorDialog(getActivity(), objInquiry.message);
                        }
                    } else {
                        Toast.makeText(getContext(), "Inquiry gagal!", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Obj_Inquiry_BPJS> call, Throwable t) {
                    progress.dismiss();
                    Toast.makeText(getContext(), "Inquiry gagal!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            Toast.makeText(getContext(), "Inquiry gagal!", Toast.LENGTH_LONG).show();
        }
    }

    private void successDialog(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        // TODO Auto-generated method stub
                        Intent intent = new Intent(getContext(), BPJSDetail.class);
                        intent.putExtra("PRODUCTID",productCode);
                        intent.putExtra("IDPEL",IDPEL);
                        intent.putExtra("NAMA",NAMA);
                        intent.putExtra("BULAN",BULAN);
                        intent.putExtra("JUMPESERTA",JUMPESERTA);
                        intent.putExtra("TOTTAGIHAN",TOTTAGIHAN);
                        intent.putExtra("ADMIN",ADMIN);
                        intent.putExtra("TUNGGAKAN",TUNGGAKAN);
                        intent.putExtra("TOTBAYAR",TOTBAYAR);
                        intent.putExtra("REFFID",REFFID);
                        startActivity(intent);
                    }
                }).show();
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);
        snackbar.show();
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
