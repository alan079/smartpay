package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_NominalToken;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 31/05/2017.
 */

public class ListrikTokenFragment_New extends Fragment {
    private EditText editNo;
    private ImageView icListrik;
    private Button btnInquiry;
    ProgressDialog progressDialog;

    private Obj_NominalToken objNominalToken;
    private String sSubscriber = "";
    private String NOMETER = "", TARIFDAYA = "", NAMAPELANGGAN = "", REFFID = "";
    private String NO = "", CATEGORY = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getArguments() != null) {
            NO = getArguments().getString("NO");
            CATEGORY = getArguments().getString("CATEGORY");
        }

        View view = inflater.inflate(R.layout.frag_listrik_token_new, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);

        if(CATEGORY != null) {
            if (CATEGORY.equals("11")) {
                editNo.setText(NO);
            }
        }

        editNo.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        editNo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editNo.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    return true;
                }
                return false;
            }
        });

        btnInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Inquiry();
            }
        });
    }

    private void InitControl(View v) {

        icListrik = v.findViewById(R.id.img);
        editNo = v.findViewById(R.id.edit_no);
        btnInquiry =  v.findViewById(R.id.btn_next);
    }

    private void Inquiry() {
        progressDialog = ProgressDialog.show(getActivity(),"Informasi", "Mengambil Data", true);

        sSubscriber = editNo.getText().toString();
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("priceplnprepaid", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_NominalToken param = new Obj_NominalToken(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI, sSubscriber);

            Call<Obj_NominalToken> call = NetworkManager.getNetworkService(getActivity()).getNomToken(param);
            call.enqueue(new Callback<Obj_NominalToken>() {
                @Override
                public void onResponse(Call<Obj_NominalToken> call, Response<Obj_NominalToken> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        objNominalToken = response.body();
                        if (objNominalToken.rc.equals("00")){
                            if (objNominalToken.data == null || objNominalToken.data.size() == 0){
                                AppController.getInstance().errorDialog(getContext(), "Token Listrik tidak tersedia!");
                            }else{
                                icListrik.setColorFilter(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                                NAMAPELANGGAN = objNominalToken.namapelanggan;
                                NOMETER = sSubscriber;
                                TARIFDAYA = objNominalToken.tarifdaya;
                                REFFID = objNominalToken.inquiry_reff;

                                successDialog("Inquiry berhasil.");
                            }
                        }else if (objNominalToken.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            AppController.getInstance().errorDialog(getContext(), "Token Listrik tidak tersedia!");
                        }else if (objNominalToken.rc.equals("78")){
                            updateDeviceDialog(objNominalToken.message);
                        }else{
                            AppController.getInstance().errorDialog(getContext(), objNominalToken.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_NominalToken> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
        }
    }

    private void successDialog(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent(getContext(), ListrikTokenDetail_New.class);
                        intent.putExtra("NAMA",NAMAPELANGGAN);
                        intent.putExtra("NOMETER",NOMETER);
                        intent.putExtra("TARIFDAYA",TARIFDAYA);
                        intent.putExtra("REFFID",REFFID);
                        //intent.putStringArrayListExtra("ARRNOMINAL",arrNom);
                        startActivity(intent);
                    }
                }).show();
    }

    private void errorDialog(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                }).show();
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}