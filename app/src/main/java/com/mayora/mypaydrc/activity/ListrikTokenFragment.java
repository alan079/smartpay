package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ItemNominalTokenAdapter;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_NominalToken;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 31/05/2017.
 */

public class ListrikTokenFragment extends Fragment {
    private EditText editNo;
    private ImageView icListrik;
    private Button btnNext;

    private ItemNominalTokenAdapter itemNominalAdapter;
    private RecyclerView recyclerView;

    ProgressDialog progressDialog;
    Obj_NominalToken objNominal;

    private String REFFID = "", NAMA = "", TARIFDAYA = "";
    private String NO = "", CATEGORY = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getArguments() != null) {
            NO = getArguments().getString("NO");
            CATEGORY = getArguments().getString("CATEGORY");
        }

        View view = inflater.inflate(R.layout.frag_listrik_token, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);

        if(CATEGORY != null) {
            if (CATEGORY.equals("11")) {
                editNo.setText(NO);
            }
        }
        editNo.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(count == 0){
                    removeNominal();
                }
            }
        });

        editNo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                        // hide virtual keyboard
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(editNo.getWindowToken(),
                                InputMethodManager.RESULT_UNCHANGED_SHOWN);

                        FillNominal();

                        return true;
                }
                return false;
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // hide virtual keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editNo.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN);

                if((editNo.getText().length() > 0)){
                    FillNominal();
                }
            }
        });


    }

    private void InitControl(View v) {
        recyclerView = v.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        icListrik = v.findViewById(R.id.img);
        editNo = v.findViewById(R.id.edit_no);
        btnNext =  v.findViewById(R.id.btn_next);
        btnNext = v.findViewById(R.id.btn_next);
    }

    void FillAdapter(){
        itemNominalAdapter = new ItemNominalTokenAdapter(getActivity(), objNominal, new ItemNominalTokenAdapter.OnDownloadClicked() {
            @Override
            public void OnDownloadClicked(String NOMINAL, String HARGA, String PROD_ID, String PROD_NAME) {
                Intent mIntent = new Intent(getContext(), ListrikTokenDetailNew.class);
                mIntent.putExtra("NOMINAL",NOMINAL);
                mIntent.putExtra("HARGA",HARGA);
                mIntent.putExtra("NOPEL",editNo.getText().toString());
                mIntent.putExtra("PROD_NAME",PROD_NAME);
                mIntent.putExtra("PROD_ID",PROD_ID);
                mIntent.putExtra("NAMA",NAMA);
                mIntent.putExtra("TARIFDAYA",TARIFDAYA);
                mIntent.putExtra("REFFID",REFFID);

                startActivity(mIntent);

            }
        });
        recyclerView.setAdapter(itemNominalAdapter);
    }

    void removeNominal(){
        icListrik.setColorFilter(ContextCompat.getColor(getContext(),R.color.gray_5));
        recyclerView.setAdapter(null);
    }

    void FillNominal(){
        recyclerView.setAdapter(null);

        progressDialog = ProgressDialog.show(getActivity(),"Informasi", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("priceplnprepaid", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_NominalToken param = new Obj_NominalToken(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI, editNo.getText().toString());

            Call<Obj_NominalToken> call = NetworkManager.getNetworkService(getActivity()).getNomToken(param);
            call.enqueue(new Callback<Obj_NominalToken>() {
                @Override
                public void onResponse(Call<Obj_NominalToken> call, Response<Obj_NominalToken> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        objNominal = response.body();
                        if (objNominal.rc.equals("00")){
                            if (objNominal.data == null || objNominal.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "Token Listrik tidak tersedia!");
                            }else{
                                icListrik.setColorFilter(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                                NAMA = objNominal.namapelanggan;
                                TARIFDAYA = objNominal.tarifdaya;
                                REFFID = objNominal.inquiry_reff;

                                FillAdapter();
                            }
                        }else if (objNominal.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), "Token Listrik tidak tersedia!");
                        }else if (objNominal.rc.equals("78")){
                            recyclerView.setAdapter(null);
                            updateDeviceDialog(objNominal.message);
                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), objNominal.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_NominalToken> call, Throwable t) {
                    progressDialog.dismiss();
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
            recyclerView.setAdapter(null);
        }
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

}
