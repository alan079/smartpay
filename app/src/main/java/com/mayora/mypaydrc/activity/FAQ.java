package com.mayora.mypaydrc.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;

import com.mayora.mypaydrc.R;

public class FAQ extends AppCompatActivity {

    private WebView webView;
    private Toolbar mToolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq);

        InitControl();

        String faq = FAQ();
        webView.loadData(faq,"text/html", "utf-8");
    }

    private void InitControl(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        webView = (WebView) findViewById(R.id.web_content);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
    };

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private String FAQ(){
        String faq = "<p style=\"text-align: justify; font-size: 11pt;\">\n\n" +
                "<b>Bagaimana saya registrasi menjadi merchant?</b> </br>\n" +
                "Untuk registrasi merchant di aplikasi mobile, segera hubungi customer service mayora dan ikuti syarat-syarat untuk menjadi merchant.</br></br>\n\n" +
                "<b>Bagaimana saya login ke aplikasi ini?</b> </br>\n" +
                "Untuk login ke aplikasi ini, masukkan userid dan password yang telah didaftarkan oleh customer service.</br></br>\n\n" +
                "<b>Bagaimana solusi ketika saya lupa Password?</b> </br>\n" +
                "Silahkan anda hubungi Call Center Mayora. Dan pihak Mayora akan mereset password Anda.</br></br>\n\n" +
                "<b>Apakah bisa saya mengubah Password?</b> </br>\n" +
                "Tentu bisa. Pilih menu Profil -> Pilih menu Ubah Password. Ubah Password anda di halaman tersebut. </br></br>\n\n" +
                "<b>Bagaimana cara saya topup? </b></br>\n" +
                "Untuk topup, SmartPay Mayora menggunakan system topup tiket. Dimana kita hanya memilih atau memasukkan jumlah nominal untuk topup lalu klik proses dan tunggu hingga muncul notifikasi. Setelah itu kita hanya melakukan transfer dengan jumlah yang sudah ditentukan ke rekening yang tertera pada notifikasi tersebut. Disarankan transfer jangan melebihi waktu yang ada pada notifikasi tersebut.</br></br>\n\n" +
                "<b>Bagaimana cara saya transaksi pembelian pulsa? </b></br>\n" +
                "Untuk transaksi pulsa, anda bisa klik menu Pulsa >> masukkan No. Hp pelanggan >> Klik Lanjut >> Pilih nominal pulsa >> Klik Proses dan tunggu hasil proses transaksi.</br></br>\n\n" +
                "<b>Bagaimana cara saya transaksi pembelian token pln? </b></br>\n" +
                "Untuk transaksi token pln, anda bisa klik menu Listrik PLN >> masuk tab Token Listrik >> masukkan No. Meter / ID Pelanggan >> Klik Lanjut >> Pilih nominal token >> Klik Proses dan tunggu hasil proses transaksi.</br></br>\n\n" +
                "<b>Apakah saya bisa melihat history transaksi? </b></br>\n" +
                "Tentu bisa. Untuk melihat history transaksi. Pilih menu Transaksi >> Masukkan Waktu Transaksi >> Masukkan No Pelanggan >> Klik Button Cari >> History transaksi akan muncul sesuai waktu dan no pelanggan yang dimasukkan. Di history transaksi tersebut anda bisa share dan print transaksi.</br></br>\n\n" +
                "<b>Bagaimana cara saya mencetak struk pembelian Token Listrik atau Tagihan PLN? </b></br>\n" +
                "Untuk mencetak struk token listrik atau tagihan PLN, klik tombol print. Maka akan muncul preview struk dan Print.</br></br>\n\n" +
                "<b>Bagaimana cara saya share hasil transaksi atau struk? </b></br>\n" +
                "Untuk share hasil transaksi atau struk. Ketika proses transaksi selesai klik button share. Atau bisa dshare di menu history transaksi</br></br>\n\n" +
                "<b>Bagaimana cara saya mendownload struk berbentuk PDF file? </b></br>\n" +
                "Untuk mendownload struk berbentuk PDF file, klik tombol print, maka struk berupa PDF mendownload secara otomatis ke folder anda.</br></br>\n\n"+"</p>\n";

        return faq;
    }
}
