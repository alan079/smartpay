package com.mayora.mypaydrc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Payment_Tv;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvDetail extends AppCompatActivity {

    private TextView txtIdpel, txtNama, txtHarga, txtTagBulan, txtAdmin, txtTotTagihan;
    private Button btnBayar;
    private String sProduct ="", sSubscriber = "";
    private String MSG = "", RECEIPT = "", TGL_BAYAR = "", NOREF = "", PRODUCT = "", IDPEL = "", NAMA = "", HARGA = "", TAGIHAN_BULAN = "",BIAYA_ADMIN = "", TOTAL_TAGIHAN = "", REFFID = "", INQUIRY_NUM = "";

    ProgressDialog progress;
    Obj_Payment_Tv objPaymentTv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tv_detail);

        InitControl();
        GetExtrasData();

        btnBayar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Payment();
            }
        });
    }

    void InitControl(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        NestedScrollView scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        //linearLayout = findViewById(R.id.layout_detail);
        txtIdpel = findViewById(R.id.txt_idpel);
        txtNama = findViewById(R.id.txt_nama);
        txtTagBulan = findViewById(R.id.txt_tagbulan);
        txtTotTagihan = findViewById(R.id.txt_totaltagihan);
        txtAdmin = findViewById(R.id.txt_admin);
        txtHarga = findViewById(R.id.txt_totalbayar);

        btnBayar = findViewById(R.id.btnBayar);
    }

    void GetExtrasData(){
        Intent intent = getIntent();
        IDPEL = intent.getStringExtra("IDPEL");
        NAMA = intent.getStringExtra("NAMA");
        TAGIHAN_BULAN = intent.getStringExtra("TAGIHAN_BULAN");
        HARGA = intent.getStringExtra("HARGA");
        BIAYA_ADMIN = intent.getStringExtra("ADMIN");
        TOTAL_TAGIHAN = intent.getStringExtra("TOTAL_TAGIHAN");
        PRODUCT = intent.getStringExtra("PRODUCT");
        REFFID = intent.getStringExtra("REFFID");
        INQUIRY_NUM = intent.getStringExtra("INQUIRY_NUM");

        txtIdpel.setText(IDPEL);
        txtNama.setText(NAMA);
        txtTagBulan.setText(TAGIHAN_BULAN);
        txtHarga.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(HARGA)));
        txtAdmin.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(BIAYA_ADMIN)));
        txtTotTagihan.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTAL_TAGIHAN)));
    }


    private void Payment() {
        progress = ProgressDialog.show(this, "Info", "Checking data", true);

        sProduct = PRODUCT;
        String subscriber = txtIdpel.getText().toString();
        sSubscriber = subscriber;
        String version = AppController.getInstance().getVersionName();
        String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(Pin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("payment", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Payment_Tv param = new Obj_Payment_Tv(AppConfig.IMEI, AppConfig.KEY, sProduct, AppConfig.REQID, sSubscriber, AppConfig.USERID, version, INQUIRY_NUM);
            Call<Obj_Payment_Tv> call = NetworkManager.getNetworkService(this).paymentTv(param);
            call.enqueue(new Callback<Obj_Payment_Tv>() {
                @Override
                public void onResponse(Call<Obj_Payment_Tv> call, Response<Obj_Payment_Tv> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objPaymentTv = response.body();
                        if (objPaymentTv.rc.equals("00")) {

                            MSG = objPaymentTv.message;
                            TGL_BAYAR = objPaymentTv.display.TGL_BAYAR;
                            IDPEL = objPaymentTv.display.NOPEL;
                            NOREF = objPaymentTv.display.NOREF;
                            NAMA = objPaymentTv.display.NAMA;
                            BIAYA_ADMIN = objPaymentTv.display.BIAYA_ADMIN;
                            TAGIHAN_BULAN = objPaymentTv.display.TAGIHAN_BULAN;
                            TOTAL_TAGIHAN = objPaymentTv.display.TOTAL_TAGIHAN;
                            HARGA = objPaymentTv.display.HARGA;
                            PRODUCT = objPaymentTv.product;
                            RECEIPT = objPaymentTv.receipt;

                            Intent intent = new Intent(TvDetail.this, TvStatus.class);
                            intent.putExtra("MSG", MSG);
                            intent.putExtra("TGL_BAYAR",TGL_BAYAR);
                            intent.putExtra("IDPEL",IDPEL);
                            intent.putExtra("NOREF",NOREF);
                            intent.putExtra("NAMA",NAMA);
                            intent.putExtra("BIAYA_ADMIN",BIAYA_ADMIN);
                            intent.putExtra("TAGIHAN_BULAN",TAGIHAN_BULAN);
                            intent.putExtra("TOTAL_TAGIHAN",TOTAL_TAGIHAN);
                            intent.putExtra("HARGA",HARGA);
                            intent.putExtra("PRODUCT",PRODUCT);
                            intent.putExtra("RECEIPT", RECEIPT);
                            startActivity(intent);

                        } else {
                            AppController.getInstance().errorDialog(TvDetail.this, objPaymentTv.message);
                        }
                    } else {
                        Toast.makeText(TvDetail.this, "Inquiry gagal!", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Obj_Payment_Tv> call, Throwable t) {
                    progress.dismiss();
                    Toast.makeText(TvDetail.this, "Inquiry gagal!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            Toast.makeText(TvDetail.this, "Inquiry gagal!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //optionmenu
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                break;
        }
        return true;
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}
