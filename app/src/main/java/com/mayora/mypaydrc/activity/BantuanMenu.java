package com.mayora.mypaydrc.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.MenuCardContentAdapter;
import com.mayora.mypaydrc.adapter.RecyclerItemClickListener;
import com.mayora.mypaydrc.model.MenuCardModel;
import com.mayora.mypaydrc.ui.GridSpacingItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eroy on 3/2/2017.
 */

public class BantuanMenu extends AppCompatActivity {
    private MenuCardContentAdapter menuAdapter;
    private List<MenuCardModel> menuList;
    private RecyclerView recyclerView;
    private Toolbar mToolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_list);

        InitControl();
    }

    void InitControl(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NestedScrollView scroll = (NestedScrollView) findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setNestedScrollingEnabled(false);

        menuList = new ArrayList<>();
        menuAdapter = new MenuCardContentAdapter(this, menuList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(2), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(menuAdapter);

        prepareMenuCard();

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        switch (position) {
                            case 0:
                                Intent contact = new Intent(getApplicationContext(),BantuanKontak.class);
                                startActivity(contact);
                                break;
                            case 1:
                                Intent faq = new Intent(getApplicationContext(),FAQ.class);
                                startActivity(faq);
                                break;

                            default:
                                break;
                        }
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
    }

    /**
     * Adding menu
     */
    private void prepareMenuCard() {
        int[] img = new int[]{
                R.drawable.ic_contact,
                R.drawable.ic_question};

        MenuCardModel a = new MenuCardModel("Hubungi Kami", img[0]);
        menuList.add(a);
        a = new MenuCardModel("FAQ", img[1]);
        menuList.add(a);
        menuAdapter.notifyDataSetChanged();
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            //optionmenu
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                break;
        }
        return true;
    }


}
