package com.mayora.mypaydrc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Payment_TELPPOST;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeleponDetail extends AppCompatActivity {

    private TextView txtIDPel, txtNama, txtTunggakan, txtTotTagihan, txtAdmin, txtTotBayar;
    private LinearLayout linearLayout;

    ProgressDialog progress;
    private Obj_Payment_TELPPOST objPayment;
    private String sReffid ="", sNominal = "";
    private String MSG="",IDPEL = "", NAMA = "", TUNGGAKAN ="", TOTTAGIHAN = "", REFFID = "", NOREF="",ADMIN="", TOTBAYAR="", PRODUCTID="",
            RECEIPT= "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.telepon_detail);

        GetExtrasData();
        InitControl();

    }

    void InitControl(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        NestedScrollView scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        linearLayout = findViewById(R.id.layout_pulsa_detail);
        txtIDPel = findViewById(R.id.txt_idpel);
        txtIDPel.setText(IDPEL);
        txtNama = findViewById(R.id.txt_nama);
        txtNama.setText(NAMA);
        txtTunggakan = findViewById(R.id.txt_tunggakan);
        txtTunggakan.setText(TUNGGAKAN);
        txtTotTagihan = findViewById(R.id.txt_totaltagihan);
        txtTotTagihan.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTTAGIHAN)));
        txtAdmin = findViewById(R.id.txt_admin);
        txtAdmin.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(ADMIN)));
        txtTotBayar = findViewById(R.id.txt_totalbayar);
        txtTotBayar.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTBAYAR)));

        Button btnBayar = findViewById(R.id.btnBayar);
        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentProccess();
            }
        });
    }

    void GetExtrasData(){
        Intent intent = getIntent();
        IDPEL = intent.getStringExtra("IDPEL");
        NAMA = intent.getStringExtra("NAMA");
        TUNGGAKAN = intent.getStringExtra("TUNGGAKAN");
        TOTTAGIHAN = intent.getStringExtra("TOTTAGIHAN");
        ADMIN = intent.getStringExtra("ADMIN");
        TOTBAYAR = intent.getStringExtra("TOTBAYAR");
        REFFID = intent.getStringExtra("REFFID");
        PRODUCTID = intent.getStringExtra("PRODUCTID");
    }

    private void PaymentProccess() {
        progress = ProgressDialog.show(this, "Info", "Checking data", true);

        String version = AppController.getInstance().getVersionName();
        String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(Pin);
        Log.e("PIN", sPin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("payment", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Payment_TELPPOST param = new Obj_Payment_TELPPOST(AppConfig.IMEI, AppConfig.KEY, AppConfig.REQID, AppConfig.USERID, version, PRODUCTID, IDPEL, REFFID);
            Call<Obj_Payment_TELPPOST> call = NetworkManager.getNetworkService(this).paymentTelpPOST(param);
            call.enqueue(new Callback<Obj_Payment_TELPPOST>() {
                @Override
                public void onResponse(Call<Obj_Payment_TELPPOST> call, Response<Obj_Payment_TELPPOST> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objPayment = response.body();
                        if (objPayment != null) {
                            if (objPayment.rc.equals("00")) {

                                try {
                                    MSG = objPayment.message;
                                    IDPEL = objPayment.subscriber;
                                    NAMA = objPayment.display.NAMA;
                                    NOREF = objPayment.reference;
                                    TOTTAGIHAN = objPayment.display.TOTAL_TAGIHAN;
                                    ADMIN = objPayment.display.BIAYA_ADMIN;
                                    TOTBAYAR = objPayment.display.HARGA;
                                    RECEIPT = objPayment.receipt;

                                    Intent mIntent = new Intent(getApplicationContext(), TeleponStatus.class);
                                    mIntent.putExtra("MSG", MSG);
                                    mIntent.putExtra("IDPEL", IDPEL);
                                    mIntent.putExtra("NAMA", NAMA);
                                    mIntent.putExtra("TUNGGAKAN", TUNGGAKAN);
                                    mIntent.putExtra("TOTTAGIHAN", TOTTAGIHAN);
                                    mIntent.putExtra("ADMIN", ADMIN);
                                    mIntent.putExtra("TOTBAYAR", TOTBAYAR);
                                    mIntent.putExtra("PRODUCTID", PRODUCTID);
                                    mIntent.putExtra("NOREF", NOREF);
                                    mIntent.putExtra("RECEIPT", RECEIPT);
                                    startActivity(mIntent);
                                    finish();

                                } catch (Exception e) {
                                    SnackbarMessage(linearLayout, "Pembayaran Tagihan Telepon gagal!", Snackbar.LENGTH_INDEFINITE);
                                    Log.e("ErrorTeleponPayment", e.toString());
                                }

                            } else {
                                NOREF = objPayment.reference;
                                AppController.getInstance().errorDialog(TeleponDetail.this, objPayment.message);
                            }
                        }else{
                            SnackbarMessage(linearLayout, "Pembayaran Tagihan Telepon gagal!", Snackbar.LENGTH_INDEFINITE);
                            Log.e("ErrorTeleponPayment", "null");
                        }
                    } else {
                        SnackbarMessage(linearLayout, "Pembayaran Tagihan Telepon gagal!", Snackbar.LENGTH_INDEFINITE);
                    }
                }

                @Override
                public void onFailure(Call<Obj_Payment_TELPPOST> call, Throwable t) {
                    progress.dismiss();
                    SnackbarMessage(linearLayout, "Pembayaran Tagihan Telepon gagal!", Snackbar.LENGTH_INDEFINITE);
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            SnackbarMessage(linearLayout, "Pembayaran Tagihan Telepon gagal!", Snackbar.LENGTH_INDEFINITE);
        }
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //optionmenu
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                break;
        }
        return true;
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);
        snackbar.show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}