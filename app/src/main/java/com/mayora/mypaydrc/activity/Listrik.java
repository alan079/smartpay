package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ViewPagerAdapter;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_CekSaldo;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Listrik extends AppCompatActivity {
    private Toolbar mToolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_listrik_token_menu,
            R.drawable.ic_listrik_menu
    };
    private String[] tabTitles = {"Token Listrik","Tagihan Listrik"};
    private TextView txtSaldo;
    ProgressDialog progress;
    Obj_CekSaldo cekSaldo;
    private Menu menu;
    private String NO = "", CATEGORY = "";
    private int POSITION=0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pulsa);

        LoadData();
        InitControl();
    }

    void InitControl(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.setCurrentItem(POSITION);

        txtSaldo = (TextView) findViewById(R.id.txt_saldo);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

    }

    void LoadData() {
        if (getIntent() != null) {
            Intent intent = getIntent();
            NO = intent.getStringExtra("NO");
            CATEGORY = intent.getStringExtra("CATEGORY");
            POSITION = intent.getIntExtra("POSITION", 0);
        }
    }

    private void setupTabIcons() {

        int i = 0;
        for (int count = viewPager.getAdapter().getCount(); i < count ; i++) {
            LinearLayout tabLinearLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab_content, null);
            TextView tabContent = (TextView) tabLinearLayout.findViewById(R.id.tabContent);
            tabContent.setText("  " + tabTitles[i]);
            tabContent.setCompoundDrawablesWithIntrinsicBounds(tabIcons[i], 0, 0, 0);
            tabLayout.getTabAt(i).setCustomView(tabContent);
        }

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), NO, CATEGORY);
        adapter.addFrag(new ListrikTokenFragment_New(), "Token Listrik");
        adapter.addFrag(new ListrikPLNFragment(), "Tagihan Listrik");
        viewPager.setAdapter(adapter);
    }

    private void CekSaldo(){
        progress = ProgressDialog.show(this, "", "Checking data", true);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("saldo", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_CekSaldo param = new Obj_CekSaldo(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI);
            Call<Obj_CekSaldo> call = NetworkManager.getNetworkService(this).cekSaldo(param);
            call.enqueue(new Callback<Obj_CekSaldo>() {
                @Override
                public void onResponse(Call<Obj_CekSaldo> call, Response<Obj_CekSaldo> response) {
                    int code = response.code();

                    if (code == 200){
                        progress.dismiss();
                        cekSaldo = response.body();

                        if (cekSaldo.rc.equals("00")){
                            AppConfig.BALANCE = cekSaldo.data.balance;
                            String sBal = "Rp. "+ AppConfig.BALANCE;
                            txtSaldo.setText(sBal);
                        }else if (cekSaldo.rc.equals("73")){
                            txtSaldo.setText("Rp.0");
                            //AppController.getInstance().showForceUpdateDialog(getApplicationContext());
                        }else if (cekSaldo.rc.equals("78")){
                            updateDeviceDialog(cekSaldo.message);
                        }else if (cekSaldo.rc.equals("97")){
                            Intent i = new Intent(getApplicationContext(), Login.class);
                            startActivity(i);
                            finish();
                        }else{
                            txtSaldo.setText("Rp.0");

                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_CekSaldo> call, Throwable t) {
                    progress.dismiss();
                    txtSaldo.setText("Rp.0");
                }
            });
        }catch (Exception e){
            progress.dismiss();
            Log.v("Exception", e.toString());
        }
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getApplicationContext(),Login.class);
                        startActivity(i);
                        finish();

                    }
                }).show();
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_token, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            //optionmenu
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                break;

            case R.id.guide_pembelian_token:
                Intent i = new Intent(getApplicationContext(), Guide_Token.class);
                startActivity(i);

        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        CekSaldo();
    }


}
