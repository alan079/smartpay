package com.mayora.mypaydrc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ItemNominalAdapter;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Nominal;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 31/05/2017.
 */

public class PulsaPaketFragment extends Fragment {
    private EditText editNo;
    private ImageView icprovider, icphone;
    private Button btnNext;

    private ItemNominalAdapter itemNominalAdapter;
    private RecyclerView recyclerView;

    private String provider="", prefix = "";

    ProgressDialog progressDialog;
    Obj_Nominal objNominal;

    private String NO = "", CATEGORY = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null) {
            NO = getArguments().getString("NO");
            CATEGORY = getArguments().getString("CATEGORY");
        }

        View view = inflater.inflate(R.layout.frag_pulsa, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);

        if(CATEGORY != null) {
            if (CATEGORY.equals("10")) {
                editNo.setText(NO);
            }
        }

        editNo.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(count == 0){
                    removeNominal();
                }
            }
        });

        editNo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (editNo.getText().toString().length() > 3){
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        // hide virtual keyboard
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(editNo.getWindowToken(),
                                InputMethodManager.RESULT_UNCHANGED_SHOWN);

                        prefix = editNo.getText().toString().substring(0, 4);

                        return true;
                    }
                }
                return false;
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (editNo.getText().toString().length() > 3){
                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editNo.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    prefix = editNo.getText().toString().substring(0, 4);
                    FillNominal();
                }
            }
        });

    }

    private void InitControl(View v) {
        recyclerView = v.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        icphone = v.findViewById(R.id.imgPhone);
        icprovider = v.findViewById(R.id.ic_provider);
        editNo = v.findViewById(R.id.edit_no);
        btnNext = v.findViewById(R.id.btn_next);
    }

    void showProvider(){
        if (prefix.equals("0855") | prefix.equals("0856") | prefix.equals("0857") | prefix.equals("0858") | prefix.equals("0814")
                | prefix.equals("0815") | prefix.equals("0816")) {
            AppController.getInstance().displayImageDrawable(getContext(), R.drawable.ic_indosat, icprovider);
            provider = "INDOSAT OOREDOO";

        } else if (prefix.equals("0811") | prefix.equals("0812") | prefix.equals("0813") | prefix.equals("0821")
                | prefix.equals("0822") | prefix.equals("0823") | prefix.equals("0852") | prefix.equals("0853")
                | prefix.equals("0851")) {
            AppController.getInstance().displayImageDrawable(getContext(), R.drawable.ic_telkomsel, icprovider);
            provider = "TELKOMSEL";

        } else if (prefix.equals("0881") | prefix.equals("0882") | prefix.equals("0883") | prefix.equals("0884")
                | prefix.equals("0885") | prefix.equals("0886") | prefix.equals("0887") | prefix.equals("0888")
                | prefix.equals("0889")) {
            AppController.getInstance().displayImageDrawable(getContext(), R.drawable.ic_smartfren, icprovider);
            provider = "SMARTFREN";

        } else if (prefix.equals("0817") | prefix.equals("0818") | prefix.equals("0819") | prefix.equals("0859")
                | prefix.equals("0877") | prefix.equals("0878")) {
            AppController.getInstance().displayImageDrawable(getContext(), R.drawable.ic_xl, icprovider);
            provider = "XL";

        } else if (prefix.equals("0831") | prefix.equals("0832") | prefix.equals("0833") | prefix.equals("0835") | prefix.equals("0837")
                | prefix.equals("0838")) {
            AppController.getInstance().displayImageDrawable(getContext(), R.drawable.ic_axis, icprovider);
            provider = "AXIS";

        } else if (prefix.equals("0896") | prefix.equals("0897") | prefix.equals("0898") | prefix.equals("0899")) {
            AppController.getInstance().displayImageDrawable(getContext(), R.drawable.ic_tri, icprovider);
            provider = "TRI";
        }
    }

    void removeNominal(){
        AppController.getInstance().displayImageDrawable(getContext(),0,icprovider);
        icphone.setColorFilter(ContextCompat.getColor(getContext(),R.color.gray_5));
        recyclerView.setAdapter(null);
    }

    void FillAdapter(){
        itemNominalAdapter = new ItemNominalAdapter(getActivity(), objNominal, new ItemNominalAdapter.OnDownloadClicked() {
            @Override
            public void OnDownloadClicked(String NOMINAL, String HARGA, String PROD_ID, String PROD_NAME) {
                Intent mIntent = new Intent(getContext(), PulsaDetail.class);
                mIntent.putExtra("NOMINAL",NOMINAL);
                mIntent.putExtra("HARGA",HARGA);
                mIntent.putExtra("NOPEL",editNo.getText().toString());
                mIntent.putExtra("PROD_NAME",PROD_NAME);
                mIntent.putExtra("PROD_ID",PROD_ID);
                startActivity(mIntent);

            }
        });
        recyclerView.setAdapter(itemNominalAdapter);
    }

    void FillNominal(){
        recyclerView.setAdapter(null);

        progressDialog = ProgressDialog.show(getActivity(),"Informasi", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("prefix", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_Nominal param = new Obj_Nominal(AppConfig.USERID, AppConfig.REQID, prefix, AppConfig.KEY, version, AppConfig.IMEI, "2");

            Call<Obj_Nominal> call = NetworkManager.getNetworkService(getActivity()).getNominal(param);
            call.enqueue(new Callback<Obj_Nominal>() {
                @Override
                public void onResponse(Call<Obj_Nominal> call, Response<Obj_Nominal> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        objNominal = response.body();
                        if (objNominal.rc.equals("00")){
                            if (objNominal.data == null || objNominal.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "Paket tidak tersedia!");
                            }else{
                                icphone.setColorFilter(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                                FillAdapter();
                                showProvider();
                            }
                        }else if (objNominal.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), "Paket tidak tersedia!");
                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), "Paket tidak tersedia!");
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_Nominal> call, Throwable t) {
                    progressDialog.dismiss();
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
            recyclerView.setAdapter(null);
        }
    }

}
