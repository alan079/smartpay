package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Inquiry_Citilink;
import com.mayora.mypaydrc.model.Obj_Inquiry_TiketKereta;
import com.mayora.mypaydrc.model.Obj_Jenis_Type;
import com.mayora.mypaydrc.model.Obj_SubCategory;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 26/02/2018.
 */

public class TiketFragment extends Fragment {
    private Button btnNext;
    private EditText txtKodeBayar;
    private TextView titleKodeBayar;
    private RelativeLayout relativeLayout;
    private Spinner spinJenis;

    ProgressDialog progress;
    private Obj_Inquiry_TiketKereta objInquiry;
    private Obj_Inquiry_Citilink objInquiryCitilink;
    private String sProduct ="", sSubscriber = "";

    private String MSG = "", KODE_BAYAR = "", NAMA= "", HARGA ="", TOTAL_TAGIHAN="", BIAYA_ADMIN="", JML_PENUMPANG= "", DARI = "",
            TUJUAN = "", TGL_BRGKT = "", JAM_BRGKT = "", NAMA_KERETA = "", NO_KERETA = "", NO_KURSI = "", PRODUCT = "", INQUIRYNUM = "";
    private String KODE_BOOKING = "", NO_TERBANG = "", JML_TERBANG = "", KELAS = "", CARRIER = "";

    ProgressDialog progressDialog;
    private Obj_SubCategory objSubCategory;
    private ArrayList<Obj_Jenis_Type> arrJenisName = new ArrayList<Obj_Jenis_Type>();
    private String NO ="", CATEGORY= "", productCode = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getArguments() != null) {
            NO = getArguments().getString("NO");
            CATEGORY = getArguments().getString("CATEGORY");
        }

        View view = inflater.inflate(R.layout.frag_tiket, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        InitControl(view);

        try {
            FillJenis();
        }catch(Exception e){
            e.printStackTrace();
        }

        spinJenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Obj_Jenis_Type jenis = (Obj_Jenis_Type) parent.getSelectedItem();
                productCode = jenis.getId();
                if (productCode.equals("KAIPAY01")){
                    titleKodeBayar.setText("Kode Pembayaran");
                } else {
                    titleKodeBayar.setText("Kode Booking");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productCode.equals("KAIPAY01")){
                    InquiryKereta();
                } else {
                    InquiryCitilink();
                }
            }
        });


    }

    private void InitControl(View v) {
        relativeLayout = v.findViewById(R.id.layout_no);
        titleKodeBayar = v.findViewById(R.id.txt_kodebayar);
        txtKodeBayar = v.findViewById(R.id.txtKodebayar);
        btnNext = v.findViewById(R.id.btnNext);
        spinJenis = v.findViewById(R.id.spin_jenis);
    }

    void FillJenis(){

        progressDialog = ProgressDialog.show(getActivity(),"Informasi", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("productinquiry", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_SubCategory param = new Obj_SubCategory(AppConfig.USERID, AppConfig.REQID, "1", AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_SubCategory> call = NetworkManager.getNetworkService(getActivity()).getSubCategoryProd(param);
            call.enqueue(new Callback<Obj_SubCategory>() {
                @Override
                public void onResponse(Call<Obj_SubCategory> call, Response<Obj_SubCategory> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        String jName = "";
                        String jId = "";
                        objSubCategory = response.body();
                        if (objSubCategory.rc.equals("00")){
                            for (Obj_SubCategory.Datum dat : objSubCategory.data){
                                jName = dat.name;
                                jId = dat.product_id;
                                arrJenisName.add(new Obj_Jenis_Type(jId, jName));
                            }
                            adapterJenis(arrJenisName);

                        }else if (objSubCategory.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            AppController.getInstance().errorDialog(getContext(), "Produk Tiket tidak tersedia!");
                        }else if (objSubCategory.rc.equals("78")){
                            updateDeviceDialog(objSubCategory.message);
                        }else{
                            AppController.getInstance().errorDialog(getContext(), "Produk Tiket tidak tersedia!");
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_SubCategory> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
        }
    }

    void adapterJenis(ArrayList<Obj_Jenis_Type> data){
        ArrayAdapter<Obj_Jenis_Type> adapter = new ArrayAdapter<Obj_Jenis_Type>(getContext(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinJenis.setAdapter(adapter);
    }

    private void InquiryKereta() {
        progress = ProgressDialog.show(getActivity(), "Info", "Checking data", true);

        sProduct = productCode;
        String subscriber = txtKodeBayar.getText().toString();
        sSubscriber = subscriber;
        String version = AppController.getInstance().getVersionName();
        String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(Pin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("inquiry", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        //1000004579

        try {
            Obj_Inquiry_TiketKereta param = new Obj_Inquiry_TiketKereta(AppConfig.IMEI, AppConfig.KEY, sProduct, AppConfig.REQID, sSubscriber, AppConfig.USERID, version);
            Call<Obj_Inquiry_TiketKereta> call = NetworkManager.getNetworkService(getContext()).inquiryTiketKereta(param);
            call.enqueue(new Callback<Obj_Inquiry_TiketKereta>() {
                @Override
                public void onResponse(Call<Obj_Inquiry_TiketKereta> call, Response<Obj_Inquiry_TiketKereta> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objInquiry = response.body();
                        if (objInquiry.rc.equals("00")) {

                            MSG = objInquiry.message;
                            KODE_BAYAR = objInquiry.subscriber;
                            NAMA = objInquiry.display.NAMA;
                            NAMA_KERETA = objInquiry.display.NAMA_KERETA;
                            NO_KERETA = objInquiry.display.NO_KERETA;
                            NO_KURSI = objInquiry.display.NO_KURSI;
                            DARI = objInquiry.display.DARI;
                            TUJUAN = objInquiry.display.TUJUAN;
                            TGL_BRGKT = objInquiry.display.TGL_BRGKT;
                            JAM_BRGKT = objInquiry.display.JAM_BRGKT;
                            JML_PENUMPANG = objInquiry.display.JML_PENUMPANG;
                            HARGA = objInquiry.display.HARGA;
                            BIAYA_ADMIN = objInquiry.display.BIAYA_ADMIN;
                            TOTAL_TAGIHAN = objInquiry.display.TOTAL_TAGIHAN;
                            PRODUCT = objInquiry.product;
                            INQUIRYNUM = objInquiry.inquiry_num;

                            successDialog(objInquiry.message);

                        } else {
                            AppController.getInstance().errorDialog(getActivity(), objInquiry.message);
                        }
                    } else {
                        Toast.makeText(getContext(), "Inquiry gagal!", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Obj_Inquiry_TiketKereta> call, Throwable t) {
                    progress.dismiss();
                    Toast.makeText(getContext(), "Inquiry gagal!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            Toast.makeText(getContext(), "Inquiry gagal!", Toast.LENGTH_LONG).show();
        }
    }


    private void InquiryCitilink() {
        progress = ProgressDialog.show(getActivity(), "Info", "Checking data", true);

        sProduct = productCode;
        String subscriber = txtKodeBayar.getText().toString();
        sSubscriber = subscriber;
        String version = AppController.getInstance().getVersionName();
        String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(Pin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("inquiry", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Inquiry_Citilink param = new Obj_Inquiry_Citilink(AppConfig.IMEI, AppConfig.KEY, sProduct, AppConfig.REQID, sSubscriber, AppConfig.USERID, version);
            Call<Obj_Inquiry_Citilink> call = NetworkManager.getNetworkService(getContext()).inquiryCitilink(param);
            call.enqueue(new Callback<Obj_Inquiry_Citilink>() {
                @Override
                public void onResponse(Call<Obj_Inquiry_Citilink> call, Response<Obj_Inquiry_Citilink> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objInquiryCitilink = response.body();
                        if (objInquiryCitilink.rc.equals("00")) {

                            KODE_BOOKING = objInquiryCitilink.display.KODE_PEMBAYARAN;
                            NAMA = objInquiryCitilink.display.NAMA;
                            NO_TERBANG = objInquiryCitilink.display.NO_PENERBANGAN;
                            JML_TERBANG = objInquiryCitilink.display.JML_PENERBANGAN;
                            KELAS = objInquiryCitilink.display.KELAS;
                            CARRIER = objInquiryCitilink.display.CARRIER;
                            DARI = objInquiryCitilink.display.DARI;
                            TUJUAN = objInquiryCitilink.display.TUJUAN;
                            TGL_BRGKT = objInquiryCitilink.display.TGL_BRGKT;
                            JAM_BRGKT = objInquiryCitilink.display.JAM_BRGKT;
                            JML_PENUMPANG = objInquiryCitilink.display.JML_PENUMPANG;
                            HARGA = objInquiryCitilink.display.HARGA;
                            BIAYA_ADMIN = objInquiryCitilink.display.BIAYA_ADMIN;
                            TOTAL_TAGIHAN = objInquiryCitilink.display.TOTAL_TAGIHAN;
                            PRODUCT = objInquiryCitilink.product;
                            INQUIRYNUM = objInquiryCitilink.inquiry_num;

                            successDialogCitilink(objInquiryCitilink.message);

                        } else {
                            AppController.getInstance().errorDialog(getActivity(), objInquiryCitilink.message);
                        }
                    } else {
                        Toast.makeText(getContext(), "Inquiry gagal!", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Obj_Inquiry_Citilink> call, Throwable t) {
                    progress.dismiss();
                    Toast.makeText(getContext(), "Inquiry gagal!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            Toast.makeText(getContext(), "Inquiry gagal!", Toast.LENGTH_LONG).show();
        }
    }

    private void successDialog(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub

                        Intent intent = new Intent(getContext(), TiketKeretaDetail.class);
                        intent.putExtra("KODE_BAYAR",KODE_BAYAR);
                        intent.putExtra("NAMA",NAMA);
                        intent.putExtra("NAMA_KERETA",NAMA_KERETA);
                        intent.putExtra("NO_KERETA",NO_KERETA);
                        intent.putExtra("NO_KURSI",NO_KURSI);
                        intent.putExtra("DARI",DARI);
                        intent.putExtra("TUJUAN",TUJUAN);
                        intent.putExtra("TGL_BRGKT",TGL_BRGKT);
                        intent.putExtra("JAM_BRGKT",JAM_BRGKT);
                        intent.putExtra("JML_PENUMPANG",JML_PENUMPANG);
                        intent.putExtra("HARGA",HARGA);
                        intent.putExtra("BIAYA_ADMIN",BIAYA_ADMIN);
                        intent.putExtra("TOTAL_TAGIHAN",TOTAL_TAGIHAN);
                        intent.putExtra("PRODUCT",PRODUCT);
                        intent.putExtra("INQUIRYNUM",INQUIRYNUM);
                        startActivity(intent);
                    }
                }).show();
    }

    private void successDialogCitilink(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent(getContext(), CitilinkDetail.class);
                        intent.putExtra("KODE_BOOKING",KODE_BOOKING);
                        intent.putExtra("NAMA",NAMA);
                        intent.putExtra("NO_TERBANG",NO_TERBANG);
                        intent.putExtra("JML_TERBANG",JML_TERBANG);
                        intent.putExtra("KELAS",KELAS);
                        intent.putExtra("CARRIER",CARRIER);
                        intent.putExtra("DARI",DARI);
                        intent.putExtra("TUJUAN",TUJUAN);
                        intent.putExtra("TGL_BRGKT",TGL_BRGKT);
                        intent.putExtra("JAM_BRGKT",JAM_BRGKT);
                        intent.putExtra("JML_PENUMPANG",JML_PENUMPANG);
                        intent.putExtra("HARGA",HARGA);
                        intent.putExtra("BIAYA_ADMIN",BIAYA_ADMIN);
                        intent.putExtra("TOTAL_TAGIHAN",TOTAL_TAGIHAN);
                        intent.putExtra("PRODUCT",PRODUCT);
                        intent.putExtra("INQUIRYNUM",INQUIRYNUM);
                        startActivity(intent);
                    }
                }).show();
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);
        snackbar.show();
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
