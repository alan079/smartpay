package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ItemNotifikasi;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Message;
import com.mayora.mypaydrc.util.DBNotificationHelper;

import java.util.List;

public class Notifikasi extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private Toolbar mToolbar;
    private RecyclerView recyclerView;

    private ProgressDialog progressDialog;
    private RecyclerView.Adapter mAdapter;

    private LinearLayoutManager mLayoutManager;
    SwipeRefreshLayout swipeRefreshLayout;

    int start = 0;
    int end = 10;

    private List<Message> messageList;
    private Menu menu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notifikasi);

        AppController.getInstance().getSessionManager().putIntegerData(AppConfig.NOTIF_COUNTER, 0);

        InitControl();
        FillGrid();
    }

    private void InitControl() {

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0) //check for scroll down
                {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        start = start + 10;
                        FillGridMore();
                    }
                }
            }
        });

    }

    void FillGrid(){
        start = 0;
        end = 10;

        progressDialog = ProgressDialog.show(this,"", "Mengambil Data", true);

        try{
            DBNotificationHelper dbNotif= new DBNotificationHelper(getApplicationContext());
            messageList = dbNotif.getAllMessages(start, end);
            if (messageList.size() > 0){
                FillAdapter();
            }else{
                Toast.makeText(getApplicationContext(),"Notifikasi tidak ada.", Toast.LENGTH_SHORT).show();
            }
            progressDialog.dismiss();
        }catch (Exception e){
            progressDialog.dismiss();
            recyclerView.setAdapter(null);
        }
    }

    void FillGridOnRefresh(){
        start = 0;
        end = 10;

        try{
            DBNotificationHelper dbNotif= new DBNotificationHelper(getApplicationContext());
            messageList = dbNotif.getAllMessages(start, end);
            if (messageList.size() > 0){
                FillAdapter();
            }else{
                Toast.makeText(getApplicationContext(),"Notifikasi tidak ada.", Toast.LENGTH_SHORT).show();
            }
            swipeRefreshLayout.setRefreshing(false);
        }catch (Exception e){
            swipeRefreshLayout.setRefreshing(false);
            recyclerView.setAdapter(null);
        }
    }

    void FillGridMore(){
        final ProgressDialog progressDialogMore = ProgressDialog.show(this,null, null, true);
        progressDialogMore.getWindow().setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
        progressDialogMore.setContentView( R.layout.progress_item );

        try{
            DBNotificationHelper dbNotif= new DBNotificationHelper(getApplicationContext());
            List<Message> messageListMore = dbNotif.getAllMessages(start, end);
            if (messageListMore.size() > 0){

                for (int i = 0; i < messageListMore.size(); i++) {
                    Message msg;
                    msg = messageListMore.get(i);
                    messageList.add(msg);
                }

                mAdapter.notifyDataSetChanged();
            }

            progressDialogMore.dismiss();
        }catch (Exception e){
            progressDialogMore.dismiss();
            recyclerView.setAdapter(null);
        }
    }

    void FillAdapter(){
        mAdapter = new ItemNotifikasi(this, messageList, new ItemNotifikasi.OnDownloadClicked() {
            @Override
            public void OnDownloadClicked(String DATE, String TITLE, String BODY) {

            }
        });
        recyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onRefresh() {
        FillGridOnRefresh();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_notif, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.delete:
                deleteDialog();
                break;

        }
        return true;
    }

    private void deleteNotif(){
        DBNotificationHelper dbNotif= new DBNotificationHelper(getApplicationContext());
        dbNotif.deleteAllMessage();
        recyclerView.setAdapter(null);
    }

    private void deleteDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialog));
        dialog.setMessage("Apakah anda yakin akan menghapus semua notifikasi?")
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        deleteNotif();
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
