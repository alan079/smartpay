package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.view.ContextThemeWrapper;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_NominalTopup;
import com.mayora.mypaydrc.model.Obj_Topup;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
* Created by eroy on 24/07/2017.
*/

public class TopupFragment extends Fragment {
   private Spinner spinNominal;
   private Button btnTopup;
   private ArrayList<String> nomTopup = new ArrayList<>();

   Obj_NominalTopup objNominalTopup;
   Obj_Topup objTopup;

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.frag_topup, container, false);

       return view;
   }

   @Override
   public void onViewCreated(View view, Bundle savedInstanceState) {
       InitControl(view);

       btnTopup.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               processTopup();
           }
       });

   }

   private void InitControl(View v) {
       spinNominal = (Spinner) v.findViewById(R.id.spinNominalTopup);
       btnTopup = (Button) v.findViewById(R.id.btnSubmit);
   }

   void FillNomTopup(){
       nomTopup.clear();
       final ProgressDialog progressDialogNom = ProgressDialog.show(getActivity(),"", "Mengambil Data", true);

       try {
           AppConfig.KEY = AppController.getInstance().getSignaturePost("ticketnominal", AppConfig.USERID);
       } catch (NoSuchAlgorithmException e) {
           e.printStackTrace();
       }

       String version = AppController.getInstance().getVersionName();

       try{
           Obj_NominalTopup param = new Obj_NominalTopup(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI);

           Call<Obj_NominalTopup> call = NetworkManager.getNetworkService(getActivity()).getNomTopup(param);
           call.enqueue(new Callback<Obj_NominalTopup>() {
               @Override
               public void onResponse(Call<Obj_NominalTopup> call, Response<Obj_NominalTopup> response) {
                   int code = response.code();
                   progressDialogNom.dismiss();

                   if (code == 200){
                       objNominalTopup = response.body();
                       if (objNominalTopup.rc.equals("00")){
                           if (objNominalTopup.data.nominal == null){
                               AppController.getInstance().errorDialog(getContext(), "Nominal Topup tidak tersedia!");
                           }else{
                               List<String> inomTopup = objNominalTopup.data.nominal;
                               for (int i=0;i<inomTopup.size();i++){
                                   String sNom = AppController.getInstance().toNumberFormatIDR(Double.parseDouble(inomTopup.get(i)));
                                   nomTopup.add(sNom);
                               }

                               FillAdapter_NomTopup(nomTopup);
                           }
                       }else if (objNominalTopup.rc.equals("73")){
                           //AppController.getInstance().showForceUpdateDialog(Login.this);
                           AppController.getInstance().errorDialog(getContext(), "Nominal Topup tidak tersedia!");
                       }else if (objNominalTopup.rc.equals("78")){
                           updateDeviceDialog(objNominalTopup.message);
                       }else{
                           AppController.getInstance().errorDialog(getContext(), "Nominal Topup tidak tersedia!");
                       }
                   }
               }

               @Override
               public void onFailure(Call<Obj_NominalTopup> call, Throwable t) {
                   Log.d("Nominal Topup", t.toString());
                   progressDialogNom.dismiss();
               }
           });
       }catch (Exception e){
           Log.d("Nominal Topup", e.toString());
           progressDialogNom.dismiss();
       }
   }

   private void FillAdapter_NomTopup(ArrayList<String> nom){
       try{
           ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, nom);
           adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
           spinNominal.setAdapter(adapter);
       }catch(Exception e){
           Log.d("AdapterNomTopup", e.toString());
       }
   }

   void processTopup(){
       final ProgressDialog progressDialog = ProgressDialog.show(getActivity(),"", "Proses Top Up", true);

       String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
       String sPin = decrypt(Pin);
       String version = AppController.getInstance().getVersionName();

       try {
           AppConfig.KEY = AppController.getInstance().getSignaturePut("topup", AppConfig.USERID, sPin);
       } catch (NoSuchAlgorithmException e) {
           e.printStackTrace();
       }

       Log.d("Devid", AppConfig.IMEI);
       try{
           String sNomTopup = spinNominal.getSelectedItem().toString().replace(".","").trim();
           Obj_Topup param = new Obj_Topup(AppConfig.USERID, AppConfig.REQID, sNomTopup, AppConfig.KEY, version, AppConfig.IMEI);

           Call<Obj_Topup> call = NetworkManager.getNetworkService(getActivity()).topup(param);
           call.enqueue(new Callback<Obj_Topup>() {
               @Override
               public void onResponse(Call<Obj_Topup> call, Response<Obj_Topup> response) {
                   int code = response.code();
                   progressDialog.dismiss();

                   try {
                       if (code == 200) {
                           objTopup = response.body();
                           if (objTopup.rc.equals("00")) {
                               AppController.getInstance().errorDialog(getContext(), objTopup.message);
                           } else if (objTopup.rc.equals("73")) {
                               AppController.getInstance().errorDialog(getContext(), objTopup.message);
                               //AppController.getInstance().showForceUpdateDialog(getContext());
                           } else if (objTopup.rc.equals("78")) {
                               updateDeviceDialog(objTopup.message);
                           } else {
                               AppController.getInstance().errorDialog(getContext(), objTopup.message);
                           }
                       }
                   }catch (Exception e){
                       e.printStackTrace();
                   }
               }

               @Override
               public void onFailure(Call<Obj_Topup> call, Throwable t) {
                   Log.d("Topup", t.toString());
                   AppController.getInstance().errorDialog(getContext(), "Permintaan Topup GAGAL!");
                   progressDialog.dismiss();
               }
           });
       }catch (Exception e){
           Log.d("Topup", e.toString());
           AppController.getInstance().errorDialog(getContext(), "Permintaan Topup GAGAL!");
           progressDialog.dismiss();
       }
   }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() ) {
            FillNomTopup();
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() ) {
            FillNomTopup();
        }
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
