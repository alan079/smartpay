package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Register;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 25/05/2017.
 */

public class Registrasi extends AppCompatActivity {

    private Button btnReg;
    private Toolbar mToolbar;
    private EditText editUserid, editNama, editAddr, editEmail, editHp, editPin, editPinConfirm;
    private CheckBox cbTerm;
    private TextView txtTerm;

    ProgressDialog progress;

    private Obj_Register objRegister;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registrasi);

        InitControl();
    }

    private void InitControl(){

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editNama = (EditText) findViewById(R.id.editName);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editHp = (EditText) findViewById(R.id.editHp);
        editAddr = (EditText) findViewById(R.id.editAddr);
        editUserid = (EditText) findViewById(R.id.editUserid);
        editPin = (EditText) findViewById(R.id.editPin);
        editPinConfirm = (EditText) findViewById(R.id.editPinConfirm);
        txtTerm = (TextView) findViewById(R.id.txtTerm);
        txtTerm.setClickable(true);
        cbTerm = (CheckBox) findViewById(R.id.cbTerm);
        btnReg = (Button) findViewById(R.id.btn_signup);

        txtTerm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /*Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/
            }
        });

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editUserid.getText().length() == 0) {
                    editUserid.setError(getString(R.string.error_field_required));
                    editUserid.requestFocus();
                } else if(editPin.getText().length() == 0){
                    editPin.setError(getString(R.string.error_field_required));
                    editPin.requestFocus();
                }else if(editPin.getText().length() != 6){
                    editPin.setError(getString(R.string.error_invalid_pin));
                    editPin.requestFocus();
                }else if(! (editPinConfirm.getText().toString()).equals(editPin.getText().toString())){
                    editPinConfirm.setError(getString(R.string.error_invalid_confirm));
                    editPinConfirm.requestFocus();
                }else if (editNama.getText().length() == 0) {
                    editNama.setError(getString(R.string.error_field_required));
                    editNama.requestFocus();
                } else if (editEmail.getText().length() == 0) {
                    editEmail.setError(getString(R.string.error_field_required));
                    editEmail.requestFocus();
                } else if (!(editEmail.getText().toString().contains("@"))) {
                    editEmail.setError(getString(R.string.error_mail));
                    editEmail.requestFocus();
                } else if (editHp.getText().length() == 0) {
                    editHp.setError(getString(R.string.error_field_required));
                    editHp.requestFocus();
                } else if (editAddr.getText().length() == 0) {
                    editAddr.setError(getString(R.string.error_field_required));
                    editAddr.requestFocus();
                } else if (!cbTerm.isChecked()) {
                    Toast.makeText(getApplicationContext(), "Anda belum menyetujui Syarat & Ketentuan!", Toast.LENGTH_LONG).show();
                } else {
                    ValidasiRegister();
                }

            }
        });
    };

    private void ValidasiRegister(){
        progress = ProgressDialog.show(this, "Info", "Checking data", true);

        try {
            AppConfig.HASHID = AppController.getInstance().getHashIdMD(AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try{
            String sPin = AppController.getInstance().md5(editPin.getText().toString().trim());
            String sUserid = editUserid.getText().toString().trim();
            String sName = editNama.getText().toString();
            String sEmail = editEmail.getText().toString();
            String sHp = editHp.getText().toString();
            String sAddress = editAddr.getText().toString();

            Obj_Register param = new Obj_Register(AppConfig.HASHID, AppConfig.REQID, sUserid, sPin, AppConfig.IMEI,
                    sName, sEmail, sHp, sAddress);
            Call<Obj_Register> call = NetworkManager.getNetworkService(this).regUser(param);
            call.enqueue(new Callback<Obj_Register>() {
                @Override
                public void onResponse(Call<Obj_Register> call, Response<Obj_Register> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200){
                        objRegister = response.body();
                        if (objRegister.rc.equals("00")){
                            Toast.makeText(getApplicationContext(),"Registrasi Berhasil. Silahkan login.",Toast.LENGTH_LONG).show();

                            Intent intent = new Intent (Registrasi.this, Login.class);
                            startActivity(intent);
                            finish();

                        }else{
                            errorDialog(objRegister.info);
                        }
                    }else{
                        errorDialog("Registrasi Gagal!");
                    }
                }

                @Override
                public void onFailure(Call<Obj_Register> call, Throwable t) {
                    progress.dismiss();
                    errorDialog("Registrasi Gagal!");
                }
            });
        }catch (Exception e){
            progress.dismiss();
            errorDialog("Registrasi Gagal!");
        }
    }

    private void errorDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                }).show();

    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //optionmenu
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                break;

        }
        return true;
    }
}
