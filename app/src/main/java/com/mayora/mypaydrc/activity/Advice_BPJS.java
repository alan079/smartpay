package com.mayora.mypaydrc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Advice_BPJS;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Advice_BPJS extends AppCompatActivity{

    private Button btnAdvice;
    private EditText editNopel, editNoref;
    private NestedScrollView scroll;

    ProgressDialog progress;
    private Obj_Advice_BPJS objAdviceBpjs;
    private String PRODUCTID="", MSG="",IDPEL = "", NAMA = "", BULAN ="", TUNGGAKAN="",  TOTTAGIHAN = "", REFFID = "",
            JUMPESERTA="", NOREF="",ADMIN="", TOTBAYAR="", SN="", RECEIPT="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.advice_activity);

        InitControl();

        btnAdvice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdviceProccess();
            }
        });

    }

    void InitControl(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Advice BPJS");

        scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        editNopel = (EditText) findViewById(R.id.editNopel);
        editNoref = (EditText) findViewById(R.id.editNoref);
        btnAdvice = findViewById(R.id.btn_advice);
    }

    private void AdviceProccess() {
        progress = ProgressDialog.show(this, "Info", "Checking data", true);

        String Idpel = editNopel.getText().toString().trim();
        String Noref = editNoref.getText().toString().trim();
        String version = AppController.getInstance().getVersionName();
        String decryptPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decryptPin);
        Log.e("decrypt", sPin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("payment", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Advice_BPJS param = new Obj_Advice_BPJS(AppConfig.IMEI, AppConfig.KEY, AppConfig.REQID, AppConfig.USERID, version, "", Idpel, Noref, "1", "1");
            Call<Obj_Advice_BPJS> call = NetworkManager.getNetworkService(this).adviceBPJS(param);
            call.enqueue(new Callback<Obj_Advice_BPJS>() {
                @Override
                public void onResponse(Call<Obj_Advice_BPJS> call, Response<Obj_Advice_BPJS> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objAdviceBpjs = response.body();
                        if (objAdviceBpjs != null) {
                            if (objAdviceBpjs.rc.equals("00")) {
                                try {
                                    MSG = objAdviceBpjs.message;
                                    SN = objAdviceBpjs.display.SN;
                                    IDPEL = objAdviceBpjs.display.IDPEL;
                                    NAMA = objAdviceBpjs.display.NAMA;
                                    NOREF = objAdviceBpjs.display.NOREF;
                                    JUMPESERTA = objAdviceBpjs.display.JUMLAH_PESERTA;
                                    BULAN = objAdviceBpjs.display.TAGIHAN_BULAN;
                                    TUNGGAKAN = objAdviceBpjs.display.TUNGGAKAN;
                                    TOTTAGIHAN = objAdviceBpjs.display.TOTAL_TAGIHAN;
                                    ADMIN = objAdviceBpjs.display.BIAYA_ADMIN;
                                    TOTBAYAR = objAdviceBpjs.display.HARGA;
                                    RECEIPT = objAdviceBpjs.receipt;
                                    PRODUCTID = objAdviceBpjs.product;

                                    Intent mIntent = new Intent(getApplicationContext(), Advice_BPJS_Status.class);
                                    mIntent.putExtra("MSG", MSG);
                                    mIntent.putExtra("SN", SN);
                                    mIntent.putExtra("IDPEL", IDPEL);
                                    mIntent.putExtra("NAMA", NAMA);
                                    mIntent.putExtra("NOREF", NOREF);
                                    mIntent.putExtra("JUMPESERTA", JUMPESERTA);
                                    mIntent.putExtra("BULAN", BULAN);
                                    mIntent.putExtra("TUNGGAKAN", TUNGGAKAN);
                                    mIntent.putExtra("TOTTAGIHAN", TOTTAGIHAN);
                                    mIntent.putExtra("ADMIN", ADMIN);
                                    mIntent.putExtra("TOTBAYAR", TOTBAYAR);
                                    mIntent.putExtra("PRODUCTID", PRODUCTID);
                                    mIntent.putExtra("RECEIPT", RECEIPT);
                                    startActivity(mIntent);
                                    finish();

                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), "Pembayaran Tagihan BPJS gagal!", Toast.LENGTH_LONG).show();
                                    Log.e("ErrorAdvicePayment", e.toString());
                                }

                            } else {
                                Toast.makeText(getApplicationContext(), objAdviceBpjs.message, Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(getApplicationContext(), "Pembayaran Tagihan BPJS gagal!", Toast.LENGTH_LONG).show();
                            Log.e("ErrorAdvicePayment", "null");
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Pembayaran Tagihan BPJS gagal!", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Obj_Advice_BPJS> call, Throwable t) {
                    progress.dismiss();
                    Toast.makeText(getApplicationContext(), "Pembayaran Tagihan BPJS gagal!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            Toast.makeText(getApplicationContext(), "Pembayaran Tagihan BPJS gagal!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                break;
        }
        return true;
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
