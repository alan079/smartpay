package com.mayora.mypaydrc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_AuthUser;

/**
 * Created by eroy on 29/04/2017.
 */

public class Splash extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        AppController.getInstance().getFCMID(this);

        Timer();

    }

    void Timer(){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                CheckUserData();
            }
        }, SPLASH_TIME_OUT);
    }

    void CheckUserData(){
        Obj_AuthUser authUser = AppController.getInstance().getSessionManager().getUserProfile();

        if (authUser != null){

            AppConfig.USERID = authUser.user;

            Intent intent = new Intent (getApplicationContext(), Main.class);
            startActivity(intent);
            finish();

        } else {
            Intent i = new Intent(getApplicationContext(),Login.class);
            startActivity(i);
            finish();
        }

    }
}
