package com.mayora.mypaydrc.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.print.BluetoothSPP;
import com.mayora.mypaydrc.print.BluetoothState;
import com.mayora.mypaydrc.print.DeviceList;

public class PreviewStrukTv extends AppCompatActivity {

    private Toolbar mToolbar;
    private TextView tglbayar, nopel, noref, reffid, nama, harga, totaltagihan, biayaadmin, tagihanbulan;
    private String MSG = "", RECEIPT = "", TGL_BAYAR = "", NOREF = "", PRODUCT = "", IDPEL = "", NAMA = "", HARGA = "", TAGIHAN_BULAN = "",BIAYA_ADMIN = "", TOTAL_TAGIHAN = "", REFFID = "", INQUIRY_NUM = "";

    private Button btnPrint;
    private Menu menu;
    private CoordinatorLayout coordinatorLayout;

    private BluetoothSPP bt;
    private StringBuilder data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_struk_tv);

        InitControl();
        getData();
        setupData();

        Snackbar.make(coordinatorLayout,"Status : Anda belum memilih printer!", Snackbar.LENGTH_INDEFINITE).show();
        bt = new BluetoothSPP(this);

        if(!bt.isBluetoothAvailable()) {
            Toast.makeText(getApplicationContext()
                    , "Bluetooth is not available"
                    , Toast.LENGTH_SHORT).show();
            finish();
        }

        bt.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            public void onDataReceived(byte[] data, String message) {
                Snackbar.make(coordinatorLayout,message +"\n", Snackbar.LENGTH_SHORT).show();
            }
        });

        bt.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            public void onDeviceDisconnected() {
                btnPrint.setVisibility(View.GONE);
                Snackbar.make(coordinatorLayout,"Status : Not connect", Snackbar.LENGTH_INDEFINITE).show();
            }

            public void onDeviceConnectionFailed() {
                btnPrint.setVisibility(View.GONE);
                Snackbar.make(coordinatorLayout,"Status : Connection failed", Snackbar.LENGTH_INDEFINITE).show();
            }

            public void onDeviceConnected(String name, String address) {
                btnPrint.setVisibility(View.VISIBLE);
                Snackbar.make(coordinatorLayout,"Status : Connected to " + name, Snackbar.LENGTH_INDEFINITE).show();
            }
        });
    }


    void InitControl(){
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        coordinatorLayout = findViewById(R.id.main_content);

        NestedScrollView scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        tglbayar = findViewById(R.id.txt_waktu);
        noref = findViewById(R.id.txt_noref);
        nopel = findViewById(R.id.txt_idpel);
        nama = findViewById(R.id.txt_nama);
        harga = findViewById(R.id.txt_harga);
        totaltagihan = findViewById(R.id.txt_totaltagihan);
        biayaadmin = findViewById(R.id.txt_admin);
        tagihanbulan = findViewById(R.id.txt_tagihanbulan);

        btnPrint = findViewById(R.id.btn_print);

        btnPrint.setVisibility(View.GONE);
        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupPrint();
            }
        });
    }

    void getData(){
        Intent mIntent = getIntent();
        MSG = mIntent.getStringExtra("MSG");
        TGL_BAYAR = mIntent.getStringExtra("TGL_BAYAR");
        IDPEL = mIntent.getStringExtra("IDPEL");
        NOREF = mIntent.getStringExtra("NOREF");
        NAMA = mIntent.getStringExtra("NAMA");
        TAGIHAN_BULAN = mIntent.getStringExtra("TAGIHAN_BULAN");
        HARGA = mIntent.getStringExtra("HARGA");
        BIAYA_ADMIN = mIntent.getStringExtra("BIAYA_ADMIN");
        TOTAL_TAGIHAN = mIntent.getStringExtra("TOTAL_TAGIHAN");
        PRODUCT = mIntent.getStringExtra("PRODUCT");
        RECEIPT = mIntent.getStringExtra("RECEIPT");

        tglbayar.setText(TGL_BAYAR);
        nopel.setText(IDPEL);
        noref.setText(NOREF);
        nama.setText(NAMA);
        harga.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(HARGA)));
        biayaadmin.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(BIAYA_ADMIN)));
        totaltagihan.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTAL_TAGIHAN)));
        tagihanbulan.setText(TAGIHAN_BULAN);
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_struk, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.device_connect:
                bt.setDeviceTarget(BluetoothState.DEVICE_OTHER);
                Intent intent = new Intent(getApplicationContext(), DeviceList.class);
                startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
                break;

        }
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if(resultCode == Activity.RESULT_OK)
                bt.connect(data);
        } else if(requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if(resultCode == Activity.RESULT_OK) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_ANDROID);
            } else {
                Snackbar.make(coordinatorLayout,"Bluetooth was not enabled.", Snackbar.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bt.stopService();
    }

    @Override
    public void onStop() {
        super.onStop();
        bt.stopService();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!bt.isBluetoothEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, BluetoothState.REQUEST_ENABLE_BT);
        } else {
            if(!bt.isServiceAvailable()) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_ANDROID);
            }
        }
    }

    private void setupData() {
        data = new StringBuilder();
        data.append("SMARTPAY \n");
        data.append(tglbayar.getText().toString());
        data.append("\n\n");
        data.append(String.format("%-10s : %s", "NO. REF", NOREF));
        data.append("\n");
        data.append(String.format("%-10s : %s", "NO. PELANGGAN", IDPEL));
        data.append("\n");
        data.append(String.format("%-10s : %s", "NAMA PELANGGAN", NAMA));
        data.append("\n");
        data.append(String.format("%-10s : %s", "TAGIHAN BULAN", TAGIHAN_BULAN));
        data.append("\n");
        data.append(String.format("%-10s : %s", "TOTAL TAGIHAN", TOTAL_TAGIHAN));
        data.append("\n");
        data.append(String.format("%-10s : %s", "BIAYA ADMIN", BIAYA_ADMIN));
        data.append("\n");
        data.append(String.format("%-10s : %s", "TOTAL BAYAR", HARGA));
        data.append("\n\n");
        data.append("Terima Kasih\n");
        data.append("Simpan struk ini sebagai bukti pembayaran yang sah\r\n");
    }

    private void setupPrint(){
        try {
            bt.send(data.toString(),true);
        } catch (Exception e) {
            errorDialog(e.toString());
        }
    }

    private void errorDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                }).show();
    }
}
