package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ItemTransHistoryAdapter;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_TransHistory;
import com.mayora.mypaydrc.net.NetworkManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 27/05/2017.
 */

public class TransaksiFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private Button btnSearch, btnAdvice;
    private EditText editDate, editNo;
    private TextView txtNo, txtDate;
    private RecyclerView recyclerView;

    private ProgressDialog progressDialog;
    private Obj_TransHistory obj_transHistory, obj_transHistoryFull ;
    private RecyclerView.Adapter mAdapter;

    private LinearLayoutManager mLayoutManager;
    SwipeRefreshLayout swipeRefreshLayout;

    int page = 1;
    int total_page = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_trans, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        InitControl(view);

        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker().show();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editDate.getText().length() == 0){
                    editDate.setError(getString(R.string.error_field_required));
                    editDate.requestFocus();
                }else{
                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editNo.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    FillGrid();
                }

            }
        });

        btnAdvice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Advice_PDAM.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });


    }

    private void InitControl(View v) {
        float scale = getResources().getConfiguration().fontScale;
        float sizeNeeded = (float) 14.0;

        txtDate = (TextView) v.findViewById(R.id.txt_date);
        txtDate.setTextSize(sizeNeeded/scale);
        txtNo = (TextView) v.findViewById(R.id.txt_no);
        txtNo.setTextSize(sizeNeeded/scale);
        editDate = (EditText) v.findViewById(R.id.editDate);
        editDate.setText(AppController.getInstance().getDate());
        editDate.setTextSize(sizeNeeded/scale);
        editNo = (EditText) v.findViewById(R.id.editNo);
        editNo.setTextSize(sizeNeeded/scale);
        btnSearch = (Button) v.findViewById(R.id.btn_search);
        btnSearch.setTextSize(sizeNeeded/scale);
        btnAdvice = (Button) v.findViewById(R.id.btn_advice);
        btnAdvice.setTextSize(sizeNeeded/scale);

        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0) //check for scroll down
                {

                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        if (page <= total_page) {
                            FillGridMore();
                            page = page + 1;
                        }
                    }
                }
            }
        });
    }

    void FillGrid(){
        page = 1;

        progressDialog = ProgressDialog.show(getActivity(),"", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("history", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_TransHistory param = new Obj_TransHistory(AppConfig.USERID, AppConfig.REQID, editDate.getText().toString(), page, editNo.getText().toString(), AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_TransHistory> call = NetworkManager.getNetworkService(getActivity()).getTransHistory(param);
            call.enqueue(new Callback<Obj_TransHistory>() {
                @Override
                public void onResponse(Call<Obj_TransHistory> call, Response<Obj_TransHistory> response) {
                    int code = response.code();
                    progressDialog.dismiss();

                    if (code == 200){
                        obj_transHistory = response.body();
                        if (obj_transHistory.rc.equals("00")){
                            if (obj_transHistory.data == null || obj_transHistory.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "History Transaksi tidak ditemukan!");
                            }else{
                                total_page = Integer.valueOf(obj_transHistory.total_paging);
                                obj_transHistoryFull = obj_transHistory;
                                FillAdapter();

                                page=page+1;
                            }
                        }else if (obj_transHistory.rc.equals("73")){
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_transHistory.message);
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                        }else if (obj_transHistory.rc.equals("78")){
                            recyclerView.setAdapter(null);
                            updateDeviceDialog(obj_transHistory.message);
                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_transHistory.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_TransHistory> call, Throwable t) {
                    progressDialog.dismiss();
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
            recyclerView.setAdapter(null);
        }
    }

    void FillGridOnRefresh(){
        page = 1;

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("history", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_TransHistory param = new Obj_TransHistory(AppConfig.USERID, AppConfig.REQID, editDate.getText().toString(), page, editNo.getText().toString(), AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_TransHistory> call = NetworkManager.getNetworkService(getActivity()).getTransHistory(param);
            call.enqueue(new Callback<Obj_TransHistory>() {
                @Override
                public void onResponse(Call<Obj_TransHistory> call, Response<Obj_TransHistory> response) {
                    int code = response.code();
                    swipeRefreshLayout.setRefreshing(false);
                    if (code == 200){
                        obj_transHistory = response.body();
                        if (obj_transHistory.rc.equals("00")){
                            if (obj_transHistory.data == null || obj_transHistory.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "History Transaksi tidak ditemukan!");
                            }else{
                                total_page = Integer.valueOf(obj_transHistory.total_paging);
                                obj_transHistoryFull = obj_transHistory;
                                FillAdapter();

                                page=page+1;
                            }
                        }else if (obj_transHistory.rc.equals("73")){
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_transHistory.message);
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                        }else if (obj_transHistory.rc.equals("78")){
                            recyclerView.setAdapter(null);
                            updateDeviceDialog(obj_transHistory.message);
                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_transHistory.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_TransHistory> call, Throwable t) {
                    swipeRefreshLayout.setRefreshing(false);
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            swipeRefreshLayout.setRefreshing(false);
            recyclerView.setAdapter(null);
        }
    }

    void FillGridMore(){
        final ProgressDialog progressDialogMore = ProgressDialog.show(getActivity(),null, null, true);
        progressDialogMore.getWindow().setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
        progressDialogMore.setContentView( R.layout.progress_item );
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("history", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_TransHistory param = new Obj_TransHistory(AppConfig.USERID, AppConfig.REQID, editDate.getText().toString(), page, editNo.getText().toString(), AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_TransHistory> call = NetworkManager.getNetworkService(getActivity()).getTransHistory(param);
            call.enqueue(new Callback<Obj_TransHistory>() {
                @Override
                public void onResponse(Call<Obj_TransHistory> call, Response<Obj_TransHistory> response) {
                    int code = response.code();
                    if (code == 200){
                        progressDialogMore.dismiss();
                        obj_transHistory = response.body();
                        if (obj_transHistory.rc.equals("00")){
                            for(Obj_TransHistory.Datum dat : obj_transHistory.data){
                                obj_transHistoryFull.data.add(dat);
                            }

                            mAdapter.notifyDataSetChanged();
                        }else if (obj_transHistory.rc.equals("73")){
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_transHistory.message);
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                        }else if (obj_transHistory.rc.equals("78")){
                            recyclerView.setAdapter(null);
                            updateDeviceDialog(obj_transHistory.message);
                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_transHistory.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_TransHistory> call, Throwable t) {
                    recyclerView.setAdapter(null);
                    progressDialogMore.dismiss();
                }
            });
        }catch (Exception e){
            recyclerView.setAdapter(null);
            progressDialogMore.dismiss();
        }
    }

    void FillAdapter(){
        mAdapter = new ItemTransHistoryAdapter(getActivity(), obj_transHistoryFull, new ItemTransHistoryAdapter.OnDownloadClicked() {
            @Override
            public void OnDownloadClicked(String TRXID, String DATETIME, String NOPEL, String PROD_NAME, String PRICE, String SN, String STATUS, String FLAG) {
                if(FLAG.equals("PRINT")){
                    Intent mIntent = new Intent(getActivity(), PreviewStrukPulsa.class);
                    mIntent.putExtra("TRXID",TRXID);
                    mIntent.putExtra("WAKTU",DATETIME);
                    mIntent.putExtra("NOPEL",NOPEL);
                    mIntent.putExtra("PROD_NAME",PROD_NAME);
                    mIntent.putExtra("PRICE",PRICE);
                    mIntent.putExtra("SN",SN);
                    mIntent.putExtra("STATUS",STATUS);
                    startActivity(mIntent);

                }else{
                   shareData(TRXID, DATETIME, NOPEL, PROD_NAME, PRICE, SN, STATUS);

                }

            }
        });
        recyclerView.setAdapter(mAdapter);
    }

    private void shareData(String TRXID, String DATETIME, String NOPEL, String PROD_NAME, String PRICE, String SN, String STATUS) {
        StringBuilder data = new StringBuilder();
        data.append("BANK MAYORA \n");
        data.append("MYPAY \n");
        data.append(DATETIME);
        data.append("\n\n");
        data.append(String.format("%-11s : %s", "NO REF", TRXID));
        data.append("\n");
        data.append(String.format("%-10s : %s", "NO PELANGGAN", NOPEL));
        data.append("\n");
        data.append(String.format("%-9s : %s", "PRODUK", PROD_NAME));
        data.append("\n");
        data.append(String.format("%-10s : %s", "HARGA", AppController.getInstance().toCurrencyIDR(Double.parseDouble(PRICE))));
        data.append("\n");
        data.append(String.format("%-10s : %s", "STATUS", STATUS));
        data.append("\n");
        data.append(String.format("%-16s : %s", "SN", SN));
        data.append("\n\n");
        data.append("TERIMA KASIH \n");
        data.append("CALL CENTER (021) 5655288 \n");

        try{
            convertImage(data.toString(), TRXID);
        }catch (Exception e){
            e.printStackTrace();
        }

        File outputFile = new File(AppConfig.STORAGE_CARD + "/Download/" + TRXID + ".png");
        Uri uri = Uri.fromFile(outputFile);

        if (outputFile.exists()) {
            Intent share = new Intent();
            share.setAction(Intent.ACTION_SEND);
            share.setType("image/*");
            share.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(share, "Kirim Struk"));
        }else{
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, data.toString());
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Kirim Struk"));
        }
    }

    private DatePickerDialog showDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = new DatePickerDialog(getActivity(), mDateSetListener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));

        return dpd;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        // onDateSet method
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String sDay, sMonth, sYear;
            sDay = String.valueOf(dayOfMonth);
            if (sDay.length() < 2) sDay = "0" + sDay;

            sMonth = String.valueOf(monthOfYear+1);
            if (sMonth.length() < 2) sMonth = "0" + sMonth;

            sYear = String.valueOf(year);
            String sDate = sYear + "-" + sMonth + "-" + sDay;
            editDate.setText(sDate);
        }
    };

    @Override
    public void onRefresh() {
        FillGridOnRefresh();
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

    void convertImage(final String text, String fname) throws IOException {
        Log.d("ConvertImage", fname);
        final Rect bounds = new Rect();
        TextPaint textPaint = new TextPaint() {
            {
                setColor(Color.BLACK);
                setTextAlign(Paint.Align.LEFT);
                setTextSize(22f);
                setAntiAlias(true);
            }
        };
        textPaint.getTextBounds(text, 0, text.length(), bounds);
        StaticLayout mTextLayout = new StaticLayout(text, textPaint,
                bounds.width(), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        int maxWidth = -1;
        for (int i = 0; i < mTextLayout.getLineCount(); i++) {
            if (maxWidth < mTextLayout.getLineWidth(i)) {
                maxWidth = (int) mTextLayout.getLineWidth(i);
            }
        }
        final Bitmap bmp = Bitmap.createBitmap(maxWidth , mTextLayout.getHeight(),
                Bitmap.Config.ARGB_8888);
        bmp.eraseColor(Color.WHITE);// just adding black background
        final Canvas canvas = new Canvas(bmp);
        mTextLayout.draw(canvas);
        FileOutputStream stream = new FileOutputStream(AppConfig.STORAGE_CARD + "/Download/" + fname + ".png");
        bmp.compress(Bitmap.CompressFormat.PNG, 85, stream);
        bmp.recycle();
        stream.close();
    }

}
