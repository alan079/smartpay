package com.mayora.mypaydrc.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ItemPromoAdapter;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Promo;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 27/05/2017.
 */

public class PromoFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;

    ProgressDialog progressDialog;
    private Obj_Promo obj_promo;
    private ItemPromoAdapter mAdapter;

    private LinearLayoutManager mLayoutManager;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_promo, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() ) {
            FillGrid();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() ) {
            FillGrid();
        }
    }

    private void InitControl(View v) {

        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    void FillGrid(){
        progressDialog = ProgressDialog.show(getActivity(),"", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("promosi", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_Promo param = new Obj_Promo(AppConfig.IMEI, AppConfig.KEY, AppConfig.REQID, AppConfig.USERID, version);

            Call<Obj_Promo> call = NetworkManager.getNetworkService(getActivity()).getPromo(param);
            call.enqueue(new Callback<Obj_Promo>() {
                @Override
                public void onResponse(Call<Obj_Promo> call, Response<Obj_Promo> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        obj_promo = response.body();
                        if (obj_promo.rc.equals("00")){
                            if (obj_promo.data == null || obj_promo.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "Promo tidak ditemukan!");
                            }else{
                                FillAdapter();

                            }
                        }else if (obj_promo.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_promo.message);

                        }else if (obj_promo.rc.equals("78")){
                            updateDeviceDialog(obj_promo.message);

                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_promo.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_Promo> call, Throwable t) {
                    progressDialog.dismiss();
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
            recyclerView.setAdapter(null);
        }
    }

    void FillGridOnRefresh(){
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("promosi", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_Promo param = new Obj_Promo(AppConfig.IMEI, AppConfig.KEY, AppConfig.REQID, AppConfig.USERID, version);

            Call<Obj_Promo> call = NetworkManager.getNetworkService(getActivity()).getPromo(param);
            call.enqueue(new Callback<Obj_Promo>() {
                @Override
                public void onResponse(Call<Obj_Promo> call, Response<Obj_Promo> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        obj_promo = response.body();
                        if (obj_promo.rc.equals("00")){
                            if (obj_promo.data == null || obj_promo.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "Promo tidak ditemukan!");
                            }else{
                                FillAdapter();
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        }else if (obj_promo.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_promo.message);

                        }else if (obj_promo.rc.equals("78")){
                            updateDeviceDialog(obj_promo.message);

                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_promo.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_Promo> call, Throwable t) {
                    swipeRefreshLayout.setRefreshing(false);
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            swipeRefreshLayout.setRefreshing(false);
            recyclerView.setAdapter(null);
        }

    }

    void FillAdapter(){
        mAdapter = new ItemPromoAdapter(getActivity(), obj_promo, new ItemPromoAdapter.OnDownloadClicked() {
            @Override
            public void OnDownloadClicked() {

            }
        });
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onRefresh() {
        FillGridOnRefresh();
    }


    private void updateDeviceDialog(String msg){
        @SuppressLint("RestrictedApi")
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.AlertDialog));
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

}
