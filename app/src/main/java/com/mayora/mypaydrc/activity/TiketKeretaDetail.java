package com.mayora.mypaydrc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Payment_TiketKereta;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TiketKeretaDetail extends AppCompatActivity {

    private TextView txtKodebayar, txtNama, txtNamaKereta, txtNoKereta, txtNoKursi, txtDari, txtTujuan, txtTglBrgkt,
            txtJamBrgkt, txtJmlPenumpang, txtHarga, txtAdmin, txtTotalTagihan;
    private Button btnBayar;
    private LinearLayout linearLayout;

    ProgressDialog progress;
    Obj_Payment_TiketKereta objPaymentTiketKereta;
    private String sSubscriber = "", sProduct = "", sInquiry_num = "";
    private String MSG = "", TGL_BAYAR = "", NOPEL = "", NOREF = "", INQUIRYNUM = "", KODE_BAYAR = "", NAMA= "", HARGA ="", TOTAL_TAGIHAN="", BIAYA_ADMIN="", JML_PENUMPANG= "", DARI = "",
            TUJUAN = "", TGL_BRGKT = "", JAM_BRGKT = "", NAMA_KERETA = "", NO_KERETA = "", NO_KURSI = "", PRODUCT = "", RECEIPT= "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tiket_detail);

        InitControl();
        GetExtrasData();

        btnBayar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Payment();
            }
        });
    }

    void InitControl(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        NestedScrollView scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        linearLayout = findViewById(R.id.layout_detail);

        txtKodebayar = findViewById(R.id.txt_kodebayar);
        txtNama = findViewById(R.id.txt_nama);
        txtNamaKereta = findViewById(R.id.txt_namakereta);
        txtNoKereta = findViewById(R.id.txt_nokereta);
        txtNoKursi = findViewById(R.id.txt_nokursi);
        txtDari = findViewById(R.id.txt_dari);
        txtTujuan = findViewById(R.id.txt_tujuan);
        txtTglBrgkt = findViewById(R.id.txt_tglbrgkt);
        txtJamBrgkt = findViewById(R.id.txt_jambrgkt);
        txtJmlPenumpang = findViewById(R.id.txt_jmlpenumpang);
        txtHarga = findViewById(R.id.txt_harga);
        txtAdmin = findViewById(R.id.txt_admin);
        txtTotalTagihan = findViewById(R.id.txt_totaltagihan);

        btnBayar = findViewById(R.id.btnBayar);
    }

    void GetExtrasData(){

        Intent intent = getIntent();
        KODE_BAYAR = intent.getStringExtra("KODE_BAYAR");
        NAMA = intent.getStringExtra("NAMA");
        NAMA_KERETA = intent.getStringExtra("NAMA_KERETA");
        NO_KERETA = intent.getStringExtra("NO_KERETA");
        NO_KURSI = intent.getStringExtra("NO_KURSI");
        DARI = intent.getStringExtra("DARI");
        TUJUAN = intent.getStringExtra("TUJUAN");
        TGL_BRGKT = intent.getStringExtra("TGL_BRGKT");
        JAM_BRGKT = intent.getStringExtra("JAM_BRGKT");
        JML_PENUMPANG = intent.getStringExtra("JML_PENUMPANG");
        HARGA = intent.getStringExtra("HARGA");
        BIAYA_ADMIN = intent.getStringExtra("BIAYA_ADMIN");
        TOTAL_TAGIHAN = intent.getStringExtra("TOTAL_TAGIHAN");
        PRODUCT = intent.getStringExtra("PRODUCT");
        INQUIRYNUM = intent.getStringExtra("INQUIRYNUM");

        txtKodebayar.setText(KODE_BAYAR);
        txtNama.setText(NAMA);
        txtNamaKereta.setText(NAMA_KERETA);
        txtNoKereta.setText(NO_KERETA);
        txtNoKursi.setText(NO_KURSI);
        txtDari.setText(DARI);
        txtTujuan.setText(TUJUAN);
        txtTglBrgkt.setText(TGL_BRGKT);
        txtJamBrgkt.setText(JAM_BRGKT);
        txtJmlPenumpang.setText(JML_PENUMPANG);
        txtHarga.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(HARGA)));
        txtAdmin.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(BIAYA_ADMIN)));
        txtTotalTagihan.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTAL_TAGIHAN)));
    }

    private void Payment() {
        progress = ProgressDialog.show(this, "Info", "Checking data", true);

        sProduct = PRODUCT;
        String subscriber = txtKodebayar.getText().toString();
        sSubscriber = subscriber;
        String version = AppController.getInstance().getVersionName();
        String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(Pin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("payment", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Payment_TiketKereta param = new Obj_Payment_TiketKereta(AppConfig.IMEI, AppConfig.KEY, sProduct, AppConfig.REQID, sSubscriber, AppConfig.USERID, version, INQUIRYNUM);
            Call<Obj_Payment_TiketKereta> call = NetworkManager.getNetworkService(this).paymentTiketKereta(param);
            call.enqueue(new Callback<Obj_Payment_TiketKereta>() {
                @Override
                public void onResponse(Call<Obj_Payment_TiketKereta> call, Response<Obj_Payment_TiketKereta> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objPaymentTiketKereta = response.body();
                        if (objPaymentTiketKereta.rc.equals("00")) {

                            MSG = objPaymentTiketKereta.message;
                            TGL_BAYAR = objPaymentTiketKereta.display.TGL_BAYAR;
                            KODE_BAYAR = objPaymentTiketKereta.display.NOPEL;
                            NOREF = objPaymentTiketKereta.reffnum;
                            NAMA = objPaymentTiketKereta.display.NAMA;
                            NAMA_KERETA = objPaymentTiketKereta.display.NAMA_KERETA;
                            NO_KERETA = objPaymentTiketKereta.display.NO_KERETA;
                            NO_KURSI = objPaymentTiketKereta.display.NO_KURSI;
                            DARI = objPaymentTiketKereta.display.DARI;
                            TUJUAN = objPaymentTiketKereta.display.TUJUAN;
                            TGL_BRGKT = objPaymentTiketKereta.display.TGL_BRGKT;
                            JAM_BRGKT = objPaymentTiketKereta.display.JAM_BRGKT;
                            JML_PENUMPANG = objPaymentTiketKereta.display.JML_PENUMPANG;
                            HARGA = objPaymentTiketKereta.display.HARGA;
                            BIAYA_ADMIN = objPaymentTiketKereta.display.BIAYA_ADMIN;
                            TOTAL_TAGIHAN = objPaymentTiketKereta.display.TOTAL_TAGIHAN;
                            PRODUCT = objPaymentTiketKereta.product;
                            RECEIPT = objPaymentTiketKereta.receipt;

                            Intent intent = new Intent(TiketKeretaDetail.this, TiketKeretaStatus.class);
                            intent.putExtra("MSG", MSG);
                            intent.putExtra("TGL_BAYAR",TGL_BAYAR);
                            intent.putExtra("KODE_BAYAR",KODE_BAYAR);
                            intent.putExtra("NOREF",NOREF);
                            intent.putExtra("NAMA",NAMA);
                            intent.putExtra("NAMA_KERETA",NAMA_KERETA);
                            intent.putExtra("NO_KERETA",NO_KERETA);
                            intent.putExtra("NO_KURSI",NO_KURSI);
                            intent.putExtra("DARI",DARI);
                            intent.putExtra("TUJUAN",TUJUAN);
                            intent.putExtra("TGL_BRGKT",TGL_BRGKT);
                            intent.putExtra("JAM_BRGKT",JAM_BRGKT);
                            intent.putExtra("JML_PENUMPANG",JML_PENUMPANG);
                            intent.putExtra("HARGA",HARGA);
                            intent.putExtra("BIAYA_ADMIN",BIAYA_ADMIN);
                            intent.putExtra("TOTAL_TAGIHAN",TOTAL_TAGIHAN);
                            intent.putExtra("PRODUCT",PRODUCT);
                            intent.putExtra("RECEIPT", RECEIPT);
                            startActivity(intent);

                        } else {
                            AppController.getInstance().errorDialog(TiketKeretaDetail.this, objPaymentTiketKereta.message);
                        }
                    } else {
                        Toast.makeText(TiketKeretaDetail.this, "Inquiry gagal!", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Obj_Payment_TiketKereta> call, Throwable t) {
                    progress.dismiss();
                    Toast.makeText(TiketKeretaDetail.this, "Inquiry gagal!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            Toast.makeText(TiketKeretaDetail.this, "Inquiry gagal!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //optionmenu
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                break;
        }
        return true;
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
