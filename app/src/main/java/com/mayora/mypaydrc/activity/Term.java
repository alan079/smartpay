package com.mayora.mypaydrc.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;

import com.mayora.mypaydrc.R;

/**
 * Created by eroy on 08/08/2017.
 */

public class Term extends AppCompatActivity {

    private WebView webView;
    private Toolbar mToolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.term);

        InitControl();

        String term = terms();
        webView.loadData(term,"text/html", "utf-8");
    }

    private void InitControl(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        webView = (WebView) findViewById(R.id.web_content);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
    };

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            //optionmenu
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                break;

        }
        return true;
    }

    private String terms(){
        String terms = "<p style=\"text-align: justify; font-size: 9pt;\">\n" +
                "1. Pemilik Aplikasi ini Adalah BANK MAYORA. </br>\n" +
                "2. Dengan menginstal Aplikasi ini, menggunakan setiap bagian dalam Aplikasi atau setiap konten dan fasilitas pada Aplikasi ini. </br>\n" +
                "3. Pengguna dapat membeli voucher atau melakukan pembayaran melalui SmartPay Mayora Mobile Application. </br>\n" +
                "4. Pihak Pengguna harus jujur dan akurat, serta mengemban tanggungjawab sepenuhnya dalam Resellerikan informasi mengenai diri pribadi atau perusahaan kepada pihak SmartPay Mayora. </br>\n" +
                "5. SmartPay Mayora menjaga komitmen terhadap para Pengguna dengan menghormati dan menjamin keamanan data pribadi para pengguna SmartPay Mayora Mobile Application. Informasi yang Pengguna berikan kepada SmartPay Mayora akan dipergunakan untuk menjaga keamanan penggunaan User ID Pengguna. </br>\n" +
                "6. Jika Harga yang tertera pada SmartPay Mayora Mobile Application terdapat perubahan harga maka pihak SmartPay Mayora akan melakukan pemberitahuan terlebih dahulu.</br></p>\n" +
                "\n" +
                "DEFINISI</br>\n" +
                "<p style=\"text-align: justify; font-size: 9pt;\">\n" +
                "1. Pengguna adalah Pihak perorangan atau perusahaan yang telah terdaftar secara administrasi dalam kaitannya dengan penggunaan ke fitur dan fasilitas yang ada pada SmartPay Mayora Mobile Application yang disebut Reseller. Reseller harus jujur dan akurat, serta mengemban tanggungjawab sepenuhnya dalam memberikan informasi mengenai diri pribadi. Reseller yang tidak jujur dan tidak akurat dalam memberikan informasi mengenai diri pribadi dianggap oleh SmartPay Mayora sebagai pihak yang beritikad tidak baik dan SmartPay Mayora dapat melaporkan pihak yang bersangkutan kepada instansi pemerintah yang berwenang apabila dikemudian hari timbul akibat hukum dan akibat lainnya dari Reseller yang tidak jujur dan tidak akurat. SmartPay Mayora tidak bertanggungjawab atas akibat hukum dan akibat lainnya yang timbul akibat pemberian informasi yang tidak jujur dan tidak akurat oleh Reseller. Apabila Reseller telah bertindak secara jujur dan akurat dalam memberikan informasi mengenai diri pribadi, SmartPay Mayora tidak bertanggungjawab atas Penggunaan SmartPay Mayora Mobile Application oleh Reseller atau pihak lain yang diberikan kuasa oleh Reseller yang meliputi transaksi pembelian dan pembayaran atau penggunaan tidak terbatas pada kegiatan Penggunaan SmartPay Mayora Mobile Application terkait hak kekayaan intelektual lain pada SmartPay Mayora Mobile Application, interaksi lainnya yang terjadi melalui SmartPay Mayora Mobile Application diluar dari pembelian dan transaksi yang menggunakan fasilitas SmartPay Mayora, komunikasi yang dilakukan menggunakan fasilitas SmartPay Mayora Mobile Application atau berkaitan dengan seluruh hal yang dimuat dalam SmartPay Mayora Mobile Application.</br>\n" +
                "2. SmartPay Mayora tidak bertanggungjawab atas Perjanjian yang dibuat oleh Reseller, dan tindakan Reseller yang bertentangan dengan hukum, kesusilaan, dan ketertiban umum.</br>\n" +
                "3. SmartPay Mayora tidak bertanggungjawab atas kesalahan input pada saat melakukan transaksi pembelian maupun pembayaran, pengiriman saldo transaksi dan Top Up Saldo.</br>\n" +
                "4. Supplier adalah seseorang atau perusahaan penyedia product yang digunakan atau dijual oleh pihak SmartPay Mayora. </br>\n" +
                "5. Force Majeure adalah suatu keadaan memaksa di luar batas kemampuan SmartPay Mayora, Supplier, dan Reseller yang dapat mengganggu bahkan menggagalkan terlaksananya kerjasama dan atau transaksi, seperti bencana alam, peperangan, pemogokan, sabotase, pemberontakan masyarakat, blokade, kebijaksanaan Pemerintah khususnya yang disebabkan karena keadaan di luar kemampuan manusia.</br></p>\n" +
                "\n" +
                "PERUBAHAN PERJANJIAN PENGGUNAAN</br>\n" +
                "<p style=\"text-align: justify; font-size: 9pt;\">\n" +
                "1. SmartPay Mayora berwenang untuk mengubah Perjanjian Penggunaan SmartPay Mayora Mobile Application setiap saat dan tanpa memerlukan ijin atau persetujuan dari Reseller. Pengubahan Perjanjian Penggunaan mengikat setelah dimuat dalam Situs. Pengubahan dan pemuatan Perjanjian Penggunaan dapat dilakukan oleh SmartPay Mayora tanpa melakukan pemberitahuan atau menjelaskan alasannya kepada Pengguna. SmartPay Mayora dapat menyimpan Perjanjian Penggunaan yang telah diubah, tetapi tidak berkewajiban untuk memuat Perjanjian Penggunaan yang telah diubah ke dalam Situs.</br>\n" +
                "2. Apabila ada ketidaksesuaian pemahaman atau tafsiran atas Perjanjian Penggunaan ini antara Pengguna dengan SmartPay Mayora, maka para pihak sepakat untuk mengadakan perundingan dan musyawarah untuk bermufakat, baik dengan cara tatap muka secara langsung atau dengan fasilitas teknologi informasi dan komunikasi mengenai maksud dan tujuan dari klausul yang dimaksud dalam Perjanjian Penggunaan. Adalah suatu kewajiban bagi para pihak untuk mematuhi bahwa penggunaan dan tujuan perundingan dan musyawarah tersebut adalah untuk menjaga keberlanjutan layanan SmartPay Mayora Mobile Application dan kemaslahatan seluruh Reseller SmartPay Mayora Mobile Application.</br></p>\n" +
                "\n" +
                "HAK CIPTA DAN MEREK DAGANG</br>\n" +
                "<p style=\"text-align: justify; font-size: 9pt;\">\n" +
                "1. Semuanya yang ada pada atau di dalam SmartPay Mayora Mobile Application ini, adalah milik eksklusif SmartPay Mayora atau digunakan dengan izin yang jelas dari hak cipta dan atau pemilik merek dagang. Apapun kegiatan menyalin, mendistribusikan, mengirim, posting, menghubungkan, atau memodifikasi dari SmartPay Mayora Mobile Application tanpa izin tertulis PT. Ciwaru Jaya Kusumah sangat dilarang. Pelanggaran terhadap kebijakan tentang pelanggaran hak cipta, merek dagang atau hak milik intelektual lain oleh Reseller ini, dapat mengakibatkan pengenaan sanksi terhadap Reseller tersebut sesuai dengan peraturan perundang-undangan yang berlaku di Indonesia. Kecuali dinyatakan lain. </br>\n" +
                "2. SmartPay Mayora adalah merek dagang dari PT. Ciwaru Jaya Kusumah, dengan ini semua hak merek dagang yang berkaitan dengan merek dagang tersebut adalah jelas dimiliki oleh PT. Ciwaru Jaya Kusumah. Kecuali jika dinyatakan lain, dan semua merek dagang lainnya yang muncul pada SmartPay Mayora Mobile Application adalah milik dari masing-masing pemilik merek dagang. </br>\n" +
                "3. Aplikasi ini mengandung materi berhak cipta, merek dagang dan informasi kepemilikan lainnya, termasuk, namun tidak terbatas pada, teks, perangkat lunak, foto, video, grafik, musik dan suara, dan seluruh isi Aplikasi ini dilindungi oleh hak cipta sebagai sebuah kolektif di bawah undang-undang hak cipta Indonesia. SmartPay Mayora memiliki hak cipta dalam pemilihan, koordinasi, pengaturan, penambahan dan peningkatan konten dan fasilitas tersebut, serta dalam konten asli untuk itu. </br> \n" +
                "4. Ketentuan-ketentuan ini kepentingan SmartPay Mayora, anak perusahaan, afiliasi, pedagang dan penyedia konten pihak ketiga dan pemegang lisensinya dan masing-masing harus mempunyai hak untuk menyatakan dan menegakkan ketentuan-ketentuan tersebut secara langsung atau atas nama sendiri.</br></p>\n" +
                "\n" +
                "PERANGKAT KERAS DAN PERANGKAT LUNAK </br>\n" +
                "<p style=\"text-align: justify; font-size: 9pt;\">\n" +
                "1. Reseller harus bertanggung jawab untuk penyediaan dan penggunaan semua telepon pintar atau Smartphone, perangkat lunak dan peralatan lainnya yang diperlukan untuk mengakses dan menggunakan SmartPay Mayora Mobile Application dan semua biaya yang timbul darinya. SmartPay Mayora tidak bertanggung jawab atas setiap kerusakan peralatan Pengguna yang dihasilkan dari penggunaan Aplikasi ini. </br>\n" +
                "2. Kami tidak bertanggung jawab atas semua kerusakan baik insidental, konsekuensial, khusus, hukuman, pen-contoh-an, langsung atau tidak langsung dalam bentuk apapun yang mungkin timbul dari atau berhubungan dengan penggunaan layanan, termasuk namun tidak terbatas pada kehilangan pendapatan, keuntungan, bisnis atau data, atau kerusakan akibat dari virus, worms, \"Trojan horse\" atau perangkat lunak atau materi perusak lain, atau komunikasi yang dilakukan oleh Reseller dari layanan, atau gangguan atau penangguhan dari layanan, tanpa memperhitungkan faktor-faktor yang menyebabkan gangguan yang atau penangguhan. SmartPay Mayora dapat mengubah, menghentikan layanan atau kertersediaannya kepada Reseller setiap saat, dan Reseller dapat menghentikan penggunaan Layanan setiap saat. </br></p>\n" +
                "\n" +
                "PENGGUNAAN APLIKASI, FASILITAS, INFORMASI DAN PRIVASI </br>\n" +
                "<p style=\"text-align: justify; font-size: 9pt;\">\n" +
                "1. SmartPay Mayora tidak dapat menjamin keamanan privasi atau informasi yang Reseller berikan melalui internet, pesan E-mail, SMS, Chating dari Reseller, dan SmartPay Mayora tidak bertanggung jawab atas semua kewajiban yang terkait dengan Penggunaan dari informasi oleh pihak lain. </br>\n" +
                "2. SmartPay Mayora tidak bertanggung jawab terhadap informasi yang digunakan oleh orang lain dari setiap informasi yang Reseller berikan kepada mereka dan Reseller harus hati-hati dalam memilih informasi pribadi yang Reseller berikan kepada orang lain melalui layanan SmartPay Mayora, dan</br>\n" +
                "3. SmartPay Mayora tidak bertanggung jawab atas isi pesan yang dikirim oleh Reseller lain melalui layanan SmartPay Mayora, dan SmartPay Mayora terlepas dari setiap dan semua kewajiban yang terkait dengan isi komunikasi yang dapat Reseller terima dari Reseller lain.</br> \n" +
                "4. Reseller tidak dapat menuntut SmartPay Mayora atau karyawannya untuk setiap kerusakan yang dilakukan kepada Reseller melalui Aplikasi SmartPay Mayora. </br>\n" +
                "5. SmartPay Mayora tidak dapat menjamin, dan tidak bertanggung jawab untuk mem-verifikasi, keakuratan informasi yang diberikan oleh Reseller kepada layanan lain selain SmartPay Mayora. </br>\n" +
                "6. Reseller tidak boleh menggunakan layanan kami untuk tujuan melanggar hukum. SmartPay Mayora dapat menolak untuk memberikan akun yang mengatasnamakan orang lain atau orang-orang tertentu yang dilindungi oleh undang-undang, merek dagang atau hak milik, atau secara tidak sopan atau menyinggung. </br>\n" +
                "7. SmartPay Mayora tidak bisa dan tidak dapat melihat keseluruhan pesan atau material yang diinput atau dikirim oleh setiap Reseller, dan SmartPay Mayora tidak bertanggung jawab terhadap setiap pesan dan material tersebut, SmartPay Mayora. memiliki hak yang bukanlah menjadi kewajiban SmartPay Mayora secara utuh, untuk menghapus, memindahkan, atau mengedit pesan atau material, termasuk di dalamnya adalah data produk, barang, dan jasa, semua informasi yang dipublikasikan dan pesan tanpa adanya batasan tertentu, apabila berdasarkan penilaian kami material dan pesan tersebut melanggar ketentuan yang telah diinformasikan diatas. </br>\n" +
                "8. Semua Reseller diwajibkan untuk mematuhi segala peraturan perundang-undangan yang berlaku di Indonesia khususnya yang tercantum di dalam Undang-Undang ITE, selama menggunakan layanan SmartPay Mayora Mobile Application. </br></p>\n" +
                "\n" +
                "PENGHAPUSAN AKSES</br>\n" +
                "<p style=\"text-align: justify; font-size: 9pt;\">\n" +
                "SmartPay Mayora berhak, dalam kebijaksanaan umum SmartPay Mayora, untuk membatalkan atau menghentikan sementara akses Reseller ke semua atau sebagian dari layanan sewaktu-waktu, dengan atau tanpa pemberitahuan, dengan segala alasan, termasuk, tanpa batasan,  dan akibat dari pelanggaran perjanjian ini. Tanpa membatasi keadaan umum yang terdahulu, segala bentuk kecurangan, atau aktivitas ilegal yang dapat dikenakan hukuman untuk pembatalan akses Reseller ke semua atau sebagian layanan berdasarkan Perjanjian Penggunaan layanan SmartPay Mayora Mobile Application, dan Reseller bisa saja diajukan kepada pihak yang berwajib.</br></p>\n" +
                "\n" +
                "PEMBATALAN KEANGGOTAAN</br>\n" +
                "<p style=\"text-align: justify; font-size: 9pt;\">\n" +
                "Setiap permintaan pembatalan akan sesegera mungkin dipandu oleh customer support kami secara tercatat, agar Reseller bisa segera membatalkan keanggotaan Reseller. Reseller menerima bahwa ketika Pengguna membatalkan keanggotaan Pengguna, segala layanan yang telah Reseller terima tidak akan dapat di gunakan kembali dan akses Reseller akan di hapus. Reseller juga menyetujui dan menerima kehilangan keseluruhan profil Reseller, E-mail dan segala macam informasi keanggotaan yang seharusnya Reseller terima. Informasi ini tidak bisa didapatkan kembali atau dipindah tangankan ke pihak ketiga. </p>\n" +
                "<p>&nbsp;</p>\n";

        return terms;
    }
}
