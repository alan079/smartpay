package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ItemNominalEmoneyAdapter;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Emoney;
import com.mayora.mypaydrc.model.Obj_Emoney_Nominal;
import com.mayora.mypaydrc.model.Obj_Jenis_Type;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmoneyFragment extends Fragment {
    private EditText editIDpel;
    private Button btnNext;
    private RelativeLayout relativeLayout;
    private Spinner spinJenis;

    ProgressDialog progressDialog;
    private Obj_Emoney objEmoney;
    private Obj_Emoney_Nominal objEmoneyNominal;
    private ArrayList<Obj_Jenis_Type> arrJenis = new ArrayList<Obj_Jenis_Type>();
    private ItemNominalEmoneyAdapter itemNominalEmoneyAdapter;
    private RecyclerView recyclerView;

    private String productCode = "";
    private String NO="", CATEGORY="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getArguments() != null) {
            NO = getArguments().getString("NO");
            CATEGORY = getArguments().getString("CATEGORY");
        }
        View view = inflater.inflate(R.layout.frag_emoney, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);

        if(CATEGORY != null) {
            if (CATEGORY.equals("8")) {
                editIDpel.setText(NO);
            }
        }
        editIDpel.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        try {
            FillJenis();
        }catch(Exception e){
            e.printStackTrace();
        }

        editIDpel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editIDpel.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    return true;
                }
                return false;
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // hide virtual keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editIDpel.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN);

                if(editIDpel.getText().length() > 0){
                    FillNominal();
                }

            }
        });

        spinJenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Obj_Jenis_Type jenis = (Obj_Jenis_Type) parent.getSelectedItem();
                productCode = jenis.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private void InitControl(View v) {
        relativeLayout = v.findViewById(R.id.layout_no);
        editIDpel = v.findViewById(R.id.edit_no);
        btnNext = v.findViewById(R.id.btnNext);
        spinJenis = v.findViewById(R.id.spin_jenis);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
    }

    void FillJenis(){

        progressDialog = ProgressDialog.show(getActivity(),"Informasi", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("emoney", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_Emoney param = new Obj_Emoney(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_Emoney> call = NetworkManager.getNetworkService(getActivity()).getEmoney(param);
            call.enqueue(new Callback<Obj_Emoney>() {
                @Override
                public void onResponse(Call<Obj_Emoney> call, Response<Obj_Emoney> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        String jName = "";
                        String jId = "";
                        objEmoney = response.body();
                        if (objEmoney.rc.equals("00")){
                            for (Obj_Emoney.Datum dat : objEmoney.data){
                                jName = dat.name;
                                jId = dat.product_category_code;
                                arrJenis.add(new Obj_Jenis_Type(jId, jName));
                            }
                            adapterJenis(arrJenis);

                        }else if (objEmoney.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            AppController.getInstance().errorDialog(getContext(), "Layanan Emoney tidak tersedia!");
                        }else if (objEmoney.rc.equals("78")){
                            updateDeviceDialog(objEmoney.message);
                        }else{
                            AppController.getInstance().errorDialog(getContext(), "Layanan Emoney tidak tersedia!");
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_Emoney> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
        }
    }

    void adapterJenis(ArrayList<Obj_Jenis_Type> data){
        ArrayAdapter<Obj_Jenis_Type> adapter = new ArrayAdapter<Obj_Jenis_Type>(getContext(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinJenis.setAdapter(adapter);
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);
        snackbar.show();
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

    void FillNominal(){
        recyclerView.setAdapter(null);

        progressDialog = ProgressDialog.show(getActivity(),"Informasi", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("emoney", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_Emoney_Nominal param = new Obj_Emoney_Nominal(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI, productCode);

            Call<Obj_Emoney_Nominal> call = NetworkManager.getNetworkService(getActivity()).getEmoneyNominal(param);
            call.enqueue(new Callback<Obj_Emoney_Nominal>() {
                @Override
                public void onResponse(Call<Obj_Emoney_Nominal> call, Response<Obj_Emoney_Nominal> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        objEmoneyNominal = response.body();
                        if (objEmoneyNominal.rc.equals("00")){
                            if (objEmoneyNominal.data == null || objEmoneyNominal.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "Emoney tidak tersedia!");
                            }else{
                                FillAdapterNominal();
                            }
                        }else if (objEmoneyNominal.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), "Emoney tidak tersedia!");
                        }else if (objEmoneyNominal.rc.equals("78")){
                            recyclerView.setAdapter(null);
                            updateDeviceDialog(objEmoneyNominal.message);
                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), "Emoney tidak tersedia!");
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_Emoney_Nominal> call, Throwable t) {
                    progressDialog.dismiss();
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
            recyclerView.setAdapter(null);
        }
    }

    void FillAdapterNominal(){
        itemNominalEmoneyAdapter = new ItemNominalEmoneyAdapter(getActivity(), objEmoneyNominal, new ItemNominalEmoneyAdapter.OnDownloadClicked() {
            @Override
            public void OnDownloadClicked(String NOMINAL, String HARGA, String PROD_ID, String PROD_NAME) {
                Intent mIntent = new Intent(getContext(), PulsaDetail.class);
                mIntent.putExtra("NOMINAL",NOMINAL);
                mIntent.putExtra("HARGA",HARGA);
                mIntent.putExtra("NOPEL",editIDpel.getText().toString());
                mIntent.putExtra("PROD_NAME",PROD_NAME);
                mIntent.putExtra("PROD_ID",PROD_ID);
                startActivity(mIntent);

            }
        });
        recyclerView.setAdapter(itemNominalEmoneyAdapter);
    }

}
