package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_ListFavorite_Add;
import com.mayora.mypaydrc.net.NetworkManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CitilinkStatus extends AppCompatActivity {

    private Button btnFinish;
    private ImageView btnPrint, btnShare, btnFavorite;
    private TextView status, nopel, noref, reffid, nama, harga, totaltagihan, biayaadmin, jmlpenumpang, dari, tujuan, tglbrgkt,
            jambrgkt, noterbang, jmlterbang, kelas, carrier;
    private String MSG = "", TGL_BAYAR = "", NOREF = "", REFFID = "", NOPEL = "", NAMA= "", HARGA ="", TOTAL_TAGIHAN="", BIAYA_ADMIN="", JML_PENUMPANG= "", DARI = "",
            TUJUAN = "", TGL_BRGKT = "", JAM_BRGKT = "", NO_TERBANG = "", JML_TERBANG= "", KELAS= "", CARRIER = "", PRODUCT = "", RECEIPT = "";

    private ProgressDialog pDialog;
    ProgressDialog progress;
    public static final int progress_bar_type = 0;

    Obj_ListFavorite_Add obj_listFavorite_add;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.citilink_status);

        InitControl();
        LoadData();

        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.image_click));

                if (RECEIPT.equals("")) {
                    Intent intent = new Intent(getApplicationContext(), PreviewStrukCitilink.class);
                    intent.putExtra("NOPEL",NOPEL);
                    intent.putExtra("NOREF",NOREF);
                    intent.putExtra("NAMA",NAMA);
                    intent.putExtra("NO_TERBANG",NO_TERBANG);
                    intent.putExtra("JML_TERBANG",JML_TERBANG);
                    intent.putExtra("KELAS",KELAS);
                    intent.putExtra("CARRIER",CARRIER);
                    intent.putExtra("DARI",DARI);
                    intent.putExtra("TUJUAN",TUJUAN);
                    intent.putExtra("TGL_BRGKT",TGL_BRGKT);
                    intent.putExtra("JAM_BRGKT",JAM_BRGKT);
                    intent.putExtra("JML_PENUMPANG",JML_PENUMPANG);
                    intent.putExtra("HARGA",HARGA);
                    intent.putExtra("BIAYA_ADMIN",BIAYA_ADMIN);
                    intent.putExtra("TOTAL_TAGIHAN",TOTAL_TAGIHAN);
                    intent.putExtra("PRODUCT",PRODUCT);
                    startActivity(intent);
                }else{
                    DownloadStruk(RECEIPT);
                }

            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.image_click));

                if (RECEIPT.equals("")){
                    shareData();
                }else{
                    shareDataPdf(RECEIPT);
                }

            }
        });

        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.image_click));

                addFavoriteDialog();
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(getApplicationContext(), Main.class);
                startActivity(intent);
            }
        });
    }


    void InitControl(){

        NestedScrollView scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        status = findViewById(R.id.txt_status);
        noref = findViewById(R.id.txt_noref);
        nopel = findViewById(R.id.txt_kodebooking);
        nama = findViewById(R.id.txt_nama);
        harga = findViewById(R.id.txt_harga);
        totaltagihan = findViewById(R.id.txt_totaltagihan);
        biayaadmin = findViewById(R.id.txt_admin);
        jmlpenumpang = findViewById(R.id.txt_jmlpenumpang);
        dari = findViewById(R.id.txt_dari);
        tujuan = findViewById(R.id.txt_tujuan);
        tglbrgkt = findViewById(R.id.txt_tglbrgkt);
        jambrgkt = findViewById(R.id.txt_jambrgkt);
        noterbang = findViewById(R.id.txt_no_terbang);
        jmlterbang = findViewById(R.id.txt_jml_terbang);
        kelas = findViewById(R.id.txt_kelas);
        carrier = findViewById(R.id.txt_carrier);

        btnPrint = findViewById(R.id.btn_print);
        btnShare = findViewById(R.id.btn_share);
        btnFavorite = findViewById(R.id.btn_favorite);
        btnFinish = findViewById(R.id.btn_finish);
    }

    void LoadData(){
        Intent mIntent = getIntent();
        MSG = mIntent.getStringExtra("MSG");
        TGL_BAYAR = mIntent.getStringExtra("TGL_BAYAR");
        NOPEL = mIntent.getStringExtra("NOPEL");
        NOREF = mIntent.getStringExtra("NOREF");
        NAMA = mIntent.getStringExtra("NAMA");
        NO_TERBANG = mIntent.getStringExtra("NO_TERBANG");
        JML_TERBANG = mIntent.getStringExtra("JML_TERBANG");
        KELAS = mIntent.getStringExtra("KELAS");
        CARRIER =mIntent.getStringExtra("CARRIER");
        DARI = mIntent.getStringExtra("DARI");
        TUJUAN = mIntent.getStringExtra("TUJUAN");
        TGL_BRGKT = mIntent.getStringExtra("TGL_BRGKT");
        JAM_BRGKT = mIntent.getStringExtra("JAM_BRGKT");
        JML_PENUMPANG = mIntent.getStringExtra("JML_PENUMPANG");
        HARGA = mIntent.getStringExtra("HARGA");
        BIAYA_ADMIN = mIntent.getStringExtra("BIAYA_ADMIN");
        TOTAL_TAGIHAN = mIntent.getStringExtra("TOTAL_TAGIHAN");
        PRODUCT = mIntent.getStringExtra("PRODUCT");
        RECEIPT = mIntent.getStringExtra("RECEIPT");

        status.setText(MSG);
        if(MSG.toUpperCase().contains("SUKSES")){
            status.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.md_green_500));
        }else if(MSG.toUpperCase().contains("GAGAL")){
            status.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.md_red_500));
        }else {
            status.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.md_yellow_800));
        }
        nopel.setText(NOPEL);
        noref.setText(NOREF);
        nama.setText(NAMA);
        harga.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(HARGA)));
        biayaadmin.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(BIAYA_ADMIN)));
        totaltagihan.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTAL_TAGIHAN)));
        jmlpenumpang.setText(JML_PENUMPANG);
        dari.setText(DARI);
        tujuan.setText(TUJUAN);
        tglbrgkt.setText(TGL_BRGKT);
        jambrgkt.setText(JAM_BRGKT);
        noterbang.setText(NO_TERBANG);
        jmlterbang.setText(JML_TERBANG);
        kelas.setText(KELAS);
        carrier.setText(CARRIER);
    }


    void favoriteAdd(){

        String decryptPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decryptPin);
        Log.e("PIN", sPin);

        progress = ProgressDialog.show(this,"", "Proses Tambah Favorit", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("favorit", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();
        String action = "add";

        try{
            Obj_ListFavorite_Add param = new Obj_ListFavorite_Add(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI, action, "3", PRODUCT, HARGA, NOPEL, NAMA );

            Call<Obj_ListFavorite_Add> call = NetworkManager.getNetworkService(this).getListFavoriteAdd(param);
            call.enqueue(new Callback<Obj_ListFavorite_Add>() {
                @Override
                public void onResponse(Call<Obj_ListFavorite_Add> call, Response<Obj_ListFavorite_Add> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200){
                        obj_listFavorite_add = response.body();
                        if (obj_listFavorite_add.rc.equals("00")){
                            AppController.getInstance().errorDialog(CitilinkStatus.this, obj_listFavorite_add.message);

                        }else if (obj_listFavorite_add.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                            AppController.getInstance().errorDialog(CitilinkStatus.this, obj_listFavorite_add.message);

                        }else if (obj_listFavorite_add.rc.equals("78")){
                            updateDeviceDialog(obj_listFavorite_add.message);

                        }else{
                            AppController.getInstance().errorDialog(CitilinkStatus.this, obj_listFavorite_add.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_ListFavorite_Add> call, Throwable t) {
                    progress.dismiss();
                }
            });
        }catch (Exception e){
            progress.dismiss();
        }
    }

    private void shareData() {
        StringBuilder data = new StringBuilder();
        data.append("BANK MAYORA \n");
        data.append("SMARTPAY \n\n");
        data.append(String.format("%-10s : %s", "TGL. BAYAR", TGL_BAYAR));
        data.append("\n\n");
        data.append(String.format("%-10s : %s", "NO. REF", NOREF));
        data.append("\n");
        data.append(String.format("%-10s : %s", "NO. PELANGGAN", NOPEL));
        data.append("\n");
        data.append(String.format("%-10s : %s", "NAMA PELANGGAN", NAMA));
        data.append("\n");
        data.append(String.format("%-10s : %s", "NO. PENERBANGAN", NO_TERBANG));
        data.append("\n");
        data.append(String.format("%-10s : %s", "JML. PENERBANGAN", JML_TERBANG));
        data.append("\n");
        data.append(String.format("%-10s : %s", "KELAS", KELAS));
        data.append("\n");
        data.append(String.format("%-10s : %s", "CARRIER", CARRIER));
        data.append("\n");
        data.append(String.format("%-10s : %s", "DARI", DARI));
        data.append("\n");
        data.append(String.format("%-10s : %s", "TUJUAN", TUJUAN));
        data.append("\n");
        data.append(String.format("%-10s : %s", "TGL. BERANGKAT", TGL_BRGKT));
        data.append("\n");
        data.append(String.format("%-10s : %s", "JAM BERANGKAT", JAM_BRGKT));
        data.append("\n");
        data.append(String.format("%-10s : %s", "JML. PENUMPANG", JML_PENUMPANG));
        data.append("\n");
        data.append(String.format("%-10s : %s", "TOTAL TAGIHAN", TOTAL_TAGIHAN));
        data.append("\n");
        data.append(String.format("%-10s : %s", "BIAYA ADMIN", BIAYA_ADMIN));
        data.append("\n");
        data.append(String.format("%-10s : %s", "TOTAL BAYAR", HARGA));
        data.append("\n\n");
        data.append("Terima Kasih\n");
        data.append("Simpan struk ini sebagai bukti pembayaran yang sah\r\n");

        try{
            convertImage(data.toString(), NOREF);
        }catch (Exception e){
            e.printStackTrace();
        }

        File outputFile = new File(AppConfig.STORAGE_CARD + "/Download/" + NOREF + ".png");
        Uri uri = Uri.fromFile(outputFile);

        if (outputFile.exists()) {
            Intent share = new Intent();
            share.setAction(Intent.ACTION_SEND);
            share.setType("image/*");
            share.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(share, "Kirim Struk"));
        }else{
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, data.toString());
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Kirim Struk"));
        }
    }

    private void shareDataPdf(String sUrl){
        AppConfig.PDF_FILENAME = AppController.getInstance().getFileNameDoc(sUrl);

        File outputFile = new File(AppConfig.STORAGE_CARD + "/Download/" + AppConfig.PDF_FILENAME);
        Uri uri = Uri.fromFile(outputFile);

        if (outputFile.exists()) {
            Intent share = new Intent();
            share.setAction(Intent.ACTION_SEND);
            share.setType("application/pdf");
            share.putExtra(Intent.EXTRA_STREAM, uri);
            //share.setPackage("com.whatsapp");
            startActivity(Intent.createChooser(share, "Kirim Struk"));
        }else{
            DownloadStruk(RECEIPT);

        }
    }

    void DownloadStruk(String sUrl) {

        AppConfig.PDF_FILENAME = AppController.getInstance().getFileNameDoc(sUrl);

        File file = new File(AppConfig.STORAGE_CARD + "/Download/" + AppConfig.PDF_FILENAME);
        if (file.exists()) {
            Intent mIntent = new Intent(getApplicationContext(), PDFViewActivity.class);
            startActivity(mIntent);
        } else {
            new CitilinkStatus.DownloadFileFromURL().execute(RECEIPT);
        }
    }

    /**
     * Showing Dialog
     * */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    /**
     * Background Async Task to download file
     * */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                AppConfig.PDF_FILENAME = AppController.getInstance().getFileNameDoc(url.toString());

                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream());

                // Output stream
                OutputStream output = new FileOutputStream(AppConfig.STORAGE_CARD + "/Download/" + AppConfig.PDF_FILENAME);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+(int)((total*100)/lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();
                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            dismissDialog(progress_bar_type);

            Intent mIntent = new Intent(getApplicationContext(), PDFViewActivity.class);
            startActivity(mIntent);
        }

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    void convertImage(final String text, String fname) throws IOException {
        Log.d("ConvertImage", fname);
        final Rect bounds = new Rect();
        TextPaint textPaint = new TextPaint() {
            {
                setColor(Color.BLACK);
                setTextAlign(Paint.Align.LEFT);
                setTextSize(22f);
                setAntiAlias(true);
            }
        };
        textPaint.getTextBounds(text, 0, text.length(), bounds);
        StaticLayout mTextLayout = new StaticLayout(text, textPaint,
                bounds.width(), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        int maxWidth = -1;
        for (int i = 0; i < mTextLayout.getLineCount(); i++) {
            if (maxWidth < mTextLayout.getLineWidth(i)) {
                maxWidth = (int) mTextLayout.getLineWidth(i);
            }
        }
        final Bitmap bmp = Bitmap.createBitmap(maxWidth , mTextLayout.getHeight(),
                Bitmap.Config.ARGB_8888);
        bmp.eraseColor(Color.WHITE);
        final Canvas canvas = new Canvas(bmp);
        mTextLayout.draw(canvas);
        FileOutputStream stream = new FileOutputStream(AppConfig.STORAGE_CARD + "/Download/" + fname + ".png");
        bmp.compress(Bitmap.CompressFormat.PNG, 85, stream);
        bmp.recycle();
        stream.close();
    }

    private void addFavoriteDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialog));
        dialog.setMessage("Tambahkan transaksi ini di menu favorit?")
                .setCancelable(false)
                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                        favoriteAdd();

                    }
                })
                .setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                    }
                }).show();
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialog));
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                        Intent i = new Intent(getApplicationContext(),Login.class);
                        startActivity(i);
                        finish();
                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}
