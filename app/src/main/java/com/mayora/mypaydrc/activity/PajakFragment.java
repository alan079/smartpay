package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Inquiry_PDAM;
import com.mayora.mypaydrc.model.Obj_SubCategory;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 26/02/2018.
 */

public class PajakFragment extends Fragment {
    private EditText editIDpel;
    private Button btnInquiry;
    private RelativeLayout relativeLayout;
    private Spinner spinJenis;

    ProgressDialog progress;
    private Obj_Inquiry_PDAM objInquiry;
    private String sProduct ="", sSubscriber = "";
    private String IDPEL = "", NAMAPELANGGAN = "", BULAN ="", TOTALLEMBAR="",RUPIAHTAGIHAN="", TOTTAGIHAN = "", REFFID = "";

    ProgressDialog progressDialog;
    private Obj_SubCategory objSubCategory;
    private List<String> arrJenisName = new ArrayList<String>();

    private String NO="", CATEGORY="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getArguments() != null) {
            NO = getArguments().getString("NO");
            CATEGORY = getArguments().getString("CATEGORY");
        }
        View view = inflater.inflate(R.layout.frag_pajak, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);

        if(CATEGORY != null) {
            if (CATEGORY.equals("12")) {
                editIDpel.setText(NO);
            }
        }
        editIDpel.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        try {
            FillJenis();
        }catch(Exception e){
            e.printStackTrace();
        }

        editIDpel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editIDpel.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    return true;
                }
                return false;
            }
        });

        btnInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // hide virtual keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editIDpel.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN);

                //Inquiry();
                Toast.makeText(getContext(),"Produk ini belum tersedia!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void InitControl(View v) {
        relativeLayout = v.findViewById(R.id.layout_no);
        editIDpel = v.findViewById(R.id.edit_no);
        btnInquiry = v.findViewById(R.id.btnInquiry);
        spinJenis = v.findViewById(R.id.spin_jenis);
    }

    void FillJenis(){

        progressDialog = ProgressDialog.show(getActivity(),"Informasi", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("productinquiry", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_SubCategory param = new Obj_SubCategory(AppConfig.USERID, AppConfig.REQID, "9", AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_SubCategory> call = NetworkManager.getNetworkService(getActivity()).getSubCategoryProd(param);
            call.enqueue(new Callback<Obj_SubCategory>() {
                @Override
                public void onResponse(Call<Obj_SubCategory> call, Response<Obj_SubCategory> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        String jName = "";
                        objSubCategory = response.body();
                        if (objSubCategory.rc.equals("00")){
                            for (Obj_SubCategory.Datum dat : objSubCategory.data){
                                jName = dat.name;
                                arrJenisName.add(jName);
                            }
                            adapterJenis(arrJenisName);

                        }else if (objSubCategory.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            AppController.getInstance().errorDialog(getContext(), "Layanan Pajak tidak tersedia!");
                        }else if (objSubCategory.rc.equals("78")){
                            updateDeviceDialog(objSubCategory.message);
                        }else{
                            AppController.getInstance().errorDialog(getContext(), "Layanan Pajak tidak tersedia!");
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_SubCategory> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
        }
    }

    void adapterJenis(List<String> data){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinJenis.setAdapter(adapter);
    }


    private void Inquiry() {
        progress = ProgressDialog.show(getActivity(), "Info", "Checking data", true);

        sProduct = "PLN";
        String subscriber = editIDpel.getText().toString();
        sSubscriber = subscriber;
        String version = AppController.getInstance().getVersionName();
        String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(Pin);
        Log.e("PIN", sPin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("inquiry", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Inquiry_PDAM param = new Obj_Inquiry_PDAM(AppConfig.IMEI, AppConfig.KEY, AppConfig.REQID, AppConfig.USERID, version, sProduct, sSubscriber);
            Call<Obj_Inquiry_PDAM> call = NetworkManager.getNetworkService(getContext()).inquiry(param);
            call.enqueue(new Callback<Obj_Inquiry_PDAM>() {
                @Override
                public void onResponse(Call<Obj_Inquiry_PDAM> call, Response<Obj_Inquiry_PDAM> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objInquiry = response.body();
                        if (objInquiry.rc.equals("00")) {

                            successDialog("Inquiry berhasil.");

                            String s = objInquiry.message;
                            StringTokenizer st = new StringTokenizer(s, "\r\n");
                            IDPEL = st.nextToken().replace("ID PELANGGAN : ", "");
                            NAMAPELANGGAN = st.nextToken().replace("NAMA : ", "");
                            TOTALLEMBAR = st.nextToken().replace("TOTAL LEMBAR TAGIHAN : ", "");
                            BULAN = st.nextToken().replace("BULAN DAN TAHUN TAGIHAN : ", "");
                            RUPIAHTAGIHAN = st.nextToken().replace("RUPIAH TAGIHAN : ", "");

                            TOTTAGIHAN = objInquiry.display.TOTAL_TAGIHAN;
                            REFFID = objInquiry.inquiry_num;

                        } else {
                            errorDialog(objInquiry.message);
                        }
                    } else {
                        SnackbarMessage(relativeLayout, "Inquiry gagal!", Snackbar.LENGTH_INDEFINITE);
                    }
                }

                @Override
                public void onFailure(Call<Obj_Inquiry_PDAM> call, Throwable t) {
                    progress.dismiss();
                    SnackbarMessage(relativeLayout, "Inquiry gagal!", Snackbar.LENGTH_INDEFINITE);
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            SnackbarMessage(relativeLayout, "Inquiry gagal!", Snackbar.LENGTH_INDEFINITE);
        }
    }

    private void successDialog(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent(getContext(), ListrikPLNDetail.class);
                        intent.putExtra("IDPEL",IDPEL);
                        intent.putExtra("NAMA",NAMAPELANGGAN);
                        intent.putExtra("TOTLEMBAR",TOTALLEMBAR);
                        intent.putExtra("BULAN",BULAN);
                        intent.putExtra("RPTAGIHAN",RUPIAHTAGIHAN);
                        intent.putExtra("TOTTAGIHAN",TOTTAGIHAN);
                        intent.putExtra("REFFID",REFFID);
                        startActivity(intent);
                    }
                }).show();
    }

    private void errorDialog(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                }).show();
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);
        snackbar.show();
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
