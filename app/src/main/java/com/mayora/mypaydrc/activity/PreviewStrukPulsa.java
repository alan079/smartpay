package com.mayora.mypaydrc.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.print.BluetoothSPP;
import com.mayora.mypaydrc.print.BluetoothState;
import com.mayora.mypaydrc.print.DeviceList;

/**
 * Created by eroy on 3/2/2017.
 */

public class PreviewStrukPulsa extends AppCompatActivity {
    private Toolbar mToolbar;
    private String nopel="", trxid="", prod_name="", harga="", waktu="", sn="", status="";
    private TextView txtNopel, txtProdName, txtHarga, txtWaktu, txtNoref, txtSN, txtStatus;
    private Button btnPrint;
    private Menu menu;
    private CoordinatorLayout coordinatorLayout;

    private BluetoothSPP bt;
    private StringBuilder data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_struk);

        InitControl();
        getData();
        setupData();

        Snackbar.make(coordinatorLayout,"Status : Anda belum memilih printer!",Snackbar.LENGTH_INDEFINITE).show();
        bt = new BluetoothSPP(this);

        if(!bt.isBluetoothAvailable()) {
            Toast.makeText(getApplicationContext()
                    , "Bluetooth is not available"
                    , Toast.LENGTH_SHORT).show();
            finish();
        }

        bt.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            public void onDataReceived(byte[] data, String message) {
                Snackbar.make(coordinatorLayout,message +"\n",Snackbar.LENGTH_SHORT).show();
            }
        });

        bt.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            public void onDeviceDisconnected() {
                btnPrint.setVisibility(View.GONE);
                Snackbar.make(coordinatorLayout,"Status : Not connect",Snackbar.LENGTH_INDEFINITE).show();

            }

            public void onDeviceConnectionFailed() {
                btnPrint.setVisibility(View.GONE);
                Snackbar.make(coordinatorLayout,"Status : Connection failed",Snackbar.LENGTH_INDEFINITE).show();

            }

            public void onDeviceConnected(String name, String address) {
                btnPrint.setVisibility(View.VISIBLE);
                Snackbar.make(coordinatorLayout,"Status : Connected to " + name,Snackbar.LENGTH_INDEFINITE).show();

            }
        });
    }

    void InitControl(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_content);

        NestedScrollView scroll = (NestedScrollView) findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        txtStatus = (TextView) findViewById(R.id.txt_status);
        txtNopel = (TextView) findViewById(R.id.txt_nopel);
        txtProdName = (TextView) findViewById(R.id.txt_prod);
        txtHarga = (TextView) findViewById(R.id.txt_harga);
        txtWaktu = (TextView) findViewById(R.id.txt_waktu);
        txtNoref = (TextView) findViewById(R.id.txt_noref);
        txtSN = (TextView) findViewById(R.id.txt_sn);

        btnPrint = (Button) findViewById(R.id.btn_print);
        btnPrint.setVisibility(View.GONE);
        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupPrint();
            }
        });

    }

    void getData(){
        Intent intent = getIntent();
        waktu = intent.getStringExtra("WAKTU");
        nopel = intent.getStringExtra("NOPEL");
        prod_name = intent.getStringExtra("PROD_NAME");
        harga = intent.getStringExtra("PRICE");
        trxid = intent.getStringExtra("TRXID");
        sn = intent.getStringExtra("SN");
        status = intent.getStringExtra("STATUS");

        txtStatus.setText(status);
        txtNopel.setText(nopel);
        txtProdName.setText(prod_name);
        txtHarga.setText(AppController.getInstance().toCurrencyIDR(Double.parseDouble(harga)));
        txtWaktu.setText(waktu);
        txtNoref.setText(trxid);
        txtSN.setText(sn);
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_struk, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            //optionmenu
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                break;

            case R.id.device_connect:
                bt.setDeviceTarget(BluetoothState.DEVICE_OTHER);
                Intent intent = new Intent(getApplicationContext(), DeviceList.class);
                startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);

                break;

            /*case R.id.share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, data.toString().replace("***********************************************",""));
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Kirim struk"));

                break;*/

        }
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if(resultCode == Activity.RESULT_OK)
                bt.connect(data);
        } else if(requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if(resultCode == Activity.RESULT_OK) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_ANDROID);
            } else {
                Snackbar.make(coordinatorLayout,"Bluetooth was not enabled.",Snackbar.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bt.stopService();
    }

    @Override
    public void onStop() {
        super.onStop();
        bt.stopService();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!bt.isBluetoothEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, BluetoothState.REQUEST_ENABLE_BT);
        } else {
            if(!bt.isServiceAvailable()) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_ANDROID);
            }
        }
    }

    private void setupData() {
        data = new StringBuilder();
        data.append("BANK MAYORA \n");
        data.append("SMARTPAY \n");
        data.append(waktu);
        data.append("\n\n");
        data.append(String.format("%-12s : %s", "NO REF", trxid));
        data.append("\n");
        data.append(String.format("%-12s : %s", "NO PELANGGAN", nopel));
        data.append("\n");
        data.append(String.format("%-12s : %s", "PRODUK", prod_name));
        data.append("\n");
        data.append(String.format("%-12s : %s", "HARGA", AppController.getInstance().toCurrencyIDR(Double.parseDouble(harga))));
        data.append("\n");
        data.append(String.format("%-12s : %s", "STATUS", status));
        data.append("\n");
        data.append(String.format("%-12s : %s", "SN", sn));
        data.append("\n\n");
        data.append("TERIMA KASIH \n");
        data.append("CALL CENTER (021) 5655288 \n");
        data.append("***********************************************");

    }

    private void setupPrint(){
        try {
            bt.send(data.toString(),true);
        } catch (Exception e) {
            errorDialog(e.toString());
        }

    }

    private void errorDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                }).show();
    }


}
