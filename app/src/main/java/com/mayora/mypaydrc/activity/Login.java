package com.mayora.mypaydrc.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_AuthUser;
import com.mayora.mypaydrc.model.Obj_SmsDevice;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 01/05/2017.
 */

public class Login extends AppCompatActivity {

    private Button btnSignin, btnLinkTerm, btnLinkForgotPin;
    private EditText txtUser, txtPin;
    private Obj_AuthUser authUser;
    private Obj_SmsDevice smsDevice;
    private  String sPin="";
    private CoordinatorLayout coordinatorLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        AppController.getInstance().getFCMID(this);

        InitControl();
    }

    void InitControl(){
        txtUser = (EditText)findViewById(R.id.input_uid);
        txtPin = (EditText)findViewById(R.id.input_pin);

        btnLinkTerm = (Button) findViewById(R.id.btn_term);
        btnLinkTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Term.class);
                startActivity(i);

            }
        });

        btnLinkForgotPin = (Button) findViewById(R.id.btn_forgot);
        btnLinkForgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Mohon maaf modul ini sedang dalam tahap pengembangan.", Toast.LENGTH_SHORT).show();
            }
        });

        btnSignin = (Button)findViewById(R.id.btn_signin);
        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtUser.getText().length() == 0){
                    txtUser.setError(getString(R.string.error_field_required));
                    txtUser.requestFocus();
                }else if(txtPin.getText().length() == 0){
                    txtPin.setError(getString(R.string.error_field_required));
                    txtPin.requestFocus();
                }else if(txtPin.getText().length() < 6){
                    txtPin.setError(getString(R.string.error_invalid_pin));
                    txtPin.requestFocus();
                }else{
                    ValidasiLogin();
                }
            }
        });

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        SnackbarMessage(coordinatorLayout,
                "Tips Keamanan \n\nJaga kerahasiaan User ID dan Password Anda kepada orang lain, pihak bank dan keluarga. ",
                Snackbar.LENGTH_INDEFINITE);
    }

    private void ValidasiLogin(){
        final ProgressDialog progress_login = new ProgressDialog(this,R.style.ProgressDialog);
        progress_login.setMessage("Proses Login");
        progress_login.setCancelable(false);
        progress_login.show();

        AppConfig.USERID = txtUser.getText().toString().toUpperCase().trim();
        String encrypPin = txtPin.getText().toString().trim();
        sPin = encrypt(encrypPin);
        Log.e("encryp", sPin);
        String version = AppController.getInstance().getVersionName();

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("login", AppConfig.USERID, encrypPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try{
            Obj_AuthUser param = new Obj_AuthUser(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI);
            Call<Obj_AuthUser> call = NetworkManager.getNetworkService(this).loginUser(param);
            call.enqueue(new Callback<Obj_AuthUser>() {
                @Override
                public void onResponse(Call<Obj_AuthUser> call, Response<Obj_AuthUser> response) {
                    int code = response.code();

                    if (code == 200){
                        authUser = response.body();
                        progress_login.dismiss();

                        if (authUser.rc.equals("00")){
                            AppController.getInstance().getSessionManager().setUserAccount(null);
                            AppController.getInstance().getSessionManager().setUserAccount(authUser);
                            AppController.getInstance().getSessionManager().putStringData("pin", sPin);

                            AppConfig.USERID = authUser.user;
                            AppConfig.USERNAME= authUser.data.name;

                            if (authUser.data.last_login.equals("-")){
                                Intent intent = new Intent (Login.this, ChangePinFirst.class);
                                startActivity(intent);
                                finish();
                            }else{
                                Intent intent = new Intent (Login.this, Main.class);
                                startActivity(intent);
                                finish();
                            }


                        }else if (authUser.rc.equals("73")){
                            AppController.getInstance().errorDialog(Login.this, authUser.message);
                            //AppController.getInstance().showForceUpdateDialog(Login.this);

                        }else if (authUser.rc.equals("78")){
                            AppController.getInstance().getSessionManager().setUserAccountTemp(null);
                            AppController.getInstance().getSessionManager().setUserAccountTemp(authUser);
                            AppController.getInstance().getSessionManager().putStringData("pin_temp", sPin);

                            updateDeviceDialog(authUser.message);

                        }else{
                            AppController.getInstance().errorDialog(Login.this, authUser.message);
                        }
                    }else{
                        AppController.getInstance().errorDialog(Login.this, "Login Gagal!");
                        progress_login.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Obj_AuthUser> call, Throwable t) {
                    progress_login.dismiss();
                    AppController.getInstance().errorDialog(Login.this, "Login Gagal!");
                }
            });
        }catch (Exception e){
            progress_login.dismiss();
            Log.v("Exception", e.toString());
        }
    }

    private void ReqSMSDevice(){
        final ProgressDialog progress = new ProgressDialog(this,R.style.ProgressDialog);
        progress.setMessage("Proses Verifikasi");
        progress.setCancelable(false);
        progress.show();

        AppConfig.USERID = txtUser.getText().toString().toUpperCase().trim();
        sPin = txtPin.getText().toString().trim();
        String version = AppController.getInstance().getVersionName();

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("requestsms", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try{
            Obj_SmsDevice param = new Obj_SmsDevice(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI);
            Call<Obj_SmsDevice> call = NetworkManager.getNetworkService(this).reqSmsDevice(param);
            call.enqueue(new Callback<Obj_SmsDevice>() {
                @Override
                public void onResponse(Call<Obj_SmsDevice> call, Response<Obj_SmsDevice> response) {
                    int code = response.code();

                    if (code == 200){
                        smsDevice = response.body();
                        progress.dismiss();

                        if (smsDevice.rc.equals("00")){
                            Intent i = new Intent(Login.this,VerifikasiDevice.class);
                            startActivity(i);
                        }else{
                            AppController.getInstance().errorDialog(Login.this, smsDevice.message);
                        }
                    }else{
                        AppController.getInstance().errorDialog(Login.this, "Request Verifikasi Gagal!");
                        progress.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Obj_SmsDevice> call, Throwable t) {
                    progress.dismiss();
                    AppController.getInstance().errorDialog(Login.this, "Request Verifikasi Gagal!");
                }
            });
        }catch (Exception e){
            progress.dismiss();
            Log.v("Exception", e.toString());
        }
    }

    private void updateDeviceDialog(String msg){
        @SuppressLint("RestrictedApi")
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this,R.style.AlertDialog));
        dialog.setMessage(msg + ". Apakah mau didaftarkan?")
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                        ReqSMSDevice();

                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                    }
                })
                .show();
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(11);
        textView.setTextColor(Color.WHITE);
        textView.setSingleLine(false);
        snackbar.show();
    }

    public static String encrypt(String input) {
        // This is base64 encoding, which is not an encryption
        return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
