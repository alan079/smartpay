package com.mayora.mypaydrc.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ItemPriceAdapter;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Price;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by eroy on 27/05/2017.
 */

public class PriceFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;

    private ProgressDialog progressDialog;
    private Obj_Price obj_Price;
    private ItemPriceAdapter mAdapter;

    private LinearLayoutManager mLayoutManager;
    SwipeRefreshLayout swipeRefreshLayout;
    private EditText editSearch;
    private Button btnSearch;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_price, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() ) {
            FillGrid();
        }

        editSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    editSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editSearch.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    mAdapter = new ItemPriceAdapter(getActivity(), obj_Price, new ItemPriceAdapter.OnDownloadClicked() {
                        @Override
                        public void OnDownloadClicked() {

                        }
                    });

                    try {
                        String sSearch = editSearch.getText().toString();
                        mAdapter.getFilter().filter(sSearch);
                        recyclerView.setAdapter(mAdapter);
                    }catch(Exception e){
                        Log.e("ErrorFiltered", e.toString());
                    }
                    return true;
                }
                return false;
            }
        });

        editSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if(s.length() == 0){
                    FillGrid();
                }

                mAdapter = new ItemPriceAdapter(getActivity(), obj_Price, new ItemPriceAdapter.OnDownloadClicked() {
                    @Override
                    public void OnDownloadClicked() {

                    }
                });

                try {
                    mAdapter.getFilter().filter(s.toString());
                    recyclerView.setAdapter(mAdapter);
                }catch(Exception e){
                    Log.e("ErrorFiltered", e.toString());
                }
            }
        });

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() ) {
            FillGrid();
        }
    }

    private void InitControl(View v) {
        float scale = getResources().getConfiguration().fontScale;
        float sizeNeeded = (float) 14.0;

        swipeRefreshLayout = v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = v.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        //mLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        editSearch = v.findViewById(R.id.edit_search);
        editSearch.setTextSize(sizeNeeded/scale);

        btnSearch = v.findViewById(R.id.btn_cari);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // hide virtual keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editSearch.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN);

                String sSearch = editSearch.getText().toString();
                mAdapter = new ItemPriceAdapter(getActivity(), obj_Price, new ItemPriceAdapter.OnDownloadClicked() {
                    @Override
                    public void OnDownloadClicked() {

                    }
                });

                try {
                    mAdapter.getFilter().filter(sSearch);
                }catch(Exception e){
                    Log.e("ErrorFiltered", e.toString());
                }
            }
        });
    }

    void FillGrid(){
        progressDialog = ProgressDialog.show(getActivity(),"", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("price", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_Price param = new Obj_Price(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_Price> call = NetworkManager.getNetworkService(getActivity()).getPrice(param);
            call.enqueue(new Callback<Obj_Price>() {
                @Override
                public void onResponse(Call<Obj_Price> call, Response<Obj_Price> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        obj_Price = response.body();
                        if (obj_Price.rc.equals("00")){
                            if (obj_Price.data == null || obj_Price.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "Daftar Harga tidak ditemukan!");
                            }else{
                                FillAdapter();

                            }
                        }else if (obj_Price.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_Price.message);

                        }else if (obj_Price.rc.equals("78")){
                            updateDeviceDialog(obj_Price.message);

                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_Price.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_Price> call, Throwable t) {
                    progressDialog.dismiss();
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
            recyclerView.setAdapter(null);
        }
    }

    void FillGridOnRefresh(){
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("price", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_Price param = new Obj_Price(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_Price> call = NetworkManager.getNetworkService(getActivity()).getPrice(param);
            call.enqueue(new Callback<Obj_Price>() {
                @Override
                public void onResponse(Call<Obj_Price> call, Response<Obj_Price> response) {
                    int code = response.code();
                    swipeRefreshLayout.setRefreshing(false);
                    if (code == 200){
                        obj_Price = response.body();
                        if (obj_Price.rc.equals("00")){
                            if (obj_Price.data == null || obj_Price.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "Daftar Harga tidak ditemukan!");
                            }else{
                                FillAdapter();

                            }
                        }else if (obj_Price.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_Price.message);

                        }else if (obj_Price.rc.equals("78")){
                            updateDeviceDialog(obj_Price.message);

                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_Price.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_Price> call, Throwable t) {
                    swipeRefreshLayout.setRefreshing(false);
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            swipeRefreshLayout.setRefreshing(false);
            recyclerView.setAdapter(null);
        }
    }

    void FillAdapter(){
        mAdapter = new ItemPriceAdapter(getActivity(), obj_Price, new ItemPriceAdapter.OnDownloadClicked() {
            @Override
            public void OnDownloadClicked() {

            }
        });
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onRefresh() {
        FillGridOnRefresh();
    }

    private void updateDeviceDialog(String msg){
        @SuppressLint("RestrictedApi")
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.AlertDialog));
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
            .setCancelable(false)
            .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();

                    Intent i = new Intent(getActivity(),Login.class);
                    startActivity(i);
                    getActivity().finish();

                }
            }).show();
    }

}
