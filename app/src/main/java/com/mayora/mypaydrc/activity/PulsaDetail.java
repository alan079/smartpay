package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Purchase;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 3/2/2017.
 */

public class PulsaDetail extends AppCompatActivity {
    private Toolbar mToolbar;
    private EditText editPin;

    private Button btnSubmit;
    private TextView txtNopel, txtProdName, txtNominal, txtHarga;
    private String nopel="", prod_id="", prod_name="", nominal="", harga="", category = "" ;
    ProgressDialog progress;
    Obj_Purchase obj_purchase;
    private String receipt = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pulsa_detail);

        InitControl();
        GetExtrasData();
    }

    void InitControl(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NestedScrollView scroll = (NestedScrollView) findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        txtNopel = (TextView) findViewById(R.id.txt_nopel);
        txtProdName = (TextView) findViewById(R.id.txt_prod);
        txtNominal = (TextView) findViewById(R.id.txt_nominal);
        txtHarga = (TextView) findViewById(R.id.txt_harga);

        editPin = (EditText) findViewById(R.id.edit_pin);
        btnSubmit = (Button) findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processPurchase();
            }
        });
    }

    void GetExtrasData(){
        Intent intent = getIntent();
        nopel = intent.getStringExtra("NOPEL");
        prod_id = intent.getStringExtra("PROD_ID");
        prod_name = intent.getStringExtra("PROD_NAME");
        nominal = intent.getStringExtra("NOMINAL");
        harga = intent.getStringExtra("HARGA");
        category = intent.getStringExtra("CATEGORY");

        txtNopel.setText(nopel);
        txtProdName.setText(prod_name);
        txtNominal.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(nominal)));
        txtHarga.setText(AppController.getInstance().toCurrencyIDR(Double.parseDouble(harga)));
    }

    private void processPurchase(){
        progress = ProgressDialog.show(this, "", "Proses Pembelian", true);

        //String sPin = editPin.getText().toString().trim();
        String decrypPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decrypPin);
        Log.e("decrypt", sPin);
        String price = harga.replace(".00","");

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("purchase", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_Purchase param = new Obj_Purchase(AppConfig.USERID, AppConfig.REQID, prod_id, nopel, price, AppConfig.KEY, version, AppConfig.IMEI);
            Call<Obj_Purchase> call = NetworkManager.getNetworkService(this).purchase(param);
            call.enqueue(new Callback<Obj_Purchase>() {
                @Override
                public void onResponse(Call<Obj_Purchase> call, Response<Obj_Purchase> response) {
                    int code = response.code();

                    if (code == 200){
                        obj_purchase = response.body();
                        progress.dismiss();

                        if (obj_purchase.balance != null) {
                            if (obj_purchase.rc.equals("00")) {
                                if (obj_purchase.receipt != null){
                                    receipt = obj_purchase.receipt;
                                }
                                Intent mIntent = new Intent(getApplicationContext(), PulsaStatus.class);
                                mIntent.putExtra("WAKTU", AppController.getInstance().getDateTime_YYYYMMDD());
                                mIntent.putExtra("NOMINAL", nominal);
                                mIntent.putExtra("HARGA", harga);
                                mIntent.putExtra("NOPEL", nopel);
                                mIntent.putExtra("PROD_NAME", prod_name);
                                mIntent.putExtra("PROD_ID", prod_id);
                                mIntent.putExtra("NOREF", AppConfig.REQID);
                                mIntent.putExtra("SN", obj_purchase.serial);
                                mIntent.putExtra("STATUS", obj_purchase.message);
                                mIntent.putExtra("BALANCE", obj_purchase.balance);
                                mIntent.putExtra("RECEIPT", receipt);
                                mIntent.putExtra("CATEGORY", category);
                                startActivity(mIntent);
                                finish();
                            }else {
                                Intent mIntent = new Intent(getApplicationContext(), PulsaStatus.class);
                                mIntent.putExtra("WAKTU", AppController.getInstance().getDateTime_YYYYMMDD());
                                mIntent.putExtra("NOMINAL", nominal);
                                mIntent.putExtra("HARGA", harga);
                                mIntent.putExtra("NOPEL", nopel);
                                mIntent.putExtra("PROD_NAME", prod_name);
                                mIntent.putExtra("PROD_ID", prod_id);
                                mIntent.putExtra("NOREF", AppConfig.REQID);
                                mIntent.putExtra("SN", "-");
                                mIntent.putExtra("STATUS", obj_purchase.message);
                                mIntent.putExtra("BALANCE", obj_purchase.balance);
                                mIntent.putExtra("RECEIPT", "");
                                mIntent.putExtra("CATEGORY", category);
                                startActivity(mIntent);
                                finish();
                            }
                        }else if (obj_purchase.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(getApplicationContext());
                            AppController.getInstance().errorDialog(PulsaDetail.this, obj_purchase.message);
                        }else if (obj_purchase.rc.equals("78")){
                            updateDeviceDialog(obj_purchase.message);
                        }else{
                            AppController.getInstance().errorDialog(PulsaDetail.this, obj_purchase.message);
                        }
                    }else{
                        AppController.getInstance().errorDialog(PulsaDetail.this, "Permintaan Pembelian Gagal. Silahkan coba lagi.");
                        progress.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Obj_Purchase> call, Throwable t) {
                    progress.dismiss();
                    AppController.getInstance().errorDialog(PulsaDetail.this, "Permintaan Pembelian Gagal. Silahkan coba lagi.");

                }
            });
        }catch (Exception e){
            progress.dismiss();
            AppController.getInstance().errorDialog(PulsaDetail.this, "Permintaan Pembelian Gagal. Silahkan coba lagi.");
            Log.v("Exception", e.toString());
        }
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return true;
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getApplicationContext(),Login.class);
                        startActivity(i);
                        finish();

                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}
