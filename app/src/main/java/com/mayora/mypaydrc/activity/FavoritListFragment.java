package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ItemFavoriteListAdapter;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_ListFavorite;
import com.mayora.mypaydrc.model.Obj_ListFavorite_Delete;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoritListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener  {

    private RecyclerView recyclerView;

    private ProgressDialog progressDialog;
    private Obj_ListFavorite obj_listFavorite, obj_listFavoriteFull;
    private Obj_ListFavorite_Delete obj_listFavorite_delete;
    private ItemFavoriteListAdapter mAdapter;

    private LinearLayoutManager mLayoutManager;
    SwipeRefreshLayout swipeRefreshLayout;

    int page = 1;
    int total_page = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_topup_history, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);


    }

    private void InitControl(View v) {

        swipeRefreshLayout = v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = v.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0) //check for scroll down
                {

                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        if (page <= total_page) {
                            //FillGridMore();
                            page = page + 1;
                        }
                    }
                }
            }
        });
    }

    void FillGrid(){
        page = 1;

        String decryptPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decryptPin);
        Log.e("PIN", sPin);

        progressDialog = ProgressDialog.show(getActivity(),"", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("favorit", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();
        String action = "list";

        try{
            Obj_ListFavorite param = new Obj_ListFavorite(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI, action);

            Call<Obj_ListFavorite> call = NetworkManager.getNetworkService(getActivity()).getListFavorite(param);
            call.enqueue(new Callback<Obj_ListFavorite>() {
                @Override
                public void onResponse(Call<Obj_ListFavorite> call, Response<Obj_ListFavorite> response) {
                    int code = response.code();
                    progressDialog.dismiss();

                    if (code == 200){
                        obj_listFavorite = response.body();
                        if (obj_listFavorite.rc.equals("00")){
                            if (obj_listFavorite.data == null || obj_listFavorite.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "Riwayat Topup tidak ditemukan!");
                            }else{
                                //total_page = Integer.valueOf(obj_listFavorite.total_paging);
                                obj_listFavoriteFull = obj_listFavorite;
                                FillAdapter();

                                page=page+1;
                            }
                        }else if (obj_listFavorite.rc.equals("73")){
                            recyclerView.setAdapter(null);
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                            AppController.getInstance().errorDialog(getContext(), obj_listFavorite.message);

                        }else if (obj_listFavorite.rc.equals("78")){
                            recyclerView.setAdapter(null);
                            updateDeviceDialog(obj_listFavorite.message);

                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_listFavorite.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_ListFavorite> call, Throwable t) {
                    progressDialog.dismiss();
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
            recyclerView.setAdapter(null);
        }
    }

    void FillGridOnRefresh(){
        page = 1;

        String decryptPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decryptPin);
        Log.e("PIN", sPin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("favorit", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();
        String action = "list";

        try{
            Obj_ListFavorite param = new Obj_ListFavorite(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI, action);

            Call<Obj_ListFavorite> call = NetworkManager.getNetworkService(getActivity()).getListFavorite(param);
            call.enqueue(new Callback<Obj_ListFavorite>() {
                @Override
                public void onResponse(Call<Obj_ListFavorite> call, Response<Obj_ListFavorite> response) {
                    int code = response.code();
                    swipeRefreshLayout.setRefreshing(false);

                    if (code == 200){
                        obj_listFavorite = response.body();
                        if (obj_listFavorite.rc.equals("00")){
                            if (obj_listFavorite.data == null || obj_listFavorite.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "Riwayat Topup tidak ditemukan!");
                            }else{
                                //total_page = Integer.valueOf(obj_listFavorite.total_paging);
                                obj_listFavoriteFull = obj_listFavorite;
                                FillAdapter();

                                page=page+1;
                            }
                        }else if (obj_listFavorite.rc.equals("73")){
                            recyclerView.setAdapter(null);
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                            AppController.getInstance().errorDialog(getContext(), obj_listFavorite.message);

                        }else if (obj_listFavorite.rc.equals("78")){
                            recyclerView.setAdapter(null);
                            updateDeviceDialog(obj_listFavorite.message);

                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_listFavorite.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_ListFavorite> call, Throwable t) {
                    swipeRefreshLayout.setRefreshing(false);
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            swipeRefreshLayout.setRefreshing(false);
            recyclerView.setAdapter(null);
        }
    }

    void favoriteDelete(String ID){
        page = 1;

        String decryptPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decryptPin);
        Log.e("PIN", sPin);

        progressDialog = ProgressDialog.show(getActivity(),"", "Proses Hapus Favorit", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("favorit", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();
        String action = "delete";

        try{
            Obj_ListFavorite_Delete param = new Obj_ListFavorite_Delete(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI, action, ID);

            Call<Obj_ListFavorite_Delete> call = NetworkManager.getNetworkService(getActivity()).getListFavoriteDelete(param);
            call.enqueue(new Callback<Obj_ListFavorite_Delete>() {
                @Override
                public void onResponse(Call<Obj_ListFavorite_Delete> call, Response<Obj_ListFavorite_Delete> response) {
                    int code = response.code();
                    progressDialog.dismiss();

                    if (code == 200){
                        obj_listFavorite_delete = response.body();
                        if (obj_listFavorite_delete.rc.equals("00")){
                            AppController.getInstance().errorDialog(getContext(), obj_listFavorite_delete.message);
                            FillGrid();
                        }else if (obj_listFavorite_delete.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                            AppController.getInstance().errorDialog(getContext(), obj_listFavorite_delete.message);

                        }else if (obj_listFavorite_delete.rc.equals("78")){
                            updateDeviceDialog(obj_listFavorite_delete.message);

                        }else{
                            AppController.getInstance().errorDialog(getContext(), obj_listFavorite_delete.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_ListFavorite_Delete> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
        }
    }

    void FillAdapter(){
        mAdapter = new ItemFavoriteListAdapter(getActivity(), obj_listFavoriteFull, new ItemFavoriteListAdapter.OnDownloadClicked() {
            @Override
            public void OnDownloadClicked(String ID, String DESC, String NO, String NAMA, String PROD, String CATEGORY, String FLAG) {
                if (FLAG.equals("EDIT")) {
                    Intent intent = new Intent(getActivity(), FavoritEdit.class);
                    intent.putExtra("ID", ID);
                    intent.putExtra("DESC", DESC);
                    getActivity().startActivity(intent);

                } else if (FLAG.equals("CLICK")) {
                    /*Bundle conData = new Bundle();
                    conData.putString("NO", NO);
                    Intent mIntent = new Intent();
                    mIntent.putExtras(conData);
                    getActivity().setResult(RESULT_OK, mIntent);
                    getActivity().finish();*/

                    switch (CATEGORY){
                        case "1":
                            Intent tiket = new Intent(getActivity(), Tiket.class);
                            tiket.putExtra("NO", NO);
                            tiket.putExtra("CATEGORY", CATEGORY);
                            tiket.putExtra("POSITION", 0);
                            getActivity().startActivity(tiket);
                            break;
                        case "2":
                            Intent bpjs = new Intent(getActivity(), BPJS.class);
                            bpjs.putExtra("NO", NO);
                            bpjs.putExtra("CATEGORY", CATEGORY);
                            bpjs.putExtra("POSITION", 0);
                            getActivity().startActivity(bpjs);
                            break;
                        case "3":
                            Intent plnpost = new Intent(getActivity(), Listrik.class);
                            plnpost.putExtra("NO", NO);
                            plnpost.putExtra("CATEGORY", CATEGORY);
                            plnpost.putExtra("POSITION", 1);
                            getActivity().startActivity(plnpost);
                            break;
                        case "4":
                            Intent tv = new Intent(getActivity(), Tv.class);
                            tv.putExtra("NO", NO);
                            tv.putExtra("CATEGORY", CATEGORY);
                            tv.putExtra("POSITION", 0);
                            getActivity().startActivity(tv);
                            break;
                        case "5":
                            Intent inet = new Intent(getActivity(), Internet.class);
                            inet.putExtra("NO", NO);
                            inet.putExtra("CATEGORY", CATEGORY);
                            inet.putExtra("POSITION", 0);
                            getActivity().startActivity(inet);
                            break;
                        case "6":
                            Intent pdam = new Intent(getActivity(), PDAM.class);
                            pdam.putExtra("NO", NO);
                            pdam.putExtra("CATEGORY", CATEGORY);
                            pdam.putExtra("POSITION", 0);
                            getActivity().startActivity(pdam);
                            break;
                        case "7":
                            Intent telp = new Intent(getActivity(), Telepon.class);
                            telp.putExtra("NO", NO);
                            telp.putExtra("CATEGORY", CATEGORY);
                            telp.putExtra("POSITION", 0);
                            getActivity().startActivity(telp);
                            break;
                        case "8":
                            Intent emoney = new Intent(getActivity(), Emoney.class);
                            emoney.putExtra("NO", NO);
                            emoney.putExtra("CATEGORY", CATEGORY);
                            emoney.putExtra("POSITION", 0);
                            getActivity().startActivity(emoney);
                            break;
                        case "9":
                            Intent pulsa = new Intent(getActivity(), Pulsa.class);
                            pulsa.putExtra("NO", NO);
                            pulsa.putExtra("CATEGORY", CATEGORY);
                            pulsa.putExtra("POSITION", 0);
                            getActivity().startActivity(pulsa);
                            break;
                        case "10":
                            Intent paket = new Intent(getActivity(), Pulsa.class);
                            paket.putExtra("NO", NO);
                            paket.putExtra("CATEGORY", CATEGORY);
                            paket.putExtra("POSITION", 1);
                            getActivity().startActivity(paket);
                            break;
                        case "11":
                            Intent plnpre = new Intent(getActivity(), Listrik.class);
                            plnpre.putExtra("NO", NO);
                            plnpre.putExtra("CATEGORY", CATEGORY);
                            plnpre.putExtra("POSITION", 0);
                            getActivity().startActivity(plnpre);
                            break;
                        case "12":
                            Intent pajak = new Intent(getActivity(), Pajak.class);
                            pajak.putExtra("NO", NO);
                            pajak.putExtra("CATEGORY", CATEGORY);
                            pajak.putExtra("POSITION", 0);
                            getActivity().startActivity(pajak);
                            break;
                    }



                } else {
                    deleteFavoriteDialog(ID);
                }

            }
        });

        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onRefresh() {
        FillGridOnRefresh();
    }

    @Override
    public void onResume() {
        super.onResume();
        FillGrid();

    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

    private void deleteFavoriteDialog(final String ID){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage("Apakah anda yakin akan menghapus list favorit ini?")
                .setCancelable(false)
                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        favoriteDelete(ID);

                    }
                })
                .setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
