package com.mayora.mypaydrc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Payment_PDAM;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PDAMDetail extends AppCompatActivity {

    private TextView txtIDPel, txtNama, txtStandMeter, txtBulan, txtTagihan, txtDenda, txtTotTagihan, txtAdmin, txtTotBayar, txtWilayah;
    private Button btnBayar;
    private LinearLayout linearLayout;

    ProgressDialog progress;
    private Obj_Payment_PDAM objPayment;
    private String PRODUCTID="", MSG="",IDPEL = "", NAMA = "", BULAN ="", DENDA="",RUPIAHTAGIHAN="", TOTTAGIHAN = "", REFFID = "",
            STANDMETER="",  NOREF="",ADMIN="", TOTBAYAR="", SN="", RECEIPT="", WILAYAH="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pdam_detail);

        InitControl();
        GetExtrasData();

        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentProccess();
            }
        });

    }

    void InitControl(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        NestedScrollView scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        linearLayout = findViewById(R.id.layout_detail);

        txtIDPel = findViewById(R.id.txt_idpel);
        txtNama = findViewById(R.id.txt_nama);
        txtStandMeter = findViewById(R.id.txt_standmeter);
        txtBulan = findViewById(R.id.txt_date);
        txtTagihan = findViewById(R.id.txt_tagihan);
        txtDenda = findViewById(R.id.txt_denda);
        txtAdmin = findViewById(R.id.txt_admin);
        txtTotTagihan = findViewById(R.id.txt_totaltagihan);
        txtTotBayar = findViewById(R.id.txt_totalbayar);
        txtWilayah = findViewById(R.id.txt_wilayah);

        btnBayar = findViewById(R.id.btnBayar);
    }

    void GetExtrasData(){

        Intent intent = getIntent();
        IDPEL = intent.getStringExtra("IDPEL");
        NAMA = intent.getStringExtra("NAMA");
        BULAN = intent.getStringExtra("BULAN");
        STANDMETER = intent.getStringExtra("STANDMETER");
        RUPIAHTAGIHAN = intent.getStringExtra("RPTAGIHAN");
        DENDA = intent.getStringExtra("DENDA");
        TOTTAGIHAN = intent.getStringExtra("TOTTAGIHAN");
        ADMIN = intent.getStringExtra("ADMIN");
        TOTBAYAR = intent.getStringExtra("TOTBAYAR");
        REFFID = intent.getStringExtra("REFFID");
        PRODUCTID = intent.getStringExtra("PRODUCTID");
        WILAYAH = intent.getStringExtra("WILAYAH");

        txtIDPel.setText(IDPEL);
        txtNama.setText(NAMA);
        txtStandMeter.setText(STANDMETER);
        txtBulan.setText(BULAN);
        txtTagihan.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(RUPIAHTAGIHAN)));
        txtDenda.setText(DENDA);
        txtAdmin.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(ADMIN)));
        txtTotTagihan.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTTAGIHAN)));
        txtTotBayar.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTBAYAR)));
        txtWilayah.setText(WILAYAH);

    }

    private void PaymentProccess() {
        progress = ProgressDialog.show(this, "Info", "Checking data", true);

        String version = AppController.getInstance().getVersionName();
        String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(Pin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("payment", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Payment_PDAM param = new Obj_Payment_PDAM(AppConfig.IMEI, AppConfig.KEY, AppConfig.REQID, AppConfig.USERID, version, PRODUCTID, IDPEL, REFFID);
            Call<Obj_Payment_PDAM> call = NetworkManager.getNetworkService(this).paymentPDAM(param);
            call.enqueue(new Callback<Obj_Payment_PDAM>() {
                @Override
                public void onResponse(Call<Obj_Payment_PDAM> call, Response<Obj_Payment_PDAM> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        //btnAdvice.setVisibility(View.VISIBLE);
                        btnBayar.setVisibility(View.GONE);
                        objPayment = response.body();
                        if (objPayment != null) {
                            if (objPayment.rc.equals("00")) {
                                try {
                                    MSG = objPayment.message;
                                    IDPEL = objPayment.subscriber;
                                    NAMA = objPayment.display.NAMA;
                                    BULAN = objPayment.display.PERIODE;
                                    NOREF = objPayment.reffnum;
                                    SN = objPayment.display.SN;
                                    TOTTAGIHAN = objPayment.display.TOTAL_TAGIHAN;
                                    ADMIN = objPayment.display.BIAYA_ADMIN;
                                    TOTBAYAR = objPayment.display.HARGA;
                                    RECEIPT = objPayment.receipt;
                                    WILAYAH = txtWilayah.getText().toString();

                                    Intent mIntent = new Intent(getApplicationContext(), PDAMStatus.class);
                                    mIntent.putExtra("MSG", MSG);
                                    mIntent.putExtra("IDPEL", IDPEL);
                                    mIntent.putExtra("NAMA", NAMA);
                                    mIntent.putExtra("BULAN", BULAN);
                                    mIntent.putExtra("STANDMETER", STANDMETER);
                                    mIntent.putExtra("RPTAGIHAN", RUPIAHTAGIHAN);
                                    mIntent.putExtra("DENDA", DENDA);
                                    mIntent.putExtra("TOTTAGIHAN", TOTTAGIHAN);
                                    mIntent.putExtra("ADMIN", ADMIN);
                                    mIntent.putExtra("TOTBAYAR", TOTBAYAR);
                                    mIntent.putExtra("PRODUCTID", PRODUCTID);
                                    mIntent.putExtra("NOREF", NOREF);
                                    mIntent.putExtra("SN", SN);
                                    mIntent.putExtra("RECEIPT", RECEIPT);
                                    mIntent.putExtra("WILAYAH", WILAYAH);
                                    startActivity(mIntent);
                                    finish();

                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), "Pembayaran Tagihan PDAM gagal!", Toast.LENGTH_LONG).show();
                                    Log.e("ErrorPDAMPayment", e.toString());
                                }
                            } else if (objPayment.rc.equals("01") || objPayment.rc.equals("68") || objPayment.rc.equals("69")) {
                                MSG = objPayment.message;
                                NOREF = objPayment.reffnum;

                                Intent mIntent = new Intent(getApplicationContext(), PDAMStatus.class);
                                mIntent.putExtra("MSG", MSG);
                                mIntent.putExtra("IDPEL", IDPEL);
                                mIntent.putExtra("NAMA", NAMA);
                                mIntent.putExtra("BULAN", BULAN);
                                mIntent.putExtra("STANDMETER", STANDMETER);
                                mIntent.putExtra("RPTAGIHAN", RUPIAHTAGIHAN);
                                mIntent.putExtra("DENDA", DENDA);
                                mIntent.putExtra("TOTTAGIHAN", TOTTAGIHAN);
                                mIntent.putExtra("ADMIN", ADMIN);
                                mIntent.putExtra("TOTBAYAR", TOTBAYAR);
                                mIntent.putExtra("PRODUCTID", PRODUCTID);
                                mIntent.putExtra("NOREF", NOREF);
                                mIntent.putExtra("SN", SN);
                                mIntent.putExtra("RECEIPT", RECEIPT);
                                mIntent.putExtra("WILAYAH", WILAYAH);
                                startActivity(mIntent);
                                finish();
                            } else {
                                NOREF = objPayment.reference;
                                AppController.getInstance().errorDialog(PDAMDetail.this, objPayment.message);
                            }
                        }else{
                            Toast.makeText(getApplicationContext(), "Pembayaran Tagihan PDAM gagal!", Toast.LENGTH_LONG).show();
                            Log.e("ErrorPDAMPayment", "null");
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Pembayaran Tagihan PDAM gagal!", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Obj_Payment_PDAM> call, Throwable t) {
                    progress.dismiss();
                    Toast.makeText(getApplicationContext(), "Pembayaran Tagihan PDAM gagal!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            Toast.makeText(getApplicationContext(), "Pembayaran Tagihan PDAM gagal!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //optionmenu
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                break;
        }
        return true;
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);
        snackbar.show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}