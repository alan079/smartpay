package com.mayora.mypaydrc.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.print.BluetoothSPP;
import com.mayora.mypaydrc.print.BluetoothState;
import com.mayora.mypaydrc.print.DeviceList;

public class PreviewStrukToken extends AppCompatActivity {
    private Toolbar mToolbar;
    private String IDPEL="", NOMETER="", NAMAPELANGGAN="", TARIFDAYA="", JUMLAHKWH="", RPTAGPLN="", PPN="", PPJ="", NOREF="", ADMIN="", TOTBAYAR="", TOKEN="";
    private TextView txtWaktu, txtIDpel, txtNoMeter, txtNamaPel, txtTarif, txtJumlahKwh, txtRptag, txtPPN, txtPPJ, txtNoref, txtAdmin, txtTotBayar, txtToken;
    private Button btnPrint;
    private Menu menu;
    private CoordinatorLayout coordinatorLayout;

    private BluetoothSPP bt;
    private StringBuilder data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_struk_token);

        InitControl();
        getData();
        setupData();

        Snackbar.make(coordinatorLayout,"Status : Anda belum memilih printer!", Snackbar.LENGTH_INDEFINITE).show();
        bt = new BluetoothSPP(this);

        if(!bt.isBluetoothAvailable()) {
            Toast.makeText(getApplicationContext()
                    , "Bluetooth is not available"
                    , Toast.LENGTH_SHORT).show();
            finish();
        }

        bt.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            public void onDataReceived(byte[] data, String message) {
                Snackbar.make(coordinatorLayout,message +"\n", Snackbar.LENGTH_SHORT).show();
            }
        });

        bt.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            public void onDeviceDisconnected() {
                btnPrint.setVisibility(View.GONE);
                Snackbar.make(coordinatorLayout,"Status : Not connect", Snackbar.LENGTH_INDEFINITE).show();
            }

            public void onDeviceConnectionFailed() {
                btnPrint.setVisibility(View.GONE);
                Snackbar.make(coordinatorLayout,"Status : Connection failed", Snackbar.LENGTH_INDEFINITE).show();
            }

            public void onDeviceConnected(String name, String address) {
                btnPrint.setVisibility(View.VISIBLE);
                Snackbar.make(coordinatorLayout,"Status : Connected to " + name, Snackbar.LENGTH_INDEFINITE).show();
            }
        });
    }

    void InitControl(){
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        coordinatorLayout = findViewById(R.id.main_content);

        NestedScrollView scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        txtWaktu = findViewById(R.id.txt_waktu);
        txtIDpel = findViewById(R.id.txt_idpel);
        txtNoMeter = findViewById(R.id.txt_nometer);
        txtNamaPel = findViewById(R.id.txt_nama);
        txtTarif = findViewById(R.id.txt_tarif);
        txtJumlahKwh = findViewById(R.id.txt_kwh);
        txtRptag = findViewById(R.id.txt_rptagpln);
        txtPPN = findViewById(R.id.txt_ppn);
        txtPPJ = findViewById(R.id.txt_ppj);
        txtNoref = findViewById(R.id.txt_noref);
        txtAdmin = findViewById(R.id.txt_admin);
        txtTotBayar = findViewById(R.id.txt_totbayar);
        txtToken = findViewById(R.id.txt_token);

        btnPrint = findViewById(R.id.btn_print);

        btnPrint.setVisibility(View.GONE);
        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupPrint();
            }
        });
    }

    void getData(){
        Intent mIntent = getIntent();
        IDPEL = mIntent.getStringExtra("IDPEL");
        NOMETER = mIntent.getStringExtra("NOMETER");
        NAMAPELANGGAN = mIntent.getStringExtra("NAMA");
        TARIFDAYA = mIntent.getStringExtra("TARIFDAYA");
        JUMLAHKWH = mIntent.getStringExtra("JUMLAHKWH");
        RPTAGPLN = mIntent.getStringExtra("TOTTAGIHAN");
        PPN = mIntent.getStringExtra("PPN");
        PPJ = mIntent.getStringExtra("PPJ");
        NOREF = mIntent.getStringExtra("NOREF");
        ADMIN = mIntent.getStringExtra("ADMIN");
        TOTBAYAR = mIntent.getStringExtra("TOTBAYAR");
        TOKEN = mIntent.getStringExtra("TOKEN");

        txtWaktu.setText(AppController.getInstance().getDateTime_YYYYMMDD());
        txtIDpel.setText(IDPEL);
        txtNoMeter.setText(NOMETER);
        txtNamaPel.setText(NAMAPELANGGAN);
        txtTarif.setText(TARIFDAYA);
        txtJumlahKwh.setText(JUMLAHKWH);
        txtRptag.setText(RPTAGPLN);
        txtPPN.setText(PPN);
        txtPPJ.setText(PPJ);
        txtNoref.setText(NOREF);
        txtAdmin.setText(ADMIN);
        txtTotBayar.setText(TOTBAYAR);
        txtToken.setText(TOKEN);
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_struk, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.device_connect:
                bt.setDeviceTarget(BluetoothState.DEVICE_OTHER);
                Intent intent = new Intent(getApplicationContext(), DeviceList.class);
                startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
                break;

        }
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if(resultCode == Activity.RESULT_OK)
                bt.connect(data);
        } else if(requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if(resultCode == Activity.RESULT_OK) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_ANDROID);
            } else {
                Snackbar.make(coordinatorLayout,"Bluetooth was not enabled.", Snackbar.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bt.stopService();
    }

    @Override
    public void onStop() {
        super.onStop();
        bt.stopService();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!bt.isBluetoothEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, BluetoothState.REQUEST_ENABLE_BT);
        } else {
            if(!bt.isServiceAvailable()) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_ANDROID);
            }
        }
    }

    private void setupData() {
        data = new StringBuilder();
        data.append("SMARTPAY \n");
        data.append(txtWaktu.getText().toString());
        data.append("\n\n");
        data.append(String.format("%-10s : %s", "NO. REF", NOREF));
        data.append("\n");
        data.append(String.format("%-10s : %s", "ID PELANGGAN", IDPEL));
        data.append("\n");
        data.append(String.format("%-10s : %s", "NO METER", NOMETER));
        data.append("\n");
        data.append(String.format("%-10s : %s", "NAMA PELANGGAN", NAMAPELANGGAN));
        data.append("\n");
        data.append(String.format("%-10s : %s", "TARIF/DAYA", TARIFDAYA));
        data.append("\n");
        data.append(String.format("%-10s : %s", "JUMLAH KWH", JUMLAHKWH));
        data.append("\n");
        data.append(String.format("%-10s : %s", "RP TAGIHAN PLN", RPTAGPLN));
        data.append("\n");
        data.append(String.format("%-10s : %s", "PPN", PPN));
        data.append("\n");
        data.append(String.format("%-10s : %s", "PPJ", PPJ));
        data.append("\n");
        data.append(String.format("%-10s : %s", "ADMIN CA", ADMIN));
        data.append("\n");
        data.append(String.format("%-10s : %s", "TOTAL BAYAR", TOTBAYAR));
        data.append("\n\n");
        data.append("Terima Kasih\n");
        data.append("PLN menyatakan struk ini sebagai bukti pembayaran yang sah\r\n");
        data.append("Rincian Tagihan Dapat Diakses di www.pln.co.id\n");
        data.append("Informasi Hubungi Call Center : 123 atau PLN Terdekat\r\n");

    }

    private void setupPrint(){
        try {
            bt.send(data.toString(),true);
        } catch (Exception e) {
            errorDialog(e.toString());
        }
    }

    private void errorDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                }).show();
    }

}
