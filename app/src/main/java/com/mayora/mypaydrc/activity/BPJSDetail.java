package com.mayora.mypaydrc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Payment_BPJS;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BPJSDetail extends AppCompatActivity {

    private TextView txtIDPel, txtNama, txtJumPeserta, txtBulan, txtTunggakan, txtTotTagihan, txtAdmin, txtTotBayar;
    private Button btnBayar;
    private LinearLayout linearLayout;

    ProgressDialog progress;
    private Obj_Payment_BPJS objPayment;
    private String PRODUCTID="", MSG="",IDPEL = "", NAMA = "", BULAN ="", TUNGGAKAN="",  TOTTAGIHAN = "", REFFID = "",
            JUMPESERTA="", NOREF="",ADMIN="", TOTBAYAR="", SN="", RECEIPT="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bpjs_detail);

        InitControl();
        GetExtrasData();

        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentProccess();
            }
        });

    }

    void InitControl(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        NestedScrollView scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        linearLayout = findViewById(R.id.layout_detail);

        txtIDPel = findViewById(R.id.txt_idpel);
        txtNama = findViewById(R.id.txt_nama);
        txtJumPeserta = findViewById(R.id.txt_jumpeserta);
        txtBulan = findViewById(R.id.txt_date);
        txtTotTagihan = findViewById(R.id.txt_totaltagihan);
        txtAdmin = findViewById(R.id.txt_admin);
        txtTunggakan = findViewById(R.id.txt_denda);
        txtTotBayar = findViewById(R.id.txt_totalbayar);

        btnBayar = findViewById(R.id.btnBayar);

    }

    void GetExtrasData(){

        Intent intent = getIntent();
        IDPEL = intent.getStringExtra("IDPEL");
        NAMA = intent.getStringExtra("NAMA");
        BULAN = intent.getStringExtra("BULAN");
        JUMPESERTA = intent.getStringExtra("JUMPESERTA");
        TOTTAGIHAN = intent.getStringExtra("TOTTAGIHAN");
        ADMIN = intent.getStringExtra("ADMIN");
        TUNGGAKAN = intent.getStringExtra("TUNGGAKAN");
        TOTBAYAR = intent.getStringExtra("TOTBAYAR");
        REFFID = intent.getStringExtra("REFFID");
        PRODUCTID = intent.getStringExtra("PRODUCTID");

        txtIDPel.setText(IDPEL);
        txtNama.setText(NAMA);
        txtBulan.setText(BULAN);
        txtJumPeserta.setText(JUMPESERTA);
        txtTotTagihan.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTTAGIHAN)));
        txtAdmin.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(ADMIN)));
        txtTunggakan.setText(TUNGGAKAN);
        txtTotBayar.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTBAYAR)));

    }

    private void PaymentProccess() {
        progress = ProgressDialog.show(this, "Info", "Checking data", true);

        String version = AppController.getInstance().getVersionName();
        String decryptPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decryptPin);
        Log.e("decrypt", sPin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("payment", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Payment_BPJS param = new Obj_Payment_BPJS(AppConfig.IMEI, AppConfig.KEY, AppConfig.REQID, AppConfig.USERID, version, PRODUCTID, IDPEL, REFFID, BULAN);
            Call<Obj_Payment_BPJS> call = NetworkManager.getNetworkService(this).paymentBPJS(param);
            call.enqueue(new Callback<Obj_Payment_BPJS>() {
                @Override
                public void onResponse(Call<Obj_Payment_BPJS> call, Response<Obj_Payment_BPJS> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objPayment = response.body();
                        if (objPayment != null) {
                            if (objPayment.rc.equals("00")) {
                                try {
                                    MSG = objPayment.message;
                                    IDPEL = objPayment.subscriber;
                                    NAMA = objPayment.display.NAMA;
                                    BULAN = objPayment.display.TAGIHAN_BULAN;
                                    JUMPESERTA = objPayment.display.JUMLAH_PESERTA;
                                    NOREF = objPayment.reffnum;
                                    SN = objPayment.display.SN;
                                    TOTTAGIHAN = objPayment.display.TOTAL_TAGIHAN;
                                    ADMIN = objPayment.display.BIAYA_ADMIN;
                                    TUNGGAKAN = objPayment.display.TUNGGAKAN;
                                    TOTBAYAR = objPayment.display.HARGA;
                                    RECEIPT = objPayment.receipt;

                                    Intent mIntent = new Intent(getApplicationContext(), BPJSStatus.class);
                                    mIntent.putExtra("MSG", MSG);
                                    mIntent.putExtra("IDPEL", IDPEL);
                                    mIntent.putExtra("NAMA", NAMA);
                                    mIntent.putExtra("BULAN", BULAN);
                                    mIntent.putExtra("JUMPESERTA", JUMPESERTA);
                                    mIntent.putExtra("TOTTAGIHAN", TOTTAGIHAN);
                                    mIntent.putExtra("ADMIN", ADMIN);
                                    mIntent.putExtra("TUNGGAKAN", TUNGGAKAN);
                                    mIntent.putExtra("TOTBAYAR", TOTBAYAR);
                                    mIntent.putExtra("PRODUCTID", PRODUCTID);
                                    mIntent.putExtra("NOREF", NOREF);
                                    mIntent.putExtra("SN", SN);
                                    mIntent.putExtra("RECEIPT", RECEIPT);
                                    startActivity(mIntent);
                                    finish();


                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), "Pembayaran Tagihan BPJS gagal!", Toast.LENGTH_LONG).show();
                                    Log.e("ErrorPDAMPayment", e.toString());
                                }
                            } else if (objPayment.rc.equals("01") || objPayment.rc.equals("68") || objPayment.rc.equals("69")) {
                                MSG = objPayment.message;
                                NOREF = objPayment.reffnum;

                                Intent mIntent = new Intent(getApplicationContext(), BPJSStatus.class);
                                mIntent.putExtra("MSG", MSG);
                                mIntent.putExtra("IDPEL", IDPEL);
                                mIntent.putExtra("NAMA", NAMA);
                                mIntent.putExtra("BULAN", BULAN);
                                mIntent.putExtra("JUMPESERTA", JUMPESERTA);
                                mIntent.putExtra("TOTTAGIHAN", TOTTAGIHAN);
                                mIntent.putExtra("ADMIN", ADMIN);
                                mIntent.putExtra("TUNGGAKAN", TUNGGAKAN);
                                mIntent.putExtra("TOTBAYAR", TOTBAYAR);
                                mIntent.putExtra("PRODUCTID", PRODUCTID);
                                mIntent.putExtra("NOREF", NOREF);
                                mIntent.putExtra("SN", SN);
                                mIntent.putExtra("RECEIPT", RECEIPT);
                                startActivity(mIntent);
                                finish();

                            } else {
                                AppController.getInstance().errorDialog(BPJSDetail.this, objPayment.message);
                            }
                        }else{
                            Toast.makeText(getApplicationContext(), "Pembayaran Tagihan BPJS gagal!", Toast.LENGTH_LONG).show();
                            Log.e("ErrorPDAMPayment", "null");
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Pembayaran Tagihan BPJS gagal!", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Obj_Payment_BPJS> call, Throwable t) {
                    progress.dismiss();
                    Toast.makeText(getApplicationContext(), "Pembayaran Tagihan BPJS gagal!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            Toast.makeText(getApplicationContext(), "Pembayaran Tagihan BPJS gagal!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);
        snackbar.show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}