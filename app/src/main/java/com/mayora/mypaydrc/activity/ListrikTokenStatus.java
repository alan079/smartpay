package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_ListFavorite_Add;
import com.mayora.mypaydrc.net.NetworkManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 3/2/2017.
 */

public class ListrikTokenStatus extends AppCompatActivity {

    private String nopel="", prod_id="", prod_name="", nominal="", harga="", waktu="", noref="", sn="", status="", balance="", receipt="";
    private TextView txtNopel, txtProdName, txtNominal, txtHarga, txtWaktu, txtNoref, txtSN, txtStatus, txtBalance, txtLabelNo, txtLabelToken;
    private Button btnFinish;
    private ImageView btnPrint, btnShare, btnFavorite;

    // Progress Dialog
    private ProgressDialog pDialog;
    ProgressDialog progress;
    public static final int progress_bar_type = 0;

    Obj_ListFavorite_Add obj_listFavorite_add;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pulsa_status);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        InitControl();
        LoadData();

        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.image_click));

                if (receipt.equals("")){
                    Intent mIntent = new Intent(getApplicationContext(), PreviewStrukPulsa.class);
                    mIntent.putExtra("TRXID",noref);
                    mIntent.putExtra("WAKTU",waktu);
                    mIntent.putExtra("NOPEL",nopel);
                    mIntent.putExtra("PROD_NAME",prod_name);
                    mIntent.putExtra("PRICE",harga);
                    mIntent.putExtra("SN",sn);
                    mIntent.putExtra("STATUS",status.trim());
                    mIntent.putExtra("RECEIPT",receipt);
                    startActivity(mIntent);
                }else{
                    DownloadStruk(receipt);
                }

            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.image_click));

                if (receipt.equals("")){
                    shareData();
                }else{
                    shareDataPdf(receipt);
                }

            }
        });

        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.image_click));

                addFavoriteDialog();
            }
        });
    }

    void DownloadStruk(String sUrl) {

        AppConfig.PDF_FILENAME = AppController.getInstance().getFileNameDoc(sUrl);

        File file = new File(AppConfig.STORAGE_CARD + "/Download/" + AppConfig.PDF_FILENAME);
        if (file.exists()) {
            Intent mIntent = new Intent(getApplicationContext(), PDFViewActivity.class);
            startActivity(mIntent);
        } else {
            new DownloadFileFromURL().execute(receipt);
        }
    }

    void InitControl(){

        NestedScrollView scroll = (NestedScrollView) findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        txtStatus = (TextView) findViewById(R.id.txt_status);
        txtNopel = (TextView) findViewById(R.id.txt_nopel);
        txtProdName = (TextView) findViewById(R.id.txt_prod);
        txtNominal = (TextView) findViewById(R.id.txt_nominal);
        txtHarga = (TextView) findViewById(R.id.txt_harga);
        txtWaktu = (TextView) findViewById(R.id.txt_waktu);
        txtNoref = (TextView) findViewById(R.id.txt_noref);
        txtSN = (TextView) findViewById(R.id.txt_sn);
        txtBalance = (TextView) findViewById(R.id.txt_saldo);
        txtLabelNo = (TextView) findViewById(R.id.txt_title_nopel);
        txtLabelNo.setText("No. Meter / ID Pelanggan");
        txtLabelToken = (TextView) findViewById(R.id.txt_title_sn);
        txtLabelToken.setText("Token");

        btnPrint = (ImageView) findViewById(R.id.btn_print);
        btnShare = (ImageView) findViewById(R.id.btn_share);
        btnFavorite = (ImageView) findViewById(R.id.btn_favorite);
        btnFinish = (Button) findViewById(R.id.btn_finish);

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConfig.PDF_FILENAME = "";
                finish();
            }
        });

    }

    void LoadData(){
        Intent intent = getIntent();
        waktu = intent.getStringExtra("WAKTU");
        nopel = intent.getStringExtra("NOPEL");
        prod_id = intent.getStringExtra("PROD_ID");
        prod_name = intent.getStringExtra("PROD_NAME");
        nominal = intent.getStringExtra("NOMINAL");
        harga = intent.getStringExtra("HARGA");
        noref = intent.getStringExtra("NOREF");
        sn = intent.getStringExtra("SN");
        status = intent.getStringExtra("STATUS");
        balance = intent.getStringExtra("BALANCE");
        receipt = intent.getStringExtra("RECEIPT");

        txtStatus.setText(status);
        if(status.contains("BERHASIL")){
            txtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.md_green_500));
        }else if(status.contains("GAGAL")){
            txtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.md_red_500));
        }else{
            txtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.md_yellow_800));
        }
        txtNopel.setText(nopel);
        txtProdName.setText(prod_name);
        txtNominal.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(nominal)));
        txtHarga.setText(AppController.getInstance().toCurrencyIDR(Double.parseDouble(harga)));
        txtWaktu.setText(waktu);
        txtNoref.setText(noref);
        txtSN.setText(sn);
        txtBalance.setText(AppController.getInstance().toCurrencyIDR(Double.parseDouble(balance)));

    }

    void favoriteAdd(){

        String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(Pin);
        Log.e("PIN", sPin);

        progress = ProgressDialog.show(this,"", "Proses Tambah Favorit", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("favorit", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();
        String action = "add";

        try{
            Obj_ListFavorite_Add param = new Obj_ListFavorite_Add(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI, action, "11", prod_id, nominal,nopel, "-" );

            Call<Obj_ListFavorite_Add> call = NetworkManager.getNetworkService(this).getListFavoriteAdd(param);
            call.enqueue(new Callback<Obj_ListFavorite_Add>() {
                @Override
                public void onResponse(Call<Obj_ListFavorite_Add> call, Response<Obj_ListFavorite_Add> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200){
                        obj_listFavorite_add = response.body();
                        if (obj_listFavorite_add.rc.equals("00")){
                            AppController.getInstance().errorDialog(ListrikTokenStatus.this, obj_listFavorite_add.message);

                        }else if (obj_listFavorite_add.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                            AppController.getInstance().errorDialog(ListrikTokenStatus.this, obj_listFavorite_add.message);

                        }else if (obj_listFavorite_add.rc.equals("78")){
                            updateDeviceDialog(obj_listFavorite_add.message);

                        }else{
                            AppController.getInstance().errorDialog(ListrikTokenStatus.this, obj_listFavorite_add.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_ListFavorite_Add> call, Throwable t) {
                    progress.dismiss();
                }
            });
        }catch (Exception e){
            progress.dismiss();
        }
    }

    private void shareData() {
        StringBuilder data = new StringBuilder();
        data.append("BANK MAYORA \n");
        data.append("SMARTPAY \n");
        data.append(waktu);
        data.append("\n\n");
        data.append(String.format("%-10s : %s", "NO REF", noref));
        data.append("\n");
        data.append(String.format("%-10s : %s", "NO METER/ID PELANGGAN", nopel));
        data.append("\n");
        data.append(String.format("%-8s : %s", "PRODUK", prod_name));
        data.append("\n");
        data.append(String.format("%-10s : %s", "HARGA", AppController.getInstance().toCurrencyIDR(Double.parseDouble(harga))));
        data.append("\n");
        data.append(String.format("%-10s : %s", "STATUS", status));
        data.append("\n");
        data.append(String.format("%-10s : %s", "TOKEN", sn));
        data.append("\n\n");
        data.append("TERIMA KASIH \n");
        data.append("CALL CENTER (021) 5655288 \n");

        try{
            convertImage(data.toString(), noref);
        }catch (Exception e){
            e.printStackTrace();
        }

        File outputFile = new File(AppConfig.STORAGE_CARD + "/Download/" + noref + ".png");
        Uri uri = Uri.fromFile(outputFile);

        if (outputFile.exists()) {
            Intent share = new Intent();
            share.setAction(Intent.ACTION_SEND);
            share.setType("image/*");
            share.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(share, "Kirim Struk"));
        }else{
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, data.toString());
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Kirim Struk"));
        }
    }

    private void shareDataPdf(String sUrl){
        AppConfig.PDF_FILENAME = AppController.getInstance().getFileNameDoc(sUrl);

        File outputFile = new File(AppConfig.STORAGE_CARD + "/Download/" + AppConfig.PDF_FILENAME);
        Uri uri = Uri.fromFile(outputFile);

        if (outputFile.exists()) {
            Intent share = new Intent();
            share.setAction(Intent.ACTION_SEND);
            share.setType("application/pdf");
            share.putExtra(Intent.EXTRA_STREAM, uri);
            //share.setPackage("com.whatsapp");
            startActivity(Intent.createChooser(share, "Kirim Struk"));
        }else{
            //new DownloadFileFromURL().execute(sUrl);
            DownloadStruk(receipt);

        }

    }

    @Override
    public void onBackPressed() {
        // do something on back.
        AppConfig.PDF_FILENAME = "";
        finish();
    }

    /**
     * Showing Dialog
     * */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    /**
     * Background Async Task to download file
     * */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                AppConfig.PDF_FILENAME = AppController.getInstance().getFileNameDoc(url.toString());

                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream());

                // Output stream
                OutputStream output = new FileOutputStream(AppConfig.STORAGE_CARD + "/Download/" + AppConfig.PDF_FILENAME);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+(int)((total*100)/lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();
                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);

            Intent mIntent = new Intent(getApplicationContext(), PDFViewActivity.class);
            startActivity(mIntent);
        }

    }

    void convertImage(final String text, String fname) throws IOException {
        Log.d("ConvertImage", fname);
        final Rect bounds = new Rect();
        TextPaint textPaint = new TextPaint() {
            {
                setColor(Color.BLACK);
                setTextAlign(Paint.Align.LEFT);
                setTextSize(22f);
                setAntiAlias(true);
            }
        };
        textPaint.getTextBounds(text, 0, text.length(), bounds);
        StaticLayout mTextLayout = new StaticLayout(text, textPaint,
                bounds.width(), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        int maxWidth = -1;
        for (int i = 0; i < mTextLayout.getLineCount(); i++) {
            if (maxWidth < mTextLayout.getLineWidth(i)) {
                maxWidth = (int) mTextLayout.getLineWidth(i);
            }
        }
        final Bitmap bmp = Bitmap.createBitmap(maxWidth , mTextLayout.getHeight(),
                Bitmap.Config.ARGB_8888);
        bmp.eraseColor(Color.WHITE);// just adding black background
        final Canvas canvas = new Canvas(bmp);
        mTextLayout.draw(canvas);
        FileOutputStream stream = new FileOutputStream(AppConfig.STORAGE_CARD + "/Download/" + fname + ".png");
        bmp.compress(Bitmap.CompressFormat.PNG, 85, stream);
        bmp.recycle();
        stream.close();
    }

    private void addFavoriteDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialog));
        dialog.setMessage("Tambahkan transaksi ini di menu favorit?")
                .setCancelable(false)
                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        favoriteAdd();

                    }
                })
                .setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                    }
                }).show();
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialog));
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getApplicationContext(),Login.class);
                        startActivity(i);
                        finish();

                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}
