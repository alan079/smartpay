package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Inquiry_TELPPOST;
import com.mayora.mypaydrc.model.Obj_Jenis_Type;
import com.mayora.mypaydrc.model.Obj_SubCategory;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 26/02/2018.
 */

public class TeleponFragment extends Fragment {
    private EditText editIDpel;
    private Button btnInquiry;
    private RelativeLayout relativeLayout;
    private Spinner spinJenis;

    ProgressDialog progress;
    private Obj_Inquiry_TELPPOST objInquiry;
    private String sProduct ="", sSubscriber = "";
    private String IDPEL = "", NAMA = "", BULAN ="", TOTTAGIHAN = "", REFFID = "", ADMIN = "", TUNGGAKAN= "", TOTBAYAR="";

    private ArrayList<Obj_Jenis_Type> arrJenis = new ArrayList<Obj_Jenis_Type>();

    ProgressDialog progressDialog;
    private Obj_SubCategory objSubCategory;
    private String productCode = "";
    private String NO="", CATEGORY="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getArguments() != null) {
            NO = getArguments().getString("NO");
            CATEGORY = getArguments().getString("CATEGORY");
        }
        View view = inflater.inflate(R.layout.frag_telepon, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);

        if(CATEGORY != null) {
            if (CATEGORY.equals("7")) {
                editIDpel.setText(NO);
            }
        }
        editIDpel.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        try {
            FillJenis();
        }catch(Exception e){
            e.printStackTrace();
        }

        editIDpel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editIDpel.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    return true;
                }
                return false;
            }
        });

        spinJenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Obj_Jenis_Type jenis = (Obj_Jenis_Type) parent.getSelectedItem();
                productCode = jenis.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // hide virtual keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editIDpel.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN);

                if(editIDpel.getText().length() > 0){
                  Inquiry();
                }
            }
        });


    }

    private void InitControl(View v) {
        relativeLayout = v.findViewById(R.id.layout_no);
        editIDpel = v.findViewById(R.id.edit_no);
        btnInquiry = v.findViewById(R.id.btnInquiry);
        spinJenis = v.findViewById(R.id.spin_jenis);
    }

    void FillJenis(){

        progressDialog = ProgressDialog.show(getActivity(),"Informasi", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("productinquiry", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_SubCategory param = new Obj_SubCategory(AppConfig.USERID, AppConfig.REQID, "7", AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_SubCategory> call = NetworkManager.getNetworkService(getActivity()).getSubCategoryProd(param);
            call.enqueue(new Callback<Obj_SubCategory>() {
                @Override
                public void onResponse(Call<Obj_SubCategory> call, Response<Obj_SubCategory> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        String jName = "";
                        String jId = "";
                        objSubCategory = response.body();
                        if (objSubCategory.rc.equals("00")){
                            for (Obj_SubCategory.Datum dat : objSubCategory.data){
                                jName = dat.name;
                                jId = dat.product_id;
                                arrJenis.add(new Obj_Jenis_Type(jId, jName));
                            }
                            adapterJenis(arrJenis);

                        }else if (objSubCategory.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            AppController.getInstance().errorDialog(getActivity(), "Layanan Tagihan Telepon tidak tersedia!");
                        }else if (objSubCategory.rc.equals("78")){
                            updateDeviceDialog(objSubCategory.message);
                        }else{
                            AppController.getInstance().errorDialog(getActivity(), "Layanan Tagihan Telepon tidak tersedia!");
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_SubCategory> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
        }
    }

    void adapterJenis(ArrayList<Obj_Jenis_Type> data){
        ArrayAdapter<Obj_Jenis_Type> adapter = new ArrayAdapter<Obj_Jenis_Type>(getContext(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinJenis.setAdapter(adapter);
    }


    private void Inquiry() {
        progress = ProgressDialog.show(getActivity(), "Info", "Checking data", true);

        sProduct = productCode;
        String subscriber = editIDpel.getText().toString();
        sSubscriber = subscriber;
        String version = AppController.getInstance().getVersionName();
        String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(Pin);
        Log.e("PIN", sPin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("inquiry", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Inquiry_TELPPOST param = new Obj_Inquiry_TELPPOST(AppConfig.IMEI, AppConfig.KEY, AppConfig.REQID, AppConfig.USERID, version, sProduct, sSubscriber);
            Call<Obj_Inquiry_TELPPOST> call = NetworkManager.getNetworkService(getContext()).inquiryTELPPOST(param);
            call.enqueue(new Callback<Obj_Inquiry_TELPPOST>() {
                @Override
                public void onResponse(Call<Obj_Inquiry_TELPPOST> call, Response<Obj_Inquiry_TELPPOST> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objInquiry = response.body();
                        if (objInquiry.rc.equals("00")) {

                            IDPEL = objInquiry.subscriber;
                            NAMA = objInquiry.display.NAMA;
                            TUNGGAKAN = objInquiry.display.TUNGGAKAN;
                            TOTTAGIHAN = objInquiry.display.TOTAL_TAGIHAN;
                            ADMIN = objInquiry.display.BIAYA_ADMIN;
                            TOTBAYAR = objInquiry.display.HARGA;
                            REFFID = objInquiry.inquiry_num;

                            successDialog(objInquiry.message);

                        } else {
                            AppController.getInstance().errorDialog(getActivity(), objInquiry.message);
                        }
                    } else {
                        SnackbarMessage(relativeLayout, "Inquiry gagal!", Snackbar.LENGTH_INDEFINITE);
                    }
                }

                @Override
                public void onFailure(Call<Obj_Inquiry_TELPPOST> call, Throwable t) {
                    progress.dismiss();
                    SnackbarMessage(relativeLayout, "Inquiry gagal!", Snackbar.LENGTH_INDEFINITE);
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            SnackbarMessage(relativeLayout, "Inquiry gagal!", Snackbar.LENGTH_INDEFINITE);
        }
    }

    private void successDialog(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent(getContext(), TeleponDetail.class);
                        intent.putExtra("PRODUCTID",productCode);
                        intent.putExtra("IDPEL",IDPEL);
                        intent.putExtra("NAMA",NAMA);
                        intent.putExtra("TUNGGAKAN",TUNGGAKAN);
                        intent.putExtra("TOTTAGIHAN",TOTTAGIHAN);
                        intent.putExtra("ADMIN",ADMIN);
                        intent.putExtra("TOTBAYAR",TOTBAYAR);
                        intent.putExtra("REFFID",REFFID);
                        startActivity(intent);
                    }
                }).show();
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);
        snackbar.show();
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
