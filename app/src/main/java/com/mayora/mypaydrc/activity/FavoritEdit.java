package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_ListFavorite_Edit;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoritEdit extends AppCompatActivity {

    private Button btnEdit;
    private EditText editDeskripsi;
    private String ID = "", DESC = "";
    private ImageView imgCLose;

    ProgressDialog progress;
    private Obj_ListFavorite_Edit obj_listFavorite_edit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorite_edit);

        InitControl();
        loadData();

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editDeskripsi.getText().length() == 0){
                    editDeskripsi.setError(getString(R.string.error_field_required));
                    editDeskripsi.requestFocus();
                }else {
                    favoriteEdit(ID);
                }
            }
        });

        imgCLose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void InitControl(){

        editDeskripsi = findViewById(R.id.edit_deskripsi);
        btnEdit = findViewById(R.id.btn_edit);
        imgCLose = findViewById(R.id.imgClose);

    };

    void loadData(){
        Intent intent = getIntent();
        ID = intent.getStringExtra("ID");
        DESC = intent.getStringExtra("DESC");

        editDeskripsi.setText(DESC);
    }

    void favoriteEdit(String ID){
        String decryptPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decryptPin);
        Log.e("PIN", sPin);

        progress = ProgressDialog.show(this,"", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("favorit", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();
        String action = "edit";
        String desc = editDeskripsi.getText().toString();

        try{
            Obj_ListFavorite_Edit param = new Obj_ListFavorite_Edit(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI, action, ID, desc);

            Call<Obj_ListFavorite_Edit> call = NetworkManager.getNetworkService(this).getListFavoriteEdit(param);
            call.enqueue(new Callback<Obj_ListFavorite_Edit>() {
                @Override
                public void onResponse(Call<Obj_ListFavorite_Edit> call, Response<Obj_ListFavorite_Edit> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200){
                        obj_listFavorite_edit = response.body();
                        if (obj_listFavorite_edit.rc.equals("00")){
                            SuccessDialog(obj_listFavorite_edit.message);
                        }else if (obj_listFavorite_edit.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                            AppController.getInstance().errorDialog(FavoritEdit.this, obj_listFavorite_edit.message);

                        }else if (obj_listFavorite_edit.rc.equals("78")){
                            updateDeviceDialog(obj_listFavorite_edit.message);

                        }else{
                            AppController.getInstance().errorDialog(FavoritEdit.this, obj_listFavorite_edit.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_ListFavorite_Edit> call, Throwable t) {
                    progress.dismiss();
                }
            });
        }catch (Exception e){
            progress.dismiss();
        }
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialog));
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getApplicationContext(),Login.class);
                        startActivity(i);
                        finish();

                    }
                }).show();
    }

    private void SuccessDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialog));
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        finish();
                    }
                }).show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!.,])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}
