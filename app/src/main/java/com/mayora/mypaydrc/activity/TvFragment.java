package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Inquiry_Tv;
import com.mayora.mypaydrc.model.Obj_Jenis_Type;
import com.mayora.mypaydrc.model.Obj_SubCategory;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 26/02/2018.
 */

public class TvFragment extends Fragment {
    private EditText editIDpel;
    private Button btnInquiry;
    private RelativeLayout relativeLayout;

    ProgressDialog progress;
    private Obj_Inquiry_Tv objInquiry;
    private String sProduct ="", sSubscriber = "";
    private String IDPEL = "", NAMA = "", HARGA = "", TAGIHAN_BULAN = "",BIAYA_ADMIN = "", TOTAL_TAGIHAN = "", REFFID = "",
            PRODUCT = "", INQUIRY_NUM = "";

    private ArrayList<Obj_Jenis_Type> arrJenis = new ArrayList<Obj_Jenis_Type>();

    ProgressDialog progressDialog;
    private Obj_SubCategory objSubCategory;
    private String productCode = "";
    private Spinner spinJenis;
    private String NO="", CATEGORY = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null) {
            NO = getArguments().getString("NO");
            CATEGORY = getArguments().getString("CATEGORY");
        }

        View view = inflater.inflate(R.layout.frag_tv, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);

        if(CATEGORY != null) {
            if (CATEGORY.equals("4")) {
                editIDpel.setText(NO);
            }
        }

        editIDpel.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        try {
            FillJenis();
        }catch(Exception e){
            e.printStackTrace();
        }

        editIDpel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editIDpel.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    return true;
                }
                return false;
            }
        });

        spinJenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Obj_Jenis_Type jenis = (Obj_Jenis_Type) parent.getSelectedItem();
                productCode = jenis.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // hide virtual keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editIDpel.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN);

                if(editIDpel.getText().length() > 0){
                    Inquiry();
                }

            }
        });


    }

    private void InitControl(View v) {
        relativeLayout = v.findViewById(R.id.layout_no);
        editIDpel = v.findViewById(R.id.edit_no);
        btnInquiry = v.findViewById(R.id.btnInquiry);
        spinJenis = v.findViewById(R.id.spin_jenis);
    }

    void FillJenis(){

        progressDialog = ProgressDialog.show(getActivity(),"Informasi", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("productinquiry", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_SubCategory param = new Obj_SubCategory(AppConfig.USERID, AppConfig.REQID, "4", AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_SubCategory> call = NetworkManager.getNetworkService(getActivity()).getSubCategoryProd(param);
            call.enqueue(new Callback<Obj_SubCategory>() {
                @Override
                public void onResponse(Call<Obj_SubCategory> call, Response<Obj_SubCategory> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        String jName = "";
                        String jId = "";
                        objSubCategory = response.body();
                        if (objSubCategory.rc.equals("00")){
                            for (Obj_SubCategory.Datum dat : objSubCategory.data){
                                jName = dat.name;
                                jId = dat.product_id;
                                arrJenis.add(new Obj_Jenis_Type(jId, jName));
                            }
                            adapterJenis(arrJenis);

                        }else if (objSubCategory.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                            AppController.getInstance().errorDialog(getContext(), "Layanan Tagihan Telepon tidak tersedia!");
                        }else if (objSubCategory.rc.equals("78")){
                            updateDeviceDialog(objSubCategory.message);
                        }else{
                            AppController.getInstance().errorDialog(getContext(), "Layanan Tagihan Telepon tidak tersedia!");
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_SubCategory> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
        }
    }

    void adapterJenis(ArrayList<Obj_Jenis_Type> data){
        ArrayAdapter<Obj_Jenis_Type> adapter = new ArrayAdapter<Obj_Jenis_Type>(getContext(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinJenis.setAdapter(adapter);
    }

    private void Inquiry() {
        progress = ProgressDialog.show(getActivity(), "Info", "Checking data", true);

        sProduct = productCode;
        String subscriber = editIDpel.getText().toString();
        sSubscriber = subscriber;
        String version = AppController.getInstance().getVersionName();
        String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(Pin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("inquiry", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Inquiry_Tv param = new Obj_Inquiry_Tv (AppConfig.IMEI, AppConfig.KEY, sProduct, AppConfig.REQID, sSubscriber, AppConfig.USERID, version);
            Call<Obj_Inquiry_Tv> call = NetworkManager.getNetworkService(getContext()).inquiryTv(param);
            call.enqueue(new Callback<Obj_Inquiry_Tv>() {
                @Override
                public void onResponse(Call<Obj_Inquiry_Tv> call, Response<Obj_Inquiry_Tv> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objInquiry = response.body();
                        if (objInquiry.rc.equals("00")) {

                            IDPEL = objInquiry.subscriber;
                            NAMA = objInquiry.display.NAMA;
                            HARGA = objInquiry.display.HARGA;
                            TAGIHAN_BULAN = objInquiry.display.TAGIHAN_BULAN;
                            TOTAL_TAGIHAN = objInquiry.display.TOTAL_TAGIHAN;
                            BIAYA_ADMIN = objInquiry.display.BIAYA_ADMIN;
                            REFFID = objInquiry.reference;
                            PRODUCT = objInquiry.product;
                            INQUIRY_NUM = objInquiry.inquiry_num;

                            successDialog(objInquiry.message);

                        } else {
                            errorDialog(objInquiry.message);
                        }
                    } else {
                        Toast.makeText(getContext(), "Inquiry gagal!", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Obj_Inquiry_Tv> call, Throwable t) {
                    progress.dismiss();
                    Toast.makeText(getContext(), "Inquiry gagal!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            Toast.makeText(getContext(), "Inquiry gagal!", Toast.LENGTH_LONG).show();
        }
    }

    private void successDialog(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent(getContext(), TvDetail.class);
                        intent.putExtra("IDPEL",IDPEL);
                        intent.putExtra("NAMA",NAMA);
                        intent.putExtra("HARGA",HARGA);
                        intent.putExtra("TAGIHAN_BULAN",TAGIHAN_BULAN);
                        intent.putExtra("TOTAL_TAGIHAN",TOTAL_TAGIHAN);
                        intent.putExtra("ADMIN",BIAYA_ADMIN);
                        intent.putExtra("REFFID",REFFID);
                        intent.putExtra("PRODUCT",PRODUCT);
                        intent.putExtra("INQUIRY_NUM",INQUIRY_NUM);
                        startActivity(intent);
                    }
                }).show();
    }

    private void errorDialog(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                }).show();
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);
        snackbar.show();
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
