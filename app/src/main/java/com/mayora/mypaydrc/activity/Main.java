package com.mayora.mypaydrc.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ViewPagerAdapter;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.services.AlarmReceiver;
import com.mayora.mypaydrc.services.LogoutService;

import java.util.Calendar;

/**
 * Created by eroy on 3/2/2017.
 */

public class Main extends AppCompatActivity {
    private Toolbar mToolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int FLAG_PENDINGINTENT = 2107;

    private Menu menu;
    private TextView notifCountTxt;
    int notifCount=0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        AppController.getInstance().requestAppPermissions(Main.this);
        AppController.getInstance().getFCMID(Main.this);


        InitControl();
    }

    void InitControl(){
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    private void setupTabIcons() {
        float scale = getResources().getConfiguration().fontScale;
        // This scale tells you what the font is scaled to.
        // So if you want size 16, then set the size as 16/scale
        float sizeNeeded = (float) 12.0;

        TextView tabHome = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabHome.setText("Beranda");
        tabHome.setTextSize(sizeNeeded/scale);
        tabHome.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_home_menu, 0, 0);
        tabHome.setSelected(true);
        tabLayout.getTabAt(0).setCustomView(tabHome);

        TextView tabTrans = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTrans.setText("Transaksi");
        tabTrans.setTextSize(sizeNeeded/scale);
        tabTrans.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_note_menu, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTrans);

        TextView tabHarga = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabHarga.setText("Harga");
        tabHarga.setTextSize(sizeNeeded/scale);
        tabHarga.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_tag_menu, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabHarga);

        TextView tabPromo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabPromo.setText("Promo");
        tabPromo.setTextSize(sizeNeeded/scale);
        tabPromo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_promo_menu, 0, 0);
        tabLayout.getTabAt(3).setCustomView(tabPromo);

        TextView tabProfil = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabProfil.setText("Profil");
        tabProfil.setTextSize(sizeNeeded/scale);
        tabProfil.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_profile_menu, 0, 0);
        tabLayout.getTabAt(4).setCustomView(tabProfil);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), "", "");
        adapter.addFrag(new HomeFragment(), "Beranda");
        adapter.addFrag(new TransaksiFragment(), "Transaksi");
        adapter.addFrag(new PriceFragment(), "Harga");
        adapter.addFrag(new PromoFragment(), "Promo");
        adapter.addFrag(new ProfilFragment(), "Profil");
        viewPager.setAdapter(adapter);

    }

    void StartServiceLogout(){
        Log.d("ServiceLogout", "Start");
        Intent myIntent = new Intent(getBaseContext(),
                AlarmReceiver.class);

        PendingIntent pendingIntent
                = PendingIntent.getBroadcast(getBaseContext(),
                FLAG_PENDINGINTENT, myIntent, 0);

        AlarmManager alarmManager
                = (AlarmManager)getSystemService(ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.MINUTE, 15);
        alarmManager.set(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), pendingIntent);
    }

    void StopServiceLogout(){
        Log.d("ServiceLogout", "Stop");
        Intent myIntent = new Intent(getBaseContext(),
                AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), FLAG_PENDINGINTENT, myIntent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                int resultCode = bundle.getInt(LogoutService.RESULT);
                if (resultCode == RESULT_OK) {
                    // intent Login
                    unregisterReceiver(receiver);

                    Intent mIntent = new Intent(getBaseContext(), Login.class);
                    startActivity(mIntent);
                    finish();
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        notifCount = AppController.getInstance().getSessionManager().getIntegerData(AppConfig.NOTIF_COUNTER);
        setupBadge(notifCount);

        //CheckUserData();
        StopServiceLogout();

        try {
            StopServiceLogout();
            registerReceiver(receiver, new IntentFilter(LogoutService.NOTIFICATION));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            StartServiceLogout();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        try{
            unregisterReceiver(receiver);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);

        final MenuItem menuItem = menu.findItem(R.id.notif);
        View actionView = menuItem.getActionView();
        notifCountTxt = actionView.findViewById(R.id.notification_badge);

        notifCount = AppController.getInstance().getSessionManager().getIntegerData(AppConfig.NOTIF_COUNTER);
        setupBadge(notifCount);

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notif:
                Intent i = new Intent(getApplicationContext(), Notifikasi.class);
                startActivity(i);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void setupBadge(Integer notifCount){
        if (notifCountTxt != null) {
            if (notifCount == 0) {
                if (notifCountTxt.getVisibility() != View.GONE) {
                    notifCountTxt.setVisibility(View.GONE);
                }
            } else {
                notifCountTxt.setText(String.valueOf(Math.min(notifCount, 99)));
                if (notifCountTxt.getVisibility() != View.VISIBLE) {
                    notifCountTxt.setVisibility(View.VISIBLE);
                }
            }
        }
    }

}
