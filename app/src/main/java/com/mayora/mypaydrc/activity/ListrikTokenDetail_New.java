package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ItemNominalTokenAdapter_New;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_NominalToken;
import com.mayora.mypaydrc.model.Obj_Payment_PLNPRE;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListrikTokenDetail_New extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView txtNama,txtNoMeter, txtTarifDaya;
    private Button btnBayar;
    private LinearLayout linearLayout;

    private ItemNominalTokenAdapter_New itemNominalTokenAdapterNew;
    private RecyclerView recyclerView;

    ProgressDialog progressDialog;
    Obj_NominalToken objNominal;

    private Obj_Payment_PLNPRE objPayment;
    private String nopel="", prod_id="", harga="", nama="", tarifdaya="", reffid="", price="";
    private String IDPEL = "", NOMETER = "", NAMAPELANGGAN = "", TOTTAGIHAN = "", REFFID = "", ADMIN="", TOTBAYAR="", RECEIPT="", MSG="", SN="", TARIFDAYA="", JUMLAHKWH="", PPN="", PPJ="", TOKEN="",
            KWH = "", BULAN ="", TOTALLEMBAR="",RUPIAHTAGIHAN="", STANDMETER="", RPTAGPLN="", NOREF="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listrik_token_detail_new);

        GetExtrasData();
        InitControl();
    }

    void InitControl(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NestedScrollView scroll = (NestedScrollView) findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        linearLayout = (LinearLayout)findViewById(R.id.layout_pulsa_detail);
        txtNoMeter = (TextView)findViewById(R.id.txt_nometer);
        txtNoMeter.setText(nopel);
        txtNama = (TextView)findViewById(R.id.txt_nama);
        txtNama.setText(nama);
        txtTarifDaya = (TextView)findViewById(R.id.txt_tarifdaya);
        txtTarifDaya.setText(tarifdaya);

        FillNominal();

        btnBayar = (Button)findViewById(R.id.btnBayar);
        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TOTTAGIHAN.equals("")){
                    SnackbarMessage(linearLayout, "Anda belum memilih nominal!", Snackbar.LENGTH_INDEFINITE);
                }else{
                    PaymentProccess();
                }

            }
        });
    }

    void GetExtrasData(){
        Intent intent = getIntent();
        nopel = intent.getStringExtra("NOMETER");
        nama = intent.getStringExtra("NAMA");
        tarifdaya = intent.getStringExtra("TARIFDAYA");
        reffid = intent.getStringExtra("REFFID");
        //arrNom = intent.getStringArrayListExtra("ARRNOMINAL");
        //Log.d("ArrNom",arrNom.toString());
    }

    private void PaymentProccess() {
        progressDialog = ProgressDialog.show(this, "Info", "Checking data", true);

        String sPrice = harga.replace(".00","");

        String version = AppController.getInstance().getVersionName();
        String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(Pin);
        Log.e("PIN", sPin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("payment", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            //device,key,product,reference,subscriber,user,price,version,inquiry_num
            Obj_Payment_PLNPRE param = new Obj_Payment_PLNPRE(AppConfig.IMEI, AppConfig.KEY, prod_id,
                    AppConfig.REQID, nopel , AppConfig.USERID, price, version, reffid);
            Call<Obj_Payment_PLNPRE> call = NetworkManager.getNetworkService(this).payment(param);
            call.enqueue(new Callback<Obj_Payment_PLNPRE>() {
                @Override
                public void onResponse(Call<Obj_Payment_PLNPRE> call, Response<Obj_Payment_PLNPRE> response) {
                    int code = response.code();
                    progressDialog.dismiss();

                    if (code == 200) {
                        objPayment = response.body();
                        if (!(objPayment.rc == null)) {
                            if (objPayment.rc.equals("00")) {

                                try {
                                    MSG = objPayment.message;
                                    NOMETER = objPayment.display.NOMETER.trim();
                                    IDPEL = objPayment.display.IDPEL.trim();
                                    NAMAPELANGGAN = objPayment.display.NAMA;
                                    TARIFDAYA = objPayment.display.TARIF.trim();
                                    JUMLAHKWH = objPayment.display.KWH.trim();
                                    TOTTAGIHAN = objPayment.display.RP_STROOM.trim();
                                    PPN = objPayment.display.PPN.trim();
                                    PPJ = objPayment.display.PPJ.trim();
                                    ADMIN = objPayment.display.BIAYA_ADMIN.trim();
                                    TOTBAYAR = objPayment.display.RP_BAYAR.trim();
                                    REFFID = objPayment.display.NOREF.trim();
                                    SN = objPayment.display.SN;
                                    TOKEN = objPayment.display.TOKEN.trim();
                                    //BALANCE = objPayment.balance;
                                    MSG = objPayment.message;
                                    RECEIPT = objPayment.receipt;

                                    Intent mIntent = new Intent(getApplicationContext(), ListrikTokenStatusNew.class);
                                    mIntent.putExtra("MSG", MSG);
                                    mIntent.putExtra("IDPEL", IDPEL);
                                    mIntent.putExtra("NOMETER", NOMETER);
                                    mIntent.putExtra("NAMA", NAMAPELANGGAN);
                                    mIntent.putExtra("TARIFDAYA", TARIFDAYA);
                                    mIntent.putExtra("JUMLAHKWH", JUMLAHKWH);
                                    mIntent.putExtra("TOTTAGIHAN", TOTTAGIHAN);
                                    mIntent.putExtra("PPN", PPN);
                                    mIntent.putExtra("PPJ", PPJ);
                                    mIntent.putExtra("ADMIN", ADMIN);
                                    mIntent.putExtra("TOTBAYAR", TOTBAYAR);
                                    mIntent.putExtra("NOREF", REFFID);
                                    mIntent.putExtra("SN", SN);
                                    mIntent.putExtra("TOKEN", TOKEN);
                                    //mIntent.putExtra("BALANCE", BALANCE);
                                    mIntent.putExtra("PRODUCTID", prod_id);
                                    mIntent.putExtra("RECEIPT", RECEIPT);
                                    startActivity(mIntent);
                                    finish();

                                } catch (Exception e) {
                                    SnackbarMessage(linearLayout, "Pembelian Token Listrik gagal!", Snackbar.LENGTH_INDEFINITE);
                                    Log.e("ErrorPLNToken", e.toString());
                                }

                            } else {
                                SnackbarMessage(linearLayout, objPayment.message, Snackbar.LENGTH_INDEFINITE);
                            }
                        }else{
                            SnackbarMessage(linearLayout, "Pembelian Token Listrik gagal!", Snackbar.LENGTH_INDEFINITE);
                            Log.e("ErrorPLNToken", "null");
                        }
                    } else {
                        SnackbarMessage(linearLayout, "Pembelian Token Listrik gagal!", Snackbar.LENGTH_INDEFINITE);
                    }
                }

                @Override
                public void onFailure(Call<Obj_Payment_PLNPRE> call, Throwable t) {
                    progressDialog.dismiss();
                    SnackbarMessage(linearLayout, "Pembelian Token Listrik gagal!", Snackbar.LENGTH_INDEFINITE);
                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            SnackbarMessage(linearLayout, "Pembelian Token Listrik gagal!", Snackbar.LENGTH_INDEFINITE);
        }
    }

    void FillNominal(){
        recyclerView.setAdapter(null);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("priceplnprepaid", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_NominalToken param = new Obj_NominalToken(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI, txtNoMeter.getText().toString());

            Call<Obj_NominalToken> call = NetworkManager.getNetworkService(getApplicationContext()).getNomToken(param);
            call.enqueue(new Callback<Obj_NominalToken>() {
                @Override
                public void onResponse(Call<Obj_NominalToken> call, Response<Obj_NominalToken> response) {
                    int code = response.code();
                    if (code == 200){
                        objNominal = response.body();
                        if (objNominal.rc.equals("00")){
                            if (objNominal.data == null || objNominal.data.size() == 0){
                                recyclerView.setAdapter(null);
                                SnackbarMessage(linearLayout, "Token Listrik tidak tersedia!", Snackbar.LENGTH_INDEFINITE);
                            }else{
                                FillAdapter();
                            }
                        }else if (objNominal.rc.equals("73")){
                            recyclerView.setAdapter(null);
                            SnackbarMessage(linearLayout, "Token Listrik tidak tersedia!", Snackbar.LENGTH_INDEFINITE);
                        }else if (objNominal.rc.equals("78")){
                            recyclerView.setAdapter(null);
                            updateDeviceDialog(objNominal.message);
                        }else{
                            recyclerView.setAdapter(null);
                            SnackbarMessage(linearLayout, "Token Listrik tidak tersedia!", Snackbar.LENGTH_INDEFINITE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_NominalToken> call, Throwable t) {
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            recyclerView.setAdapter(null);
        }
    }

    void FillAdapter(){
        recyclerView.setAdapter(null);
        itemNominalTokenAdapterNew = new ItemNominalTokenAdapter_New(getApplicationContext(), objNominal, new ItemNominalTokenAdapter_New.OnDownloadClicked(){
            @Override
            public void OnDownloadClicked(String NOMINAL, String HARGA, String PROD_ID, String PROD_NAME) {

                RUPIAHTAGIHAN = AppController.getInstance().toNumberFormatIDR(Double.parseDouble(NOMINAL));
                TOTTAGIHAN = NOMINAL;
                price = HARGA;
                prod_id = PROD_ID;
            }
        });
        recyclerView.setAdapter(itemNominalTokenAdapterNew);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);
        snackbar.show();
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getApplicationContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getApplicationContext(),Login.class);
                        startActivity(i);
                        finish();

                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}