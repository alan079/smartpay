package com.mayora.mypaydrc.activity;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ViewPagerAdapter;

/**
 * Created by eroy on 3/2/2017.
 */

public class Topup extends AppCompatActivity {
    private Toolbar mToolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_topup_menu,
            R.drawable.ic_history_menu
    };
    private String[] tabTitles = {"Top Up","Riwayat Top Up"};
    private Menu menu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topup);

        InitControl();
        //CekSaldo();
    }

    void InitControl(){
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

    }

    private void setupTabIcons() {

        int i = 0;
        for (int count = viewPager.getAdapter().getCount(); i < count ; i++) {
            LinearLayout tabLinearLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab_content, null);
            TextView tabContent = (TextView) tabLinearLayout.findViewById(R.id.tabContent);
            tabContent.setText("  " + tabTitles[i]);
            tabContent.setCompoundDrawablesWithIntrinsicBounds(tabIcons[i], 0, 0, 0);
            tabLayout.getTabAt(i).setCustomView(tabContent);
        }

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), "", "");
        adapter.addFrag(new TopupFragment(), "Top Up");
        adapter.addFrag(new TopupHistoryFragment(), "Riwayat Top Up");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_topup, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            //optionmenu
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                break;

            case R.id.guide_topup:
                // app icon in action bar clicked; go home
                Intent i = new Intent(getApplicationContext(), Guide_Topup.class);
                startActivity(i);
                break;

        }
        return true;
    }

}
