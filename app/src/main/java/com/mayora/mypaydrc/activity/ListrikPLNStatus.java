package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_ListFavorite_Add;
import com.mayora.mypaydrc.net.NetworkManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListrikPLNStatus extends AppCompatActivity {

    private String PRODUCTID = "", MSG = "", IDPEL = "", NAMA = "", BULAN = "", TUNGGAKAN = "", TOTTAGIHAN = "", REFFID = "",
            NOREF = "", ADMIN = "", TOTBAYAR = "", SN = "", RECEIPT = "", TARIF = "", TIPE_TRANS = "";
    private TextView txtStatus, txtIDPel, txtNamaPel, txtBulan, txtTarif, txtNoref, txtAdmin, txtTotBayar, txtTotTagihan;
    private Button btnFinish;
    private ImageView btnPrint, btnShare, btnFavorite;

    // Progress Dialog
    private ProgressDialog pDialog;
    ProgressDialog progress;
    public static final int progress_bar_type = 0;

    private TextView title_bulan, title_tarif;
    Obj_ListFavorite_Add obj_listFavorite_add;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listrik_status);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        InitControl();
        LoadData();

        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.image_click));

                if (RECEIPT.equals("")) {
                    Intent mIntent = new Intent(getApplicationContext(), PreviewStrukPLN.class);
                    mIntent.putExtra("IDPEL", IDPEL);
                    mIntent.putExtra("NAMA", NAMA);
                    mIntent.putExtra("BULAN", BULAN);
                    mIntent.putExtra("TOTTAGIHAN", TOTTAGIHAN);
                    mIntent.putExtra("ADMIN", ADMIN);
                    mIntent.putExtra("TUNGGAKAN", TUNGGAKAN);
                    mIntent.putExtra("TOTBAYAR", TOTBAYAR);
                    mIntent.putExtra("PRODUCTID", PRODUCTID);
                    mIntent.putExtra("NOREF", NOREF);
                    mIntent.putExtra("SN", SN);
                    mIntent.putExtra("TIPE", TIPE_TRANS);
                    startActivity(mIntent);
                } else {
                    DownloadStruk(RECEIPT);
                }

            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.image_click));

                if (RECEIPT.equals("")) {
                    shareData();
                } else {
                    shareDataPdf(RECEIPT);
                }

            }
        });

        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.image_click));

                addFavoriteDialog();
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void InitControl() {

        NestedScrollView scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        title_bulan = findViewById(R.id.txt_title_bulan);
        title_tarif = findViewById(R.id.txt_title_tarif);
        txtStatus = findViewById(R.id.txt_status);
        txtIDPel = findViewById(R.id.txt_idpel);
        txtNamaPel = findViewById(R.id.txt_nama);
        txtBulan = findViewById(R.id.txt_bulan);
        txtNoref = findViewById(R.id.txt_noref);
        txtTotTagihan = findViewById(R.id.txt_totaltagihan);
        txtAdmin = findViewById(R.id.txt_admin);
        txtTarif = findViewById(R.id.txt_tarif);
        txtTotBayar = findViewById(R.id.txt_totbayar);

        btnPrint = findViewById(R.id.btn_print);
        btnShare = findViewById(R.id.btn_share);
        btnFavorite = findViewById(R.id.btn_favorite);
        btnFinish = findViewById(R.id.btn_finish);


    }

    void LoadData() {
        Intent mIntent = getIntent();
        MSG = mIntent.getStringExtra("MSG");
        IDPEL = mIntent.getStringExtra("IDPEL");
        NAMA = mIntent.getStringExtra("NAMA");
        BULAN = mIntent.getStringExtra("BULAN");
        TOTTAGIHAN = mIntent.getStringExtra("TOTTAGIHAN");
        ADMIN = mIntent.getStringExtra("ADMIN");
        TUNGGAKAN = mIntent.getStringExtra("TUNGGAKAN");
        TOTBAYAR = mIntent.getStringExtra("TOTBAYAR");
        PRODUCTID = mIntent.getStringExtra("PRODUCTID");
        NOREF = mIntent.getStringExtra("NOREF");
        SN = mIntent.getStringExtra("SN");
        RECEIPT = mIntent.getStringExtra("RECEIPT");
        TARIF = mIntent.getStringExtra("TARIF");
        TIPE_TRANS = mIntent.getStringExtra("TIPE");

        txtStatus.setText(MSG);
        if (MSG.toUpperCase().contains("SUKSES")) {
            txtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.md_green_500));
        } else if (MSG.toUpperCase().contains("GAGAL")) {
            txtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.md_red_500));
        } else {
            txtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.md_yellow_800));
        }
        txtIDPel.setText(IDPEL);
        txtNamaPel.setText(NAMA);
        if (TIPE_TRANS != null) {
            title_bulan.setText("Tipe Transaksi");
            txtBulan.setText(TIPE_TRANS);
            title_tarif.setVisibility(View.GONE);
            txtTarif.setVisibility(View.GONE);
        } else {
            title_bulan.setText("Bulan Tagihan");
            txtBulan.setText(BULAN);
            title_tarif.setText("Tarif/Daya");
            txtTarif.setText(TARIF);

        }
        txtTotTagihan.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTTAGIHAN)));
        txtAdmin.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(ADMIN)));
        txtTotBayar.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTBAYAR)));
        txtNoref.setText(NOREF);
    }

    void favoriteAdd() {

        String decryptPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decryptPin);
        Log.e("PIN", sPin);

        progress = ProgressDialog.show(this, "", "Proses Tambah Favorit", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("favorit", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();
        String action = "add";

        try {
            Obj_ListFavorite_Add param = new Obj_ListFavorite_Add(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI, action, "3", PRODUCTID, TOTBAYAR, IDPEL, NAMA);

            Call<Obj_ListFavorite_Add> call = NetworkManager.getNetworkService(this).getListFavoriteAdd(param);
            call.enqueue(new Callback<Obj_ListFavorite_Add>() {
                @Override
                public void onResponse(Call<Obj_ListFavorite_Add> call, Response<Obj_ListFavorite_Add> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        obj_listFavorite_add = response.body();
                        if (obj_listFavorite_add.rc.equals("00")) {
                            AppController.getInstance().errorDialog(ListrikPLNStatus.this, obj_listFavorite_add.message);

                        } else if (obj_listFavorite_add.rc.equals("73")) {
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                            AppController.getInstance().errorDialog(ListrikPLNStatus.this, obj_listFavorite_add.message);

                        } else if (obj_listFavorite_add.rc.equals("78")) {
                            updateDeviceDialog(obj_listFavorite_add.message);

                        } else {
                            AppController.getInstance().errorDialog(ListrikPLNStatus.this, obj_listFavorite_add.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_ListFavorite_Add> call, Throwable t) {
                    progress.dismiss();
                }
            });
        } catch (Exception e) {
            progress.dismiss();
        }
    }

    private void shareData() {
        if (TIPE_TRANS != null) {
            StringBuilder data = new StringBuilder();
            data.append("BANK MAYORA \n");
            data.append("SMARTPAY \n");
            data.append("\n");
            data.append(String.format("%-10s : %s", "NO. REF", NOREF));
            data.append("\n");
            data.append(String.format("%-10s : %s", "ID PELANGGAN", IDPEL));
            data.append("\n");
            data.append(String.format("%-10s : %s", "NAMA PELANGGAN", NAMA));
            data.append("\n");
            data.append(String.format("%-10s : %s", "TIPE TRANSAKSI", TIPE_TRANS));
            data.append("\n");
            data.append(String.format("%-10s : %s", "TOTAL TAGIHAN", TOTTAGIHAN));
            data.append("\n");
            data.append(String.format("%-10s : %s", "BIAYA ADMIN", ADMIN));
            data.append("\n");
            data.append(String.format("%-10s : %s", "TOTAL BAYAR", TOTBAYAR));
            data.append("\n\n");
            data.append("Terima Kasih\n");
            data.append("Simpan struk ini sebagai bukti pembayaran yang sah\r\n");

            try {
                convertImage(data.toString(), NOREF);
            } catch (Exception e) {
                e.printStackTrace();
            }

            File outputFile = new File(AppConfig.STORAGE_CARD + "/Download/" + NOREF + ".png");
            Uri uri = Uri.fromFile(outputFile);

            if (outputFile.exists()) {
                Intent share = new Intent();
                share.setAction(Intent.ACTION_SEND);
                share.setType("image/*");
                share.putExtra(Intent.EXTRA_STREAM, uri);
                startActivity(Intent.createChooser(share, "Kirim Struk"));
            } else {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, data.toString());
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Kirim Struk"));
            }
        } else {
            StringBuilder data = new StringBuilder();
            data.append("BANK MAYORA \n");
            data.append("SMARTPAY \n");
            data.append("\n");
            data.append(String.format("%-10s : %s", "NO. REF", NOREF));
            data.append("\n");
            data.append(String.format("%-10s : %s", "ID PELANGGAN", IDPEL));
            data.append("\n");
            data.append(String.format("%-10s : %s", "NAMA PELANGGAN", NAMA));
            data.append("\n");
            data.append(String.format("%-10s : %s", "BULAN TAGIHAN", BULAN));
            data.append("\n");
            data.append(String.format("%-10s : %s", "TOTAL TAGIHAN", TOTTAGIHAN));
            data.append("\n");
            data.append(String.format("%-10s : %s", "TARIF/DAYA", TARIF));
            data.append("\n");
            data.append(String.format("%-10s : %s", "BIAYA ADMIN", ADMIN));
            data.append("\n");
            data.append(String.format("%-10s : %s", "TOTAL BAYAR", TOTBAYAR));
            data.append("\n\n");
            data.append("Terima Kasih\n");
            data.append("Simpan struk ini sebagai bukti pembayaran yang sah\r\n");

            try {
                convertImage(data.toString(), NOREF);
            } catch (Exception e) {
                e.printStackTrace();
            }

            File outputFile = new File(AppConfig.STORAGE_CARD + "/Download/" + NOREF + ".png");
            Uri uri = Uri.fromFile(outputFile);

            if (outputFile.exists()) {
                Intent share = new Intent();
                share.setAction(Intent.ACTION_SEND);
                share.setType("image/*");
                share.putExtra(Intent.EXTRA_STREAM, uri);
                startActivity(Intent.createChooser(share, "Kirim Struk"));
            } else {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, data.toString());
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Kirim Struk"));
            }
        }
    }

    private void shareDataPdf(String sUrl) {
        AppConfig.PDF_FILENAME = AppController.getInstance().getFileNameDoc(sUrl);

        File outputFile = new File(AppConfig.STORAGE_CARD + "/Download/" + AppConfig.PDF_FILENAME);
        Uri uri = Uri.fromFile(outputFile);

        if (outputFile.exists()) {
            Intent share = new Intent();
            share.setAction(Intent.ACTION_SEND);
            share.setType("application/pdf");
            share.putExtra(Intent.EXTRA_STREAM, uri);
            //share.setPackage("com.whatsapp");
            startActivity(Intent.createChooser(share, "Kirim Struk"));
        } else {
            DownloadStruk(RECEIPT);

        }
    }

    void DownloadStruk(String sUrl) {

        AppConfig.PDF_FILENAME = AppController.getInstance().getFileNameDoc(sUrl);

        File file = new File(AppConfig.STORAGE_CARD + "/Download/" + AppConfig.PDF_FILENAME);
        if (file.exists()) {
            Intent mIntent = new Intent(getApplicationContext(), PDFViewActivity.class);
            startActivity(mIntent);
        } else {
            new DownloadFileFromURL().execute(RECEIPT);
        }
    }

    /**
     * Showing Dialog
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                AppConfig.PDF_FILENAME = AppController.getInstance().getFileNameDoc(url.toString());

                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream());

                // Output stream
                OutputStream output = new FileOutputStream(AppConfig.STORAGE_CARD + "/Download/" + AppConfig.PDF_FILENAME);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();
                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);

            Intent mIntent = new Intent(getApplicationContext(), PDFViewActivity.class);
            startActivity(mIntent);
        }

    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    void convertImage(final String text, String fname) throws IOException {
        Log.d("ConvertImage", fname);
        final Rect bounds = new Rect();
        TextPaint textPaint = new TextPaint() {
            {
                setColor(Color.BLACK);
                setTextAlign(Paint.Align.LEFT);
                setTextSize(22f);
                setAntiAlias(true);
            }
        };
        textPaint.getTextBounds(text, 0, text.length(), bounds);
        StaticLayout mTextLayout = new StaticLayout(text, textPaint,
                bounds.width(), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        int maxWidth = -1;
        for (int i = 0; i < mTextLayout.getLineCount(); i++) {
            if (maxWidth < mTextLayout.getLineWidth(i)) {
                maxWidth = (int) mTextLayout.getLineWidth(i);
            }
        }
        final Bitmap bmp = Bitmap.createBitmap(maxWidth, mTextLayout.getHeight(),
                Bitmap.Config.ARGB_8888);
        bmp.eraseColor(Color.WHITE);// just adding black background
        final Canvas canvas = new Canvas(bmp);
        mTextLayout.draw(canvas);
        FileOutputStream stream = new FileOutputStream(AppConfig.STORAGE_CARD + "/Download/" + fname + ".png");
        bmp.compress(Bitmap.CompressFormat.PNG, 85, stream);
        bmp.recycle();
        stream.close();
    }

    private void addFavoriteDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialog));
        dialog.setMessage("Tambahkan transaksi ini di menu favorit?")
                .setCancelable(false)
                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        favoriteAdd();

                    }
                })
                .setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                    }
                }).show();
    }

    private void updateDeviceDialog(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialog));
        dialog.setMessage(msg + " . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getApplicationContext(), Login.class);
                        startActivity(i);
                        finish();

                    }
                }).show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}
