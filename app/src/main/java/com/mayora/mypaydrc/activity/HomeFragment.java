package com.mayora.mypaydrc.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.MenuCardMainAdapter;
import com.mayora.mypaydrc.adapter.RecyclerItemClickListener;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.MenuCardModel;
import com.mayora.mypaydrc.model.Obj_CekSaldo;
import com.mayora.mypaydrc.model.Obj_SummaryTrx;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private MenuCardMainAdapter menuAdapter;
    private List<MenuCardModel> menuList;
    private RecyclerView recyclerView;
    private TextView txtSaldo, txtSumTrans, txtWaktu, dashboardSaldo, dashboardSum;
    private LinearLayout layout_topup;
    Obj_CekSaldo cekSaldo;
    Obj_SummaryTrx sumTrx;

    private Button btnRefresh;
    private Animation animation;
    int count = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_home, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);


    }

    @SuppressLint("NewApi")
    private void InitControl(View v){

        float scale = getResources().getConfiguration().fontScale;
        // This scale tells you what the font is scaled to.
        // So if you want size 16, then set the size as 16/scale
        float sizeWaktu = (float) 11.0;
        float sizeNeeded = (float) 12.0;
        float sizeNominal = (float) 15.0;

        dashboardSaldo = v.findViewById(R.id.dashboard_title_saldo);
        dashboardSaldo.setTextSize(sizeNeeded/scale);
        dashboardSum = v.findViewById(R.id.dashboard_title_trans);
        dashboardSum.setTextSize(sizeNeeded/scale);
        txtSaldo = v.findViewById(R.id.txt_saldo);
        txtSaldo.setTextSize(sizeNominal/scale);
        txtSumTrans = v.findViewById(R.id.txt_trans);
        txtSumTrans.setTextSize(sizeNominal/scale);
        txtWaktu = v.findViewById(R.id.txt_waktu);
        txtWaktu.setTextSize(sizeWaktu/scale);

        //iniate menu card adapter
        recyclerView = v.findViewById(R.id.recycler_view);
        recyclerView.setNestedScrollingEnabled(true);

        layout_topup = v.findViewById(R.id.layout_topup);
        layout_topup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent topup = new Intent(getContext(), Topup.class);
                topup.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(topup);
            }
        });

        menuList = new ArrayList<>();
        menuAdapter = new MenuCardMainAdapter(getActivity(), menuList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 4);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(menuAdapter);

        prepareMenuCard();

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        switch (position) {
                            case 0:
                                Intent favorite = new Intent(getContext(), Favorit.class);
                                favorite.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(favorite);
                                break;
                            case 1:
                                Intent mypulsa = new Intent(getContext(), Pulsa.class);
                                mypulsa.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(mypulsa);
                                break;
                            case 2:
                                Intent listrik = new Intent(getContext(), Listrik.class);
                                listrik.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(listrik);
                                break;
                            case 3:
                                Intent tele = new Intent(getContext(), Telepon.class);
                                tele.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(tele);
                                break;
                            case 4:
                                Intent pdam = new Intent(getContext(), PDAM.class);
                                pdam.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(pdam);
                                break;
                            case 5:
                                Intent tiket = new Intent(getContext(), Tiket.class);
                                tiket.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(tiket);
                                break;
                            case 6:
                                Intent tv = new Intent(getContext(), Tv.class);
                                tv.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(tv);
                                break;
                            case 7:
                                Intent inet = new Intent(getContext(), Internet.class);
                                inet.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(inet);
                                break;
                            case 8:
                                Intent bpjs = new Intent(getContext(), BPJS.class);
                                bpjs.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(bpjs);
                                break;
                            case 9:
                                Intent pajak = new Intent(getContext(), Pajak.class);
                                pajak.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(pajak);
                                break;
                            case 10:
                                Intent emoney = new Intent(getContext(), Emoney.class);
                                emoney.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(emoney);
                                break;

                            default:
                                break;
                        }
                    }

                    @Override public void onLongItemClick(View view, int position) {

                    }
                })
        );

        animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);
        btnRefresh = v.findViewById(R.id.btn_refresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CekSaldo();
            }
        });

    }

    public void autoRefresh(){
        count++;
        CekSaldo();
        refresh(60000);
    }

    private void refresh(int milliseconds){
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                autoRefresh();
            }
        };
        handler.postDelayed(runnable, milliseconds);

    }

    /**
     * Adding menu
     */
    private void prepareMenuCard() {
        int[] img = new int[]{
                R.drawable.favorit,
                R.drawable.pulsa,
                R.drawable.listrik,
                R.drawable.telkom,
                R.drawable.pdam,
                R.drawable.tiket,
                R.drawable.tv,
                R.drawable.internet,
                R.drawable.bpjs,
                R.drawable.asuransi,
                R.drawable.emoney};

        MenuCardModel a = new MenuCardModel("Favorit", img[0]);
        menuList.add(a);
        a = new MenuCardModel("Pulsa", img[1]);
        menuList.add(a);
        a = new MenuCardModel("Listrik PLN", img[2]);
        menuList.add(a);
        a = new MenuCardModel("Telepon", img[3]);
        menuList.add(a);
        a = new MenuCardModel("PDAM", img[4]);
        menuList.add(a);
        a = new MenuCardModel("Tiket", img[5]);
        menuList.add(a);
        a = new MenuCardModel("TV Kabel", img[6]);
        menuList.add(a);
        a = new MenuCardModel("Internet", img[7]);
        menuList.add(a);
        a = new MenuCardModel("BPJS", img[8]);
        menuList.add(a);
        a = new MenuCardModel("Pajak", img[9]);
        menuList.add(a);
        a = new MenuCardModel("E-money", img[10]);
        menuList.add(a);
        menuAdapter.notifyDataSetChanged();
    }

    private void CekSaldo(){
        btnRefresh.startAnimation(animation);

        String version = AppController.getInstance().getVersionName();

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("saldo", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try{
            Obj_CekSaldo param = new Obj_CekSaldo(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI);
            Call<Obj_CekSaldo> call = NetworkManager.getNetworkService(getContext()).cekSaldo(param);
            call.enqueue(new Callback<Obj_CekSaldo>() {
                @Override
                public void onResponse(Call<Obj_CekSaldo> call, Response<Obj_CekSaldo> response) {
                    int code = response.code();

                    if (code == 200){
                        cekSaldo = response.body();

                        if (cekSaldo.rc.equals("00")){
                            AppConfig.BALANCE = cekSaldo.data.balance;
                            String sBal = "Rp."+ AppConfig.BALANCE;
                            txtSaldo.setText(sBal);
                            txtWaktu.setText(cekSaldo.data.date);
                            btnRefresh.clearAnimation();
                        }else if (cekSaldo.rc.equals("73")){
                            txtSaldo.setText("Rp.0");
                            txtWaktu.setText("");
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                        }else if (cekSaldo.rc.equals("78")){
                            updateDeviceDialog(cekSaldo.message);
                        }else if (cekSaldo.rc.equals("97")){
                            Intent i = new Intent(getActivity(),Login.class);
                            startActivity(i);
                            getActivity().finish();
                        }else{
                            txtSaldo.setText("Rp.0");
                            txtWaktu.setText("");
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_CekSaldo> call, Throwable t) {
                    btnRefresh.clearAnimation();
                    txtSaldo.setText("Rp.0");
                    txtWaktu.setText("");
                }
            });
        }catch (Exception e){
            btnRefresh.clearAnimation();
            Log.v("Exception", e.toString());
        }
    }

    private void SummaryTrx(){
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("summary", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_SummaryTrx param = new Obj_SummaryTrx(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI);
            Log.d("SumTrx", AppConfig.USERID+":"+AppConfig.REQID+":"+AppConfig.KEY);
            Call<Obj_SummaryTrx> call = NetworkManager.getNetworkService(getContext()).getSummaryTrx(param);
            call.enqueue(new Callback<Obj_SummaryTrx>() {
                @Override
                public void onResponse(Call<Obj_SummaryTrx> call, Response<Obj_SummaryTrx> response) {
                    int code = response.code();

                    if (code == 200){
                        sumTrx = response.body();

                        if (sumTrx.rc.equals("00")){
                            if (sumTrx.data.nominal == null){
                                txtSumTrans.setText("Rp. 0");
                            }else{
                                Double dSumTrx = Double.parseDouble(sumTrx.data.nominal);
                                String sSumTrx = AppController.getInstance().toCurrencyIDR(dSumTrx);
                                txtSumTrans.setText(sSumTrx);
                            }

                        }else if (sumTrx.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(Login.this);
                        }else if (sumTrx.rc.equals("78")){
                            updateDeviceDialog(sumTrx.message);
                        }else{
                            txtSumTrans.setText("Rp.0");
                        }
                    }else{
                        txtSumTrans.setText("Rp.0");
                    }
                }

                @Override
                public void onFailure(Call<Obj_SummaryTrx> call, Throwable t) {
                    txtSaldo.setText("Rp.0");
                }
            });
        }catch (Exception e){
            Log.v("Exception", e.toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()){
            CekSaldo();
            SummaryTrx();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() ) {
            CekSaldo();
            SummaryTrx();
        }
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

}
