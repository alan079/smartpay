package com.mayora.mypaydrc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Payment_PLNPRE;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 15/11/2017.
 */

public class ListrikTokenDetailNew extends AppCompatActivity {
    private Toolbar mToolbar;
    private EditText editPin;
    private Button btnSubmit;
    private TextView txtNopel, txtProdName, txtNominal, txtHarga,  txtNama, txtTarif;
    private String nopel="", prod_id="", prod_name="", nominal="", harga="", nama="", tarifdaya="", reffid="";
    private String IDPEL = "", NOMETER = "", NAMAPELANGGAN = "", TOTTAGIHAN = "", REFFID = "", ADMIN="", TOTBAYAR="", RECEIPT="", MSG="", SN="", TARIFDAYA="", JUMLAHKWH="", PPN="", PPJ="", TOKEN=""; //BALANCE="",
    ProgressDialog progress;
    Obj_Payment_PLNPRE objPayment;
    private LinearLayout linearLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listrik_token_detail);

        InitControl();
        GetExtrasData();
    }

    void InitControl(){
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NestedScrollView scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        linearLayout = findViewById(R.id.layout_pulsa_detail);

        txtNopel = findViewById(R.id.txt_nopel);
        txtNama = findViewById(R.id.txt_nama);
        txtTarif = findViewById(R.id.txt_tarifdaya);
        txtProdName = findViewById(R.id.txt_prod);
        txtNominal = findViewById(R.id.txt_nominal);
        txtHarga = findViewById(R.id.txt_harga);

        editPin = findViewById(R.id.edit_pin);
        btnSubmit = findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentProccess();
            }
        });
    }

    void GetExtrasData(){
        Intent intent = getIntent();
        nopel = intent.getStringExtra("NOPEL");
        prod_id = intent.getStringExtra("PROD_ID");
        prod_name = intent.getStringExtra("PROD_NAME");
        nominal = intent.getStringExtra("NOMINAL");
        harga = intent.getStringExtra("HARGA");
        nama = intent.getStringExtra("NAMA");
        tarifdaya = intent.getStringExtra("TARIFDAYA");
        reffid = intent.getStringExtra("REFFID");

        txtNopel.setText(nopel);
        txtNama.setText(nama);
        txtTarif.setText(tarifdaya);
        txtProdName.setText(prod_name);
        txtNominal.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(nominal)));
        txtHarga.setText(AppController.getInstance().toCurrencyIDR(Double.parseDouble(harga)));
    }

    private void PaymentProccess() {
        progress = ProgressDialog.show(this, "Info", "Checking data", true);

        String sPrice = harga.replace(".00","");

        String version = AppController.getInstance().getVersionName();
        String Pin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(Pin);
        Log.e("PIN", sPin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("payment", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            //device,key,product,reference,subscriber,user,version,inquiry_num
            Obj_Payment_PLNPRE param = new Obj_Payment_PLNPRE(AppConfig.IMEI, AppConfig.KEY, prod_id, AppConfig.REQID, nopel, AppConfig.USERID, harga, version, reffid);
            Call<Obj_Payment_PLNPRE> call = NetworkManager.getNetworkService(this).payment(param);
            call.enqueue(new Callback<Obj_Payment_PLNPRE>() {
                @Override
                public void onResponse(Call<Obj_Payment_PLNPRE> call, Response<Obj_Payment_PLNPRE> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objPayment = response.body();
                        if (!(objPayment.rc == null)) {
                            if (objPayment.rc.equals("00")) {

                                try {

                                    MSG = objPayment.message;
                                    NOMETER = objPayment.display.NO_PEL.trim();
                                    IDPEL = objPayment.display.IDPEL.trim();
                                    NAMAPELANGGAN = objPayment.display.NAMA;
                                    TARIFDAYA = objPayment.display.TARIF.trim();
                                    JUMLAHKWH = objPayment.display.KWH.trim();
                                    TOTTAGIHAN = objPayment.display.RP_STROOM.trim();
                                    PPN = objPayment.display.PPN.trim();
                                    PPJ = objPayment.display.PPJ.trim();
                                    ADMIN = objPayment.display.BIAYA_ADMIN.trim();
                                    TOTBAYAR = objPayment.display.RP_BAYAR.trim();
                                    REFFID = objPayment.display.NOREF.trim();
                                    SN = objPayment.display.SN;
                                    TOKEN = objPayment.display.TOKEN.trim();
                                    //BALANCE = objPayment.balance;
                                    MSG = objPayment.message;
                                    RECEIPT = objPayment.receipt;

                                    Intent mIntent = new Intent(getApplicationContext(), ListrikTokenStatusNew.class);
                                    mIntent.putExtra("MSG", MSG);
                                    mIntent.putExtra("IDPEL", IDPEL);
                                    mIntent.putExtra("NOMETER", NOMETER);
                                    mIntent.putExtra("NAMA", NAMAPELANGGAN);
                                    mIntent.putExtra("TARIFDAYA", TARIFDAYA);
                                    mIntent.putExtra("JUMLAHKWH", JUMLAHKWH);
                                    mIntent.putExtra("TOTTAGIHAN", TOTTAGIHAN);
                                    mIntent.putExtra("PPN", PPN);
                                    mIntent.putExtra("PPJ", PPJ);
                                    mIntent.putExtra("ADMIN", ADMIN);
                                    mIntent.putExtra("TOTBAYAR", TOTBAYAR);
                                    mIntent.putExtra("NOREF", REFFID);
                                    mIntent.putExtra("SN", SN);
                                    mIntent.putExtra("TOKEN", TOKEN);
                                    //mIntent.putExtra("BALANCE", BALANCE);
                                    mIntent.putExtra("PRODUCTID", prod_id);
                                    mIntent.putExtra("RECEIPT", RECEIPT);
                                    startActivity(mIntent);
                                    finish();

                                } catch (Exception e) {
                                    AppController.getInstance().errorDialog(getApplicationContext(), "Pembelian Token Listrik gagal!");
                                    Log.e("ErrorPLNToken", e.toString());
                                }

                            } else {
                                AppController.getInstance().errorDialog(getApplicationContext(), MSG);
                            }
                        }else{
                            AppController.getInstance().errorDialog(getApplicationContext(), "Pembelian Token Listrik gagal!");
                            Log.e("ErrorPLNToken", "null");
                        }
                    } else {
                        AppController.getInstance().errorDialog(getApplicationContext(), "Pembelian Token Listrik gagal!");
                    }
                }

                @Override
                public void onFailure(Call<Obj_Payment_PLNPRE> call, Throwable t) {
                    progress.dismiss();
                    AppController.getInstance().errorDialog(getApplicationContext(), "Pembelian Token Listrik gagal!");
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            AppController.getInstance().errorDialog(getApplicationContext(), "Pembelian Token Listrik gagal!");
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return true;
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);
        snackbar.show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

}