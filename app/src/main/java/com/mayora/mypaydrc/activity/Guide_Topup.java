package com.mayora.mypaydrc.activity;

import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.ui.CustomSliderView;

import java.util.HashMap;

public class Guide_Topup extends AppCompatActivity{

    private Button btnSkip;

    private SliderLayout mDemoSlider;
    private PagerIndicator mIndicator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guide_help);

        btnSkip = (Button) findViewById(R.id.btnSkip);

		/* Guide Slide */
        mDemoSlider = (SliderLayout) findViewById(R.id.slider);
        mIndicator = (PagerIndicator) findViewById(R.id.custom_indicator);
        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();

        file_maps.put("a", R.drawable.guide_topup1);
        file_maps.put("b", R.drawable.guide_topup2);
        file_maps.put("c", R.drawable.guide_topup3);
        file_maps.put("d", R.drawable.guide_topup4);
        file_maps.put("e", R.drawable.guide_topup5);
        file_maps.put("f", R.drawable.guide_topup6);
        file_maps.put("g", R.drawable.guide_topup7);

        CustomSliderView textSliderView1 = new CustomSliderView(getApplicationContext());
        CustomSliderView textSliderView2 = new CustomSliderView(getApplicationContext());
        CustomSliderView textSliderView3 = new CustomSliderView(getApplicationContext());
        CustomSliderView textSliderView4 = new CustomSliderView(getApplicationContext());
        CustomSliderView textSliderView5 = new CustomSliderView(getApplicationContext());
        CustomSliderView textSliderView6 = new CustomSliderView(getApplicationContext());
        CustomSliderView textSliderView7 = new CustomSliderView(getApplicationContext());
        // initialize a SliderLayout
        mDemoSlider.addSlider(textSliderView1.image(file_maps.get("a")).setScaleType(BaseSliderView.ScaleType.Fit));
        mDemoSlider.addSlider(textSliderView2.image(file_maps.get("b")).setScaleType(BaseSliderView.ScaleType.Fit));
        mDemoSlider.addSlider(textSliderView3.image(file_maps.get("c")).setScaleType(BaseSliderView.ScaleType.Fit));
        mDemoSlider.addSlider(textSliderView4.image(file_maps.get("d")).setScaleType(BaseSliderView.ScaleType.Fit));
        mDemoSlider.addSlider(textSliderView5.image(file_maps.get("e")).setScaleType(BaseSliderView.ScaleType.Fit));
        mDemoSlider.addSlider(textSliderView6.image(file_maps.get("f")).setScaleType(BaseSliderView.ScaleType.Fit));
        mDemoSlider.addSlider(textSliderView7.image(file_maps.get("g")).setScaleType(BaseSliderView.ScaleType.Fit));

        mDemoSlider.setBackgroundColor(Color.TRANSPARENT);
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setCustomIndicator(mIndicator);

        btnSkip.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }

        });
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

}