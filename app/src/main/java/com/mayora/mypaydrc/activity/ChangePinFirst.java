package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_ChangePin;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePinFirst extends AppCompatActivity {

    private Button btnSimpan;
    private Toolbar mToolbar;
    private EditText editPinOld, editPinNew, editPinNewConfirm;
    private String sPinNew="", sPinOld="", sPinNewSHA="", encryptPin="";

    ProgressDialog progress;

    private Obj_ChangePin objChangePin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_pin);

        InitControl();
    }

    private void InitControl(){

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        editPinOld = (EditText) findViewById(R.id.editPinLama);
        editPinNew = (EditText) findViewById(R.id.editPinBaru);
        editPinNewConfirm = (EditText) findViewById(R.id.editPinKonfirmasi);

        btnSimpan = (Button) findViewById(R.id.btnSubmit);
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editPinOld.getText().length() == 0){
                    editPinOld.setError(getString(R.string.error_field_required));
                    editPinOld.requestFocus();
                }else if(editPinOld.getText().length() < 6){
                    editPinOld.setError(getString(R.string.error_invalid_pin));
                    editPinOld.requestFocus();
                }else if(editPinNew.getText().length() == 0){
                    editPinNew.setError(getString(R.string.error_field_required));
                    editPinNew.requestFocus();
                }else if(editPinNew.getText().length() < 6){
                    editPinNew.setError(getString(R.string.error_invalid_pin));
                    editPinNew.requestFocus();
                }else if((editPinNew.getText().toString()).equals(editPinOld.getText().toString())){
                    editPinNew.setError(getString(R.string.error_invalid_pin_new));
                    editPinNew.requestFocus();
                }else if(! (editPinNewConfirm.getText().toString()).equals(editPinNew.getText().toString())){
                    editPinNewConfirm.setError(getString(R.string.error_invalid_confirm));
                    editPinNewConfirm.requestFocus();
                } else {
                    ValidasiChangePin();
                }
            }
        });
    };

    private void ValidasiChangePin(){
        progress = ProgressDialog.show(this, "Info", "Validasi data", true);

        sPinOld = editPinOld.getText().toString().trim();
        sPinNew = editPinNew.getText().toString().trim();
        encryptPin = encrypt(sPinNew);
        Log.e("encrypt", encryptPin);

        try {
            sPinNewSHA = AppController.getInstance().getHashPass(AppConfig.USERID, sPinNew);
        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("password", AppConfig.USERID, sPinOld);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_ChangePin param = new Obj_ChangePin(AppConfig.USERID, AppConfig.REQID, sPinNewSHA, AppConfig.KEY, version, AppConfig.IMEI);
            Call<Obj_ChangePin> call = NetworkManager.getNetworkService(this).changePass(param);
            call.enqueue(new Callback<Obj_ChangePin>() {
                @Override
                public void onResponse(Call<Obj_ChangePin> call, Response<Obj_ChangePin> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200){
                        objChangePin = response.body();

                        if(objChangePin.rc.equals("00")){
                            AppController.getInstance().getSessionManager().putStringData("pin", encryptPin);

                            editPinOld.setText("");
                            editPinNew.setText("");
                            editPinNewConfirm.setText("");
                            editPinOld.requestFocus();

                            logout(objChangePin.message);
                        }else if (objChangePin.rc.equals("73")){
                            errorDialog(objChangePin.message);
                            //AppController.getInstance().showForceUpdateDialog(getApplicationContext());
                        }
                        else if (objChangePin.rc.equals("78")){
                            updateDeviceDialog(objChangePin.message);
                        }else{
                            errorDialog(objChangePin.message);
                        }

                    }else{
                        errorDialog("Ubah Password Gagal!");
                    }
                }

                @Override
                public void onFailure(Call<Obj_ChangePin> call, Throwable t) {
                    progress.dismiss();
                    errorDialog("Ubah Password Gagal!");
                }
            });
        }catch (Exception e){
            progress.dismiss();
            errorDialog("Ubah Password Gagal!");
        }
    }

    private void errorDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(getApplicationContext(), R.style.AlertDialog));
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }).show();

    }

    private void logout(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(getApplicationContext(), R.style.AlertDialog));
        dialog.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        AppController.getInstance().getSessionManager().setUserAccount(null);
                        AppController.getInstance().getSessionManager().putStringData("pin", null);

                        Intent login = new Intent(getApplicationContext(), Login.class);
                        startActivity(login);
                        finish();
                    }
                }).show();
    }

    @Override
    public void onBackPressed() {
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialog));
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                        Intent i = new Intent(getApplicationContext(),Login.class);
                        startActivity(i);
                        finish();

                    }
                }).show();
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!.,])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public static String encrypt(String input) {
        return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
    }
}
