package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.ItemTopupHistoryAdapter;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_TopupHistory;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 24/07/2017.
 */

public class TopupHistoryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener  {

    private RecyclerView recyclerView;

    private ProgressDialog progressDialog;
    private Obj_TopupHistory obj_topupHistory, obj_topupHistoryFull;
    private RecyclerView.Adapter mAdapter;

    private LinearLayoutManager mLayoutManager;
    SwipeRefreshLayout swipeRefreshLayout;

    int page = 1;
    int total_page = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_topup_history, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);


    }

    private void InitControl(View v) {

        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0) //check for scroll down
                {

                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        if (page <= total_page) {
                            FillGridMore();
                            page = page + 1;
                        }
                    }
                }
            }
        });

    }

    void FillGrid(){
        page = 1;

        progressDialog = ProgressDialog.show(getActivity(),"", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("historytopup", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_TopupHistory param = new Obj_TopupHistory(AppConfig.USERID, AppConfig.REQID, page, AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_TopupHistory> call = NetworkManager.getNetworkService(getActivity()).getTopupHistory(param);
            call.enqueue(new Callback<Obj_TopupHistory>() {
                @Override
                public void onResponse(Call<Obj_TopupHistory> call, Response<Obj_TopupHistory> response) {
                    int code = response.code();
                    progressDialog.dismiss();

                    if (code == 200){
                        obj_topupHistory = response.body();
                        if (obj_topupHistory.rc.equals("00")){
                            if (obj_topupHistory.data == null || obj_topupHistory.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "Riwayat Topup tidak ditemukan!");
                            }else{
                                total_page = Integer.valueOf(obj_topupHistory.total_paging);
                                obj_topupHistoryFull = obj_topupHistory;
                                FillAdapter();

                                page=page+1;
                            }
                        }else if (obj_topupHistory.rc.equals("73")){
                            recyclerView.setAdapter(null);
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                            AppController.getInstance().errorDialog(getContext(), obj_topupHistory.message);

                        }else if (obj_topupHistory.rc.equals("78")){
                            recyclerView.setAdapter(null);
                            updateDeviceDialog(obj_topupHistory.message);

                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_topupHistory.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_TopupHistory> call, Throwable t) {
                    progressDialog.dismiss();
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
            recyclerView.setAdapter(null);
        }
    }

    void FillGridOnRefresh(){
        page = 1;

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("historytopup", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_TopupHistory param = new Obj_TopupHistory(AppConfig.USERID, AppConfig.REQID, page, AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_TopupHistory> call = NetworkManager.getNetworkService(getActivity()).getTopupHistory(param);
            call.enqueue(new Callback<Obj_TopupHistory>() {
                @Override
                public void onResponse(Call<Obj_TopupHistory> call, Response<Obj_TopupHistory> response) {
                    int code = response.code();
                    swipeRefreshLayout.setRefreshing(false);

                    if (code == 200){
                        obj_topupHistory = response.body();
                        if (obj_topupHistory.rc.equals("00")){
                            if (obj_topupHistory.data == null || obj_topupHistory.data.size() == 0){
                                recyclerView.setAdapter(null);
                                AppController.getInstance().errorDialog(getContext(), "Riwayat Topup tidak ditemukan!");
                            }else{
                                total_page = Integer.valueOf(obj_topupHistory.total_paging);
                                obj_topupHistoryFull = obj_topupHistory;
                                FillAdapter();

                                page=page+1;
                            }
                        }else if (obj_topupHistory.rc.equals("73")){
                            recyclerView.setAdapter(null);
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                            AppController.getInstance().errorDialog(getContext(), obj_topupHistory.message);

                        }else if (obj_topupHistory.rc.equals("78")){
                            recyclerView.setAdapter(null);
                            updateDeviceDialog(obj_topupHistory.message);
                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_topupHistory.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_TopupHistory> call, Throwable t) {
                    swipeRefreshLayout.setRefreshing(false);
                    recyclerView.setAdapter(null);
                }
            });
        }catch (Exception e){
            swipeRefreshLayout.setRefreshing(false);
            recyclerView.setAdapter(null);
        }
    }

    void FillGridMore(){
        final ProgressDialog progressDialogMore = ProgressDialog.show(getActivity(),null, null, true);
        progressDialogMore.getWindow().setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
        progressDialogMore.setContentView( R.layout.progress_item );
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("historytopup", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_TopupHistory param = new Obj_TopupHistory(AppConfig.USERID, AppConfig.REQID, page, AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_TopupHistory> call = NetworkManager.getNetworkService(getActivity()).getTopupHistory(param);
            call.enqueue(new Callback<Obj_TopupHistory>() {
                @Override
                public void onResponse(Call<Obj_TopupHistory> call, Response<Obj_TopupHistory> response) {
                    int code = response.code();
                    if (code == 200){
                        progressDialogMore.dismiss();
                        obj_topupHistory = response.body();
                        if (obj_topupHistory.rc.equals("00")){
                            for(Obj_TopupHistory.Datum dat : obj_topupHistory.data){
                                obj_topupHistoryFull.data.add(dat);
                            }

                            mAdapter.notifyDataSetChanged();
                        }else if (obj_topupHistory.rc.equals("73")){
                            recyclerView.setAdapter(null);
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                            AppController.getInstance().errorDialog(getContext(), obj_topupHistory.message);

                        }else if (obj_topupHistory.rc.equals("78")){
                            recyclerView.setAdapter(null);
                            updateDeviceDialog(obj_topupHistory.message);
                        }else{
                            recyclerView.setAdapter(null);
                            AppController.getInstance().errorDialog(getContext(), obj_topupHistory.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_TopupHistory> call, Throwable t) {
                    recyclerView.setAdapter(null);
                    progressDialogMore.dismiss();
                }
            });
        }catch (Exception e){
            recyclerView.setAdapter(null);
            progressDialogMore.dismiss();
        }
    }

    void FillAdapter(){
        mAdapter = new ItemTopupHistoryAdapter(getActivity(), obj_topupHistoryFull, new ItemTopupHistoryAdapter.OnDownloadClicked() {
            @Override
            public void OnDownloadClicked(String DATETIME, String NOM, String EXP, String STATUS) {

            }
        });
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onRefresh() {
        FillGridOnRefresh();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() ) {
            FillGrid();
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() ) {
            FillGrid();
        }
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }

}
