package com.mayora.mypaydrc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Payment_PLNPOST;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListrikPLNDetail extends AppCompatActivity {

    private TextView txtIDPel, txtNama, txtBulan, txtTunggakan, txtTotTagihan, txtAdmin, txtTotBayar;
    private TextView title_date, title_tunggakan;
    private View separator_tunggakan;
    private Button btnBayar;
    private LinearLayout linearLayout;

    ProgressDialog progress;
    private Obj_Payment_PLNPOST objPayment;
    private String PRODUCTID="", MSG="",IDPEL = "", NAMA = "", BULAN ="", TUNGGAKAN="",  TOTTAGIHAN = "", REFFID = "",
            NOREF="",ADMIN="", TOTBAYAR="", SN="", RECEIPT="", TARIF="", TIPE_TRANS="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listrik_detail);

        InitControl();
        GetExtrasData();

        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentProccess();
            }
        });

    }

    void InitControl(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        NestedScrollView scroll = findViewById(R.id.scroll);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        linearLayout = findViewById(R.id.layout_detail);

        title_date = findViewById(R.id.txt_title_date);
        title_tunggakan = findViewById(R.id.txt_title_denda);
        txtIDPel = findViewById(R.id.txt_idpel);
        txtNama = findViewById(R.id.txt_nama);
        txtBulan = findViewById(R.id.txt_date);
        txtTotTagihan = findViewById(R.id.txt_totaltagihan);
        txtAdmin = findViewById(R.id.txt_admin);
        txtTunggakan = findViewById(R.id.txt_denda);
        txtTotBayar = findViewById(R.id.txt_totalbayar);
        separator_tunggakan = findViewById(R.id.separator_tunggakan);

        btnBayar = findViewById(R.id.btnBayar);

    }

    void GetExtrasData(){

        Intent intent = getIntent();
        IDPEL = intent.getStringExtra("IDPEL");
        NAMA = intent.getStringExtra("NAMA");
        BULAN = intent.getStringExtra("BULAN");
        TOTTAGIHAN = intent.getStringExtra("TOTTAGIHAN");
        ADMIN = intent.getStringExtra("ADMIN");
        TUNGGAKAN = intent.getStringExtra("TUNGGAKAN");
        TOTBAYAR = intent.getStringExtra("TOTBAYAR");
        REFFID = intent.getStringExtra("REFFID");
        PRODUCTID = intent.getStringExtra("PRODUCTID");
        TIPE_TRANS = intent.getStringExtra("TIPE");

        txtIDPel.setText(IDPEL);
        txtNama.setText(NAMA);
        txtTotTagihan.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTTAGIHAN)));
        txtAdmin.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(ADMIN)));
        txtTotBayar.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(TOTBAYAR)));
        if (TIPE_TRANS != null){
            title_date.setText("Tipe Transaksi");
            txtBulan.setText(TIPE_TRANS);
            title_tunggakan.setVisibility(View.GONE);
            txtTunggakan.setVisibility(View.GONE);
            separator_tunggakan.setVisibility(View.GONE);
        } else {
            title_date.setText("Bulan Tagihan");
            txtBulan.setText(BULAN);
            title_tunggakan.setText("Tunggakan (Bulan)");
            txtTunggakan.setText(TUNGGAKAN);

        }

    }

    private void PaymentProccess() {
        progress = ProgressDialog.show(this, "Info", "Checking data", true);

        String version = AppController.getInstance().getVersionName();
        String decryptPin = AppController.getInstance().getSessionManager().getStringData("pin");
        String sPin = decrypt(decryptPin);
        Log.e("PIN", sPin);

        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePut("payment", AppConfig.USERID, sPin);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            Obj_Payment_PLNPOST param = new Obj_Payment_PLNPOST(AppConfig.IMEI, AppConfig.KEY, AppConfig.REQID, AppConfig.USERID, version, PRODUCTID, IDPEL, REFFID);
            Call<Obj_Payment_PLNPOST> call = NetworkManager.getNetworkService(this).paymentPLNPOST(param);
            call.enqueue(new Callback<Obj_Payment_PLNPOST>() {
                @Override
                public void onResponse(Call<Obj_Payment_PLNPOST> call, Response<Obj_Payment_PLNPOST> response) {
                    int code = response.code();
                    progress.dismiss();

                    if (code == 200) {
                        objPayment = response.body();
                        if (objPayment != null) {
                            if (objPayment.rc.equals("00")) {

                                try {
                                    MSG = objPayment.message;
                                    IDPEL = objPayment.subscriber;
                                    NAMA = objPayment.display.NAMA;
                                    BULAN = objPayment.display.TAGIHAN_BULAN;
                                    NOREF = objPayment.reference;
                                    SN = objPayment.display.SN;
                                    TOTTAGIHAN = objPayment.display.TOTAL_TAGIHAN;
                                    ADMIN = objPayment.display.BIAYA_ADMIN;
                                    TOTBAYAR = objPayment.display.HARGA;
                                    RECEIPT = objPayment.receipt;
                                    TARIF = objPayment.display.TARIF;

                                    Intent mIntent = new Intent(getApplicationContext(), ListrikPLNStatus.class);
                                    mIntent.putExtra("MSG", MSG);
                                    mIntent.putExtra("IDPEL", IDPEL);
                                    mIntent.putExtra("NAMA", NAMA);
                                    mIntent.putExtra("BULAN", BULAN);
                                    mIntent.putExtra("TOTTAGIHAN", TOTTAGIHAN);
                                    mIntent.putExtra("ADMIN", ADMIN);
                                    mIntent.putExtra("TUNGGAKAN", TUNGGAKAN);
                                    mIntent.putExtra("TOTBAYAR", TOTBAYAR);
                                    mIntent.putExtra("PRODUCTID", PRODUCTID);
                                    mIntent.putExtra("NOREF", NOREF);
                                    mIntent.putExtra("SN", SN);
                                    mIntent.putExtra("RECEIPT", RECEIPT);
                                    mIntent.putExtra("TARIF", TARIF);
                                    mIntent.putExtra("TIPE", TIPE_TRANS);
                                    startActivity(mIntent);
                                    finish();

                                } catch (Exception e) {
                                    SnackbarMessage(linearLayout, "Pembayaran Tagihan Listrik gagal!", Snackbar.LENGTH_INDEFINITE);
                                    Log.e("ErrorPLNPayment", e.toString());
                                }

                            } else {
                                AppController.getInstance().errorDialog(ListrikPLNDetail.this, objPayment.message);
                            }
                        }else{
                            SnackbarMessage(linearLayout, "Pembayaran Tagihan Listrik gagal!", Snackbar.LENGTH_INDEFINITE);
                            Log.e("ErrorPDAMPayment", "null");
                        }
                    } else {
                        SnackbarMessage(linearLayout, "Pembayaran Tagihan Listrik gagal!", Snackbar.LENGTH_INDEFINITE);
                    }
                }

                @Override
                public void onFailure(Call<Obj_Payment_PLNPOST> call, Throwable t) {
                    progress.dismiss();
                    SnackbarMessage(linearLayout, "Pembayaran Tagihan Listrik gagal!", Snackbar.LENGTH_INDEFINITE);
                }
            });
        } catch (Exception e) {
            progress.dismiss();
            SnackbarMessage(linearLayout, "Pembayaran Tagihan Listrik gagal!", Snackbar.LENGTH_INDEFINITE);
        }
    }

    @Override
    public void onBackPressed() {
        // do something on back.
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //optionmenu
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                break;
        }
        return true;
    }

    public void SnackbarMessage(View view, String msg, int duration){
        final Snackbar snackbar = Snackbar.make(view, msg, duration);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.CYAN);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);
        snackbar.show();
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}