package com.mayora.mypaydrc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.adapter.MenuAccountListAdapter;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.MenuListModel;
import com.mayora.mypaydrc.model.Obj_Profile;
import com.mayora.mypaydrc.net.NetworkManager;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eroy on 27/05/2017.
 */

public class ProfilFragment extends Fragment {

    private ListView listView;
    private MenuAccountListAdapter listAdapter;
    private ArrayList<MenuListModel> models;
    private TextView txtNama, txtHp, txtEmail, txtAddress, txtSaldo, txtAccount, txtTitleSaldo, txtTitleAccount;

    private ProgressDialog progressDialog;
    private Obj_Profile objProfile;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_profil, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitControl(view);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() ) {
            getProfile();
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() ) {
            getProfile();
        }
    }

    private void InitControl(View v) {
        float scale = getResources().getConfiguration().fontScale;
        // This scale tells you what the font is scaled to.
        // So if you want size 16, then set the size as 16/scale
        float size12 = (float) 12.0;
        float size14 = (float) 14.0;

        txtNama = v.findViewById(R.id.txtNama);
        txtNama.setTextSize(size14/scale);
        txtHp = v.findViewById(R.id.txtHp);
        txtHp.setTextSize(size12/scale);
        txtEmail = v.findViewById(R.id.txtEmail);
        txtEmail.setTextSize(size12/scale);
        txtAddress = v.findViewById(R.id.txtAddr);
        txtAddress.setTextSize(size12/scale);
        txtSaldo = v.findViewById(R.id.saldo);
        txtSaldo.setTextSize(size12/scale);
        txtAccount = v.findViewById(R.id.account);
        txtAccount.setTextSize(size12/scale);
        txtTitleAccount = v.findViewById(R.id.title_account);
        txtTitleAccount.setTextSize(size14/scale);
        txtTitleSaldo = v.findViewById(R.id.title_saldo);
        txtTitleSaldo.setTextSize(size14/scale);

        /* Menu */
        listView = v.findViewById(R.id.ListMenu);
        models = new ArrayList<MenuListModel>();
        models.add(new MenuListModel(R.drawable.ic_password,"Ubah Password"));
        //models.add(new MenuListModel(R.drawable.ic_bell,"Notifikasi", AppConfig.NOTIF_COUNTER));
        models.add(new MenuListModel(R.drawable.ic_file,"Syarat & Ketentuan"));
        models.add(new MenuListModel(R.drawable.ic_question,"Bantuan"));
        models.add(new MenuListModel(R.drawable.ic_exit,"Logout"));

        listAdapter = new MenuAccountListAdapter(getActivity(), models);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new ListItemClickListener());

    }

    private class ListItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case 0:
                    Intent pin = new Intent(getActivity(), ChangePin.class);
                    startActivity(pin);
                    break;
                case 1:
                    Intent term = new Intent(getActivity(), Term.class);
                    startActivity(term);
                    break;
                case 2:
                    Intent help = new Intent(getActivity(), BantuanMenu.class);
                    startActivity(help);
                    break;
                case 3:
                    logout();
                    break;

                default:
                    break;
            }

        }
    }

    void getProfile(){

        progressDialog = ProgressDialog.show(getActivity(),"", "Mengambil Data", true);
        try {
            AppConfig.KEY = AppController.getInstance().getSignaturePost("profile", AppConfig.USERID);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String version = AppController.getInstance().getVersionName();

        try{
            Obj_Profile param = new Obj_Profile(AppConfig.USERID, AppConfig.REQID, AppConfig.KEY, version, AppConfig.IMEI);

            Call<Obj_Profile> call = NetworkManager.getNetworkService(getActivity()).getProfile(param);
            call.enqueue(new Callback<Obj_Profile>() {
                @Override
                public void onResponse(Call<Obj_Profile> call, Response<Obj_Profile> response) {
                    int code = response.code();
                    progressDialog.dismiss();
                    if (code == 200){
                        objProfile = response.body();
                        if (objProfile.rc.equals("00")){
                            if (objProfile.data != null){
                                txtNama.setText(objProfile.data.name);
                                txtHp.setText(objProfile.data.contact);
                                txtAddress.setText(objProfile.data.address);
                                txtEmail.setText(objProfile.data.email);
                                String sBal = "Rp. "+ AppConfig.BALANCE;
                                txtSaldo.setText(sBal);
                                txtAccount.setText(objProfile.data.account);
                            }else{
                                txtNama.setText(AppConfig.USERNAME);
                                txtHp.setText("");
                                txtAddress.setText("");
                                txtEmail.setText("");
                                String sBal = "Rp. "+ AppConfig.BALANCE;
                                txtSaldo.setText(sBal);
                                txtAccount.setText("");
                            }

                        }else if (objProfile.rc.equals("73")){
                            //AppController.getInstance().showForceUpdateDialog(getContext());
                        }else if (objProfile.rc.equals("78")){
                            updateDeviceDialog(objProfile.message);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Obj_Profile> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }catch (Exception e){
            progressDialog.dismiss();
        }
    }

    private void logout(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.AlertDialog));
        dialog.setMessage("Apakah anda akan logout?")
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        AppConfig.USERID="";
                        AppController.getInstance().getSessionManager().setUserAccount(null);
                        AppController.getInstance().getSessionManager().putStringData("pin", null);

                        Intent login = new Intent(getActivity(), Login.class);
                        startActivity(login);
                        getActivity().finish();
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                }).show();
    }

    private void updateDeviceDialog(String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.AlertDialog));
        dialog.setMessage(msg+" . Harap login kembali untuk verifikasi perangkat baru.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        Intent i = new Intent(getActivity(),Login.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                }).show();
    }


}
