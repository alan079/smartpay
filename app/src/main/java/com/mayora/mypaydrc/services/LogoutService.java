package com.mayora.mypaydrc.services;

import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;

/**
 * Created by eroy on 16/08/2017.
 */

public class LogoutService extends IntentService {

    public static final String RESULT = "result";
    public static final String NOTIFICATION = "com.mayora.mypay.Main";

    public LogoutService() {
        super("ServiceAutoLogOut");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(1,new Notification());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        AppConfig.USERID="";
        AppController.getInstance().getSessionManager().setUserAccount(null);
        AppController.getInstance().getSessionManager().putStringData("pin", null);
        if (AppController.getInstance().getSessionManager().getUserProfile() == null)
            publishResults(Activity.RESULT_OK);

    }

    private void publishResults(int result) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, result);
        sendBroadcast(intent);
    }
}
