package com.mayora.mypaydrc.adapter;

/**
 * Created by eroy on 2/5/2017.
 */

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Emoney_Nominal;

public class ItemNominalEmoneyAdapter extends RecyclerView.Adapter<ItemNominalEmoneyAdapter.MyViewHolder> {

    private Context mContext;
    Obj_Emoney_Nominal itemList;
    int row_index = -1;

    public ItemNominalEmoneyAdapter(Context mContext, Obj_Emoney_Nominal itemList, OnDownloadClicked listener) {
        this.mContext = mContext;
        this.itemList = itemList;
        this.listener = listener;
    }

    public interface OnDownloadClicked {
        public void OnDownloadClicked(String NOMINAL, String HARGA, String PROD_ID, String PROD_NAME);
    }

    private OnDownloadClicked listener;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener  {
        public TextView txtNominal, txtHarga;
        LinearLayout layout_item;

        public MyViewHolder(View view) {
            super(view);
            txtNominal= (TextView) view.findViewById(R.id.txt_nominal);
            txtHarga= (TextView) view.findViewById(R.id.txt_harga);
            layout_item = (LinearLayout) view.findViewById(R.id.layout_item_nominal);

        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_nominal, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Obj_Emoney_Nominal.Datum dat = itemList.data.get(position);
        String sNom = "";
        String sHarga = "";

        sNom = dat.nominal;
        holder.txtNominal.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(sNom)));
        sHarga = dat.price;
        holder.txtHarga.setText(AppController.getInstance().toCurrencyIDR(Double.parseDouble(sHarga)));

        holder.layout_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                notifyDataSetChanged();

                listener.OnDownloadClicked(dat.nominal, dat.price, dat.product_id, dat.name);
            }
        });

        if(row_index==position){
            holder.layout_item.setBackgroundColor(Color.parseColor("#f44336"));
            holder.txtNominal.setTextColor(Color.parseColor("#ffffff"));
            holder.txtHarga.setTextColor(Color.parseColor("#ffffff"));
        }else{
            holder.layout_item.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.txtNominal.setTextColor(Color.parseColor("#757575"));
            holder.txtHarga.setTextColor(Color.parseColor("#757575"));
        }

    }

    @Override
    public int getItemCount() {
        return itemList.data.size();
    }
}

