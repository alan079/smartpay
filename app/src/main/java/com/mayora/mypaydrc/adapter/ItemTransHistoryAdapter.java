package com.mayora.mypaydrc.adapter;

/**
 * Created by eroy on 2/5/2017.
 */

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_TransHistory;

public class ItemTransHistoryAdapter extends RecyclerView.Adapter<ItemTransHistoryAdapter.MyViewHolder> {

    private Context mContext;
    Obj_TransHistory itemList;

    public ItemTransHistoryAdapter(Context mContext, Obj_TransHistory itemList, OnDownloadClicked listener) {
        this.mContext = mContext;
        this.itemList = itemList;
        this.listener = listener;
    }

    public interface OnDownloadClicked {
        public void OnDownloadClicked(String TRXID, String DATETIME, String NOPEL, String PROD_NAME, String PRICE, String SN, String STATUS,
                                      String FLAG);
    }

    private OnDownloadClicked listener;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener  {
        public TextView txtDate, txtNopel, txtProd, txtPrice, txtRef, txtStatus, txtID;
        ImageView btnPrint, btnShare;
        View vFlag;

        public MyViewHolder(View v) {
            super(v);
            txtDate= v.findViewById(R.id.txt_date);
            txtNopel= v.findViewById(R.id.txt_nopel);
            txtID= v.findViewById(R.id.txt_id);
            txtProd= v.findViewById(R.id.txt_prod);
            txtPrice= v.findViewById(R.id.txt_price);
            txtRef= v.findViewById(R.id.txt_ref);
            txtStatus= v.findViewById(R.id.txt_status);
            btnPrint= v.findViewById(R.id.btn_print);
            btnShare= v.findViewById(R.id.btn_share);
            vFlag= v.findViewById(R.id.flag_status);

        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_trans_history, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Obj_TransHistory.Datum dat = itemList.data.get(position);

        String sRef = "";
        String sDate = dat.date;
        String sTime = dat.time;
        final String sDateTime = sDate+" "+sTime;
        String sPrice = dat.price;
        if (dat.reference == null || dat.reference.equals("")){
            sRef = "-";
        }else{
            sRef = "ref. " + dat.reference;
        }
        String sID = "#" + dat.id;

        holder.txtDate.setText(sDateTime);
        holder.txtID.setText(sID);
        holder.txtNopel.setText(dat.destination);
        holder.txtProd.setText(dat.product);
        holder.txtPrice.setText(AppController.getInstance().toCurrencyIDR(Double.parseDouble(sPrice)));
        holder.txtRef.setText(sRef);
        holder.txtStatus.setText(dat.status);

        if(dat.status.toLowerCase().equals("sukses")){
            holder.vFlag.setBackgroundColor(ContextCompat.getColor(mContext,R.color.md_green_500));
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext,R.color.md_green_500));
        }else if(dat.status.toLowerCase().equals("gagal")){
            holder.vFlag.setBackgroundColor(ContextCompat.getColor(mContext,R.color.md_red_500));
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext,R.color.md_red_500));
        }else{
            holder.vFlag.setBackgroundColor(ContextCompat.getColor(mContext,R.color.md_yellow_700));
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext,R.color.md_yellow_700));
        }

        holder.btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.image_click));

                listener.OnDownloadClicked(dat.id, sDateTime, dat.destination, dat.product, dat.price, dat.reference, dat.info, "PRINT");
            }
        });

        holder.btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.image_click));

                listener.OnDownloadClicked(dat.id, sDateTime, dat.destination, dat.product, dat.price, dat.reference, dat.info, "SHARE");
            }
        });


    }

    @Override
    public int getItemCount() {
        return itemList.data.size();
    }
}

