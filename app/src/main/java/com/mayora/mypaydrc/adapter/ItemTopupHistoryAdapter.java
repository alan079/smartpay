package com.mayora.mypaydrc.adapter;

/**
 * Created by eroy on 25/07/2017.
 */

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_TopupHistory;

public class ItemTopupHistoryAdapter extends RecyclerView.Adapter<ItemTopupHistoryAdapter.MyViewHolder> {

    private Context mContext;
    Obj_TopupHistory itemList;

    public ItemTopupHistoryAdapter(Context mContext, Obj_TopupHistory itemList, OnDownloadClicked listener) {
        this.mContext = mContext;
        this.itemList = itemList;
        this.listener = listener;
    }

    public interface OnDownloadClicked {
        public void OnDownloadClicked(String DATETIME, String NOM, String EXP, String STATUS);
    }

    private OnDownloadClicked listener;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener  {
        public TextView txtDate, txtNom, txtExp, txtStatus;
        View vFlag;

        public MyViewHolder(View v) {
            super(v);
            txtDate= (TextView) v.findViewById(R.id.txt_date);
            txtNom= (TextView) v.findViewById(R.id.txt_nominal);
            txtExp= (TextView) v.findViewById(R.id.txt_expire);
            txtStatus= (TextView) v.findViewById(R.id.txt_status);
            vFlag= v.findViewById(R.id.flag_status);

        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_topup_history, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Obj_TopupHistory.Datum dat = itemList.data.get(position);

        String sDate = dat.topup_date;
        String sTime = dat.topup_time;
        final String sDateTime = sDate+" "+sTime;

        holder.txtDate.setText(sDateTime);
        holder.txtNom.setText(AppController.getInstance().toCurrencyIDR(Double.parseDouble(dat.topup_nominal)));
        holder.txtExp.setText(dat.topup_expire);
        holder.txtStatus.setText(dat.topup_status);

        if(dat.topup_status.toLowerCase().equals("sukses")){
            holder.vFlag.setBackgroundColor(ContextCompat.getColor(mContext,R.color.md_green_500));
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext,R.color.md_green_500));
        }else if(dat.topup_status.toLowerCase().equals("pending") || dat.topup_status.toLowerCase().equals("aktif")){
            holder.vFlag.setBackgroundColor(ContextCompat.getColor(mContext,R.color.md_yellow_700));
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext,R.color.md_yellow_700));
        }else{
            holder.vFlag.setBackgroundColor(ContextCompat.getColor(mContext,R.color.md_red_500));
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext,R.color.md_red_500));
        }


    }

    @Override
    public int getItemCount() {
        return itemList.data.size();
    }
}

