package com.mayora.mypaydrc.adapter;

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.model.MenuListModel;

import java.util.ArrayList;

public class MenuAccountListAdapter extends ArrayAdapter<MenuListModel> {

    private final Context context;
    private final ArrayList<MenuListModel> modelsArrayList;

    public MenuAccountListAdapter(Context context, ArrayList<MenuListModel> modelsArrayList) {

        super(context, R.layout.menu_account_item, modelsArrayList);

        this.context = context;
        this.modelsArrayList = modelsArrayList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        float scale = context.getResources().getConfiguration().fontScale;
        // This scale tells you what the font is scaled to.
        // So if you want size 16, then set the size as 16/scale
        float sizeNeeded = (float) 13.0;

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater

        View rowView = null;
        if(!modelsArrayList.get(position).isGroupHeader()){
            rowView = inflater.inflate(R.layout.menu_account_item, parent, false);
            AppCompatImageView imgView = (AppCompatImageView) rowView.findViewById(R.id.item_icon);
            TextView titleView = (TextView) rowView.findViewById(R.id.item_title);
            ImageView angleView = (ImageView) rowView.findViewById(R.id.item_angle);
            ImageView counterView = (ImageView) rowView.findViewById(R.id.item_counter);

            if(modelsArrayList.get(position).isAngle()) {
                imgView.setImageResource(modelsArrayList.get(position).getIcon());
                titleView.setText(modelsArrayList.get(position).getTitle());
                titleView.setTextSize(sizeNeeded/scale);
                angleView.setVisibility(View.VISIBLE);
            }else{
                imgView.setImageResource(modelsArrayList.get(position).getIcon());
                titleView.setText(modelsArrayList.get(position).getTitle());
                titleView.setTextSize(sizeNeeded/scale);
            }

            String count = modelsArrayList.get(position).getCounter();
            if(count == "1"){
                imgView.setImageResource(modelsArrayList.get(position).getIcon());
                titleView.setText(modelsArrayList.get(position).getTitle());
                titleView.setTextSize(sizeNeeded/scale);
                counterView.setVisibility(View.VISIBLE);
            }else{
                imgView.setImageResource(modelsArrayList.get(position).getIcon());
                titleView.setText(modelsArrayList.get(position).getTitle());
                titleView.setTextSize(sizeNeeded/scale);
            }
        }
        else{
            rowView = inflater.inflate(R.layout.menu_header, parent, false);
            TextView titleView = (TextView) rowView.findViewById(R.id.header);
            titleView.setText(modelsArrayList.get(position).getTitle());
            titleView.setTextSize(sizeNeeded/scale);

        }

        // 5. retrn rowView
        return rowView;
    }
}

