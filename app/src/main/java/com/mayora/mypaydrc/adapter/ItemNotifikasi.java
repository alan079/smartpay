package com.mayora.mypaydrc.adapter;

/**
 * Created by eroy on 2/5/2017.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.model.Message;

import java.util.List;

public class ItemNotifikasi extends RecyclerView.Adapter<ItemNotifikasi.MyViewHolder> {

    private Context mContext;
    List<Message> itemList;

    public ItemNotifikasi(Context mContext, List<Message> itemList, OnDownloadClicked listener) {
        this.mContext = mContext;
        this.itemList = itemList;
        this.listener = listener;
    }

    public interface OnDownloadClicked {
        public void OnDownloadClicked(String DATE, String TITLE, String BODY);
    }

    private OnDownloadClicked listener;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener  {
        public TextView txtDate, txtTitle, txtBody;
        ImageView btnPrint, btnShare;
        View vFlag;

        public MyViewHolder(View v) {
            super(v);
            txtDate= (TextView) v.findViewById(R.id.txt_date);
            txtTitle= (TextView) v.findViewById(R.id.txt_title);
            txtBody= (TextView) v.findViewById(R.id.txt_body);

        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notif, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final List<Message> dat = itemList;

        String sDate = dat.get(position).getDate();
        String sTitle = dat.get(position).getTitle();
        String sBody = dat.get(position).getBody();

        holder.txtDate.setText(sDate);
        holder.txtTitle.setText(sTitle);
        holder.txtBody.setText(sBody);

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}

