package com.mayora.mypaydrc.adapter;

/**
 * Created by eroy on 3/7/2017.
 */

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Price;

import java.util.ArrayList;
import java.util.List;

public class ItemPriceAdapter extends RecyclerView.Adapter<ItemPriceAdapter.MyViewHolder> implements Filterable {

    private Context mContext;
    Obj_Price itemList = null;
    Obj_Price itemListFilter = null;

    public ItemPriceAdapter(Context mContext, Obj_Price itemList, OnDownloadClicked listener) {
        this.mContext = mContext;
        this.itemList = itemList;
        this.itemListFilter = itemList;
        this.listener = listener;
    }

    public interface OnDownloadClicked {
        public void OnDownloadClicked();
    }

    private OnDownloadClicked listener;


    public class  MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener  {
        public TextView txtProd, txtProdId, txtPrice, txtNominal;
        View vFlag;

        public MyViewHolder(View v) {
            super(v);
            txtProd= (TextView) v.findViewById(R.id.txt_prod);
            txtProdId= (TextView) v.findViewById(R.id.txt_prod_id);
            txtPrice= (TextView) v.findViewById(R.id.txt_price);
            txtNominal= (TextView) v.findViewById(R.id.txt_nominal);
            vFlag= v.findViewById(R.id.flag_status);

        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_price, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        float scale = mContext.getResources().getConfiguration().fontScale;
        float sizeNeeded = (float) 12.0;

        final Obj_Price.Datum dat = itemListFilter.data.get(position);

        String sPrice = dat.price;
        String sNom = dat.nominal;

        holder.txtProd.setText(dat.name);
        holder.txtProd.setTextSize(sizeNeeded/scale);
        holder.txtProdId.setText(dat.product_id);
        holder.txtProdId.setTextSize(sizeNeeded/scale);
        holder.txtPrice.setText(AppController.getInstance().toCurrencyIDR(Double.parseDouble(sPrice)));
        holder.txtPrice.setTextSize(sizeNeeded/scale);
        holder.txtNominal.setText(AppController.getInstance().toNumberFormatIDR(Double.parseDouble(sNom)));
        holder.txtNominal.setTextSize(sizeNeeded/scale);

        if(dat.name.toLowerCase().contains("telkomsel")){
            holder.vFlag.setBackgroundColor(ContextCompat.getColor(mContext,R.color.md_red_600));
            holder.txtProd.setTextColor(ContextCompat.getColor(mContext,R.color.md_red_600));
        }else if(dat.name.toLowerCase().contains("indosat") || dat.name.toLowerCase().contains("im3") || dat.name.toLowerCase().contains("mentari")){
            holder.vFlag.setBackgroundColor(ContextCompat.getColor(mContext,R.color.md_yellow_700));
            holder.txtProd.setTextColor(ContextCompat.getColor(mContext,R.color.md_yellow_700));
        }else if(dat.name.toLowerCase().contains("smartfren")){
            holder.vFlag.setBackgroundColor(ContextCompat.getColor(mContext,R.color.md_orange_600));
            holder.txtProd.setTextColor(ContextCompat.getColor(mContext,R.color.md_orange_600));
        }else if(dat.name.toLowerCase().contains("xl") || dat.name.toLowerCase().contains("axis")){
            holder.vFlag.setBackgroundColor(ContextCompat.getColor(mContext,R.color.md_blue_600));
            holder.txtProd.setTextColor(ContextCompat.getColor(mContext,R.color.md_blue_600));
        }else if(dat.name.toLowerCase().contains("tri")){
            holder.vFlag.setBackgroundColor(ContextCompat.getColor(mContext,R.color.md_grey_600));
            holder.txtProd.setTextColor(ContextCompat.getColor(mContext,R.color.md_grey_600));
        }else {
            holder.vFlag.setBackgroundColor(ContextCompat.getColor(mContext,R.color.black));
            holder.txtProd.setTextColor(ContextCompat.getColor(mContext,R.color.black));
        }

    }

    @Override
    public int getItemCount() {
        try {
            if ((itemListFilter.data != null)) {
                return itemListFilter.data.size();
            } else {
                return 0;
            }
        }catch (Exception e){
            Log.e("ErrorItemCount", e.toString());
        }

        return 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String filterString = constraint.toString().toLowerCase();
                FilterResults results = new FilterResults();

                //int count = itemList.data.size();
                final ArrayList<Obj_Price.Datum> nlist = new ArrayList<Obj_Price.Datum>();

                for (Obj_Price.Datum dataFilter : itemList.data){
                    if (dataFilter.name.toLowerCase().contains(filterString))
                    {
                        nlist.add(dataFilter);
                    }
                }
                results.values = nlist;
                results.count = nlist.size();

                return results;

            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                try {
                    itemListFilter.data = (List<Obj_Price.Datum>) results.values;
                    notifyDataSetChanged();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        };
    }

}

