package com.mayora.mypaydrc.adapter;

/**
 * Created by eroy on 3/7/2017.
 */

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_Promo;

public class ItemPromoAdapter extends RecyclerView.Adapter<ItemPromoAdapter.MyViewHolder> {

    private Context mContext;
    Obj_Promo itemList = null;
    Obj_Promo itemListFilter = null;

    public ItemPromoAdapter(Context mContext, Obj_Promo itemList, OnDownloadClicked listener) {
        this.mContext = mContext;
        this.itemList = itemList;
        this.itemListFilter = itemList;
        this.listener = listener;
    }

    public interface OnDownloadClicked {
        public void OnDownloadClicked();
    }

    private OnDownloadClicked listener;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener  {
        public ImageView imgPromo;
        public TextView titlePromo;

        public MyViewHolder(View v) {
            super(v);
            imgPromo = (ImageView) v.findViewById(R.id.thumbnail_promo);
            titlePromo = (TextView) v.findViewById(R.id.title_promo);

        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_promo, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        float scale = mContext.getResources().getConfiguration().fontScale;
        float sizeNeeded = (float) 14.0;

        //MenuCardModel item = itemList.get(position);
        final Obj_Promo.Datum dat = itemListFilter.data.get(position);

        String sID = dat.id;
        String sName = dat.name;
        String sURL = dat.url;

        AppController.getInstance().displayImage(mContext, sURL, holder.imgPromo);
        holder.titlePromo.setText(sName);
        holder.titlePromo.setTextSize(sizeNeeded/scale);

    }

    @Override
    public int getItemCount() {
        //return itemList.size();
        try {
            if ((itemListFilter.data != null)) {
                return itemListFilter.data.size();
            } else {
                return 0;
            }
        }catch (Exception e){
            Log.e("ErrorItemCount", e.toString());
        }

        return 0;
    }
}

