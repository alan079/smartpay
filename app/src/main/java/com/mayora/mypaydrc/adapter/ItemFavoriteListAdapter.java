package com.mayora.mypaydrc.adapter;

/**
 * Created by eroy on 18/09/2018.
 */

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.model.Obj_ListFavorite;

public class ItemFavoriteListAdapter extends RecyclerView.Adapter<ItemFavoriteListAdapter.MyViewHolder> {

    private Context mContext;
    Obj_ListFavorite itemList;

    public ItemFavoriteListAdapter(Context mContext, Obj_ListFavorite itemList, OnDownloadClicked listener) {
        this.mContext = mContext;
        this.itemList = itemList;
        this.listener = listener;
    }

    public interface OnDownloadClicked {
        void OnDownloadClicked(String ID, String DESC, String NO, String NAMA, String PROD, String CATEGORY, String Flag);
    }

    private OnDownloadClicked listener;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener  {
        public TextView txtDesc, txtNo, txtName, txtProd;
        ImageView btnEdit, btnDelete;
        View vFlag;
        LinearLayout layoutView;

        public MyViewHolder(View v) {
            super(v);

            txtDesc= v.findViewById(R.id.txt_desc);
            txtNo= v.findViewById(R.id.txt_nopel);
            txtName= v.findViewById(R.id.txt_name);
            txtProd= v.findViewById(R.id.txt_prod);
            btnEdit= v.findViewById(R.id.btn_edit);
            btnDelete= v.findViewById(R.id.btn_delete);
            vFlag= v.findViewById(R.id.flag_status);
            layoutView = v.findViewById(R.id.layout_card);

        }

        @Override
        public void onClick(View v) {
        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_favorite_list, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Obj_ListFavorite.Datum dat = itemList.data.get(position);

        holder.txtDesc.setText(dat.desc);
        holder.txtNo.setText(dat.destination);
        if(! (dat.destination_name == null)){
            holder.txtName.setText(dat.destination_name);
        }else{
            holder.txtName.setText("-");
        }
        holder.txtProd.setText(dat.product_name);

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.image_click));

                listener.OnDownloadClicked(dat.code, dat.desc, dat.destination, dat.destination_name, dat.product_name, dat.category, "EDIT");
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.image_click));

                listener.OnDownloadClicked(dat.code, dat.desc, dat.destination, dat.destination_name, dat.product_name, dat.category,"DELETE");
            }
        });

        holder.layoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnDownloadClicked(dat.code, dat.desc, dat.destination, dat.destination_name, dat.product_name, dat.category,"CLICK");
            }
        });



    }

    @Override
    public int getItemCount() {
        return itemList.data.size();
    }
}

