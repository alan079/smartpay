package com.mayora.mypaydrc.adapter;

/**
 * Created by eroy on 2/5/2017.
 */

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mayora.mypaydrc.R;
import com.mayora.mypaydrc.model.MenuCardModel;

import java.util.List;

public class MenuCardMainAdapter extends RecyclerView.Adapter<MenuCardMainAdapter.MyViewHolder> {

    private Context mContext;
    private List<MenuCardModel> menuList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public AppCompatImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            thumbnail = view.findViewById(R.id.thumbnail);
        }
    }


    public MenuCardMainAdapter(Context mContext, List<MenuCardModel> menuList) {
        this.mContext = mContext;
        this.menuList = menuList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_card_main, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        float scale = mContext.getResources().getConfiguration().fontScale;
        float sizeNeeded = (float) 12.0;

        MenuCardModel menu = menuList.get(position);
        holder.title.setText(menu.getName());
        holder.title.setTextSize(sizeNeeded/scale);
        holder.thumbnail.setImageResource(menu.getThumbnail());

        // loading album cover using Glide library
        //Glide.with(mContext).load(menu.getThumbnail()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }
}

