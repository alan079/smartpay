package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eroy on 15/11/2018.
 *
 * {
 * 	"device": "127.0.0.1",
 * 	"key": "13e4f4b87a882605cfce65f3434de5b3ad9bfd5e72fd4d4cf6f2429085b7761e",
 * 	"product": "PAMJKT01",
 * 	"reference": "123654879",
 * 	"subscriber": "9990312305",
 * 	"user": "SAFERI00120991",
 * 	"version": "1.0.2",
 * 	"rc": "00",
 * 	"message": "Inquiry PAMJKT01 Ke 9990312305 BERHASIL. Inquiry Sukses",
 * 	"inquiry_num": "1000002128",
 * 	"display": {
 * 		"HARGA": "51670",
 * 		"NAMA": "KOSASIH H",
 * 		"TOTAL TAGIHAN": "50000",
 * 		"BIAYA ADMIN": "1670",
 * 		"TAGIHAN": "45000",
 * 		"TAGIHAN BULAN": "201808",
 * 		"STAND METER": "00001000-00002500",
 * 		"PENALTY": "5000"
 *        }
 * }
 */

public class Obj_Inquiry_PDAM {
    public String rc ;
    public String message;
    public String inquiry_num;
    @SerializedName("display")
    public Display display;

    public String device;
    public String key;
    public String reference;
    public String user;
    public String version;
    public String product;
    public String subscriber;

    public Obj_Inquiry_PDAM(String device, String key, String reference, String user, String version, String product, String subscriber) {
        this.device = device;
        this.key = key;
        this.reference = reference;
        this.user = user;
        this.version = version;
        this.product = product;
        this.subscriber = subscriber;
    }

    public class Display
    {
        public String HARGA;
        public String NAMA;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        public String TAGIHAN;
        @SerializedName("TAGIHAN BULAN")
        public String TAGIHAN_BULAN;
        @SerializedName("STAND METER")
        public String STAND_METER;
        public String PENALTY;
        public String REGION;

    }
}
