package com.mayora.mypaydrc.model;

import java.util.List;

/**
 * Created by eroy on 19/06/2017.
 */

public class Obj_Purchase {
    public String rc ;
    public String message;
    public String serial;
    public String balance;
    public String receipt;

    public String user;
    public String reference;
    public String product;
    public String subscriber;
    public String price;
    public String key;
    public String version;
    public String device;

    public Obj_Purchase(String user, String reference, String product, String subscriber, String price, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.product = product;
        this.subscriber = subscriber;
        this.price = price;
        this.key = key;
        this.version = version;
        this.device = device;
    }

}
