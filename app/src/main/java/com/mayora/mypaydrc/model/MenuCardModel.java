package com.mayora.mypaydrc.model;

/**
 * Created by eroy on 2/05/2017.
 */

public class MenuCardModel {
    private String name;
    private int thumbnail;

    public MenuCardModel() {
    }

    public MenuCardModel(String name, int thumbnail) {
        this.name = name;
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}

