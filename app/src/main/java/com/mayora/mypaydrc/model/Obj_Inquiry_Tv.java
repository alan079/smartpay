package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

public class Obj_Inquiry_Tv {
    /**Request:
     {
     "device": "127.0.0.1",
     "key": "1dea60b23c10b46499ea2dc352b3b42c35e303cc3b51c60453f02abd40182391",
     "product": "INDOV01",
     "reference": "20190220113359",
     "subscriber": "301000022354",
     "user": "SAFERI00120991",
     "version": "1.0.2"
     }

     Response:
     {
     "device": "127.0.0.1",
     "key": "1dea60b23c10b46499ea2dc352b3b42c35e303cc3b51c60453f02abd40182391",
     "product": "INDOV01",
     "reference": "20190220113359",
     "subscriber": "301000022354",
     "user": "SAFERI00120991",
     "month": "1",
     "version": "1.0.2",
     "rc": "00",
     "message": "Inquiry INDOV01 Ke 301000022354 BERHASIL. Transaksi Sukses",
     "inquiry_num": "1000004556",
     "display": {
     "HARGA": "298401",
     "NAMA": "ANDREW SUHADINATA",
     "TOTAL TAGIHAN": "295901",
     "BIAYA ADMIN": "2500",
     "TAGIHAN BULAN": "23012008-22022008"
     }**/

    public String rc ;
    public String message;
    public String inquiry_num;
    @SerializedName("display")
    public Display display;

    public String device;
    public String key;
    public String product;
    public String reference;
    public String subscriber;
    public String user;
    public String version;

    //{"device","key","product","reference","subscriber","user","version"}
    public Obj_Inquiry_Tv(String device, String key, String product, String reference,
                                   String subscriber, String user, String version) {
        this.device = device;
        this.key = key;
        this.product = product;
        this.reference = reference;
        this.subscriber = subscriber;
        this.user = user;
        this.version = version;
    }

    public class Display{
        public String HARGA;
        public String NAMA;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        @SerializedName("TAGIHAN BULAN")
        public String TAGIHAN_BULAN;
    }

}
