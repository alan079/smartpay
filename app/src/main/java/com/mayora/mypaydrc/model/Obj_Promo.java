package com.mayora.mypaydrc.model;

import java.util.List;

public class Obj_Promo {
    public String rc ;
    public String message;
    public List<Datum> data;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;

    public Obj_Promo(String device, String key, String reference, String user, String version) {
        this.device = device;
        this.key = key;
        this.reference = reference;
        this.user = user;
        this.version = version;
    }

    public class Datum {
        public String id;
        public String name;
        public String url;
    }
}
