package com.mayora.mypaydrc.model;

public class MenuListModel {

	private int icon;
	private String title;
	private String counter;
	private boolean isAngle = false;

	private boolean isGroupHeader = false;

	public MenuListModel(String title) {
		this(-1,title);
		isGroupHeader = true;
	}
	public MenuListModel(int icon, String title) {
		super();
		this.icon = icon;
		this.title = title;
	}
	public MenuListModel(int icon, String title, String counter) {
		super();
		this.icon = icon;
		this.title = title;
		this.counter = counter;
	}
	public MenuListModel(int icon, String title, boolean angle) {
		super();
		this.icon = icon;
		this.title = title;
		isAngle = angle;
	}
	public int getIcon() {
		return icon;
	}
	public void setIcon(int icon) {
		this.icon = icon;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCounter() {
		return counter;
	}
	public void setCounter(String counter) {
		this.counter = counter;
	}
	public boolean isAngle() {
		return isAngle;
	}
	public void setAngle(boolean angle) {
		this.isAngle = angle;
	}
	public boolean isGroupHeader() {
		return isGroupHeader;
	}
	public void setGroupHeader(boolean isGroupHeader) {
		this.isGroupHeader = isGroupHeader;
	}
	
	
	
}

