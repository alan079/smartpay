package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

public class Obj_Payment_Tv {
    /**TV KABEL PAYMENT:
     Request:
     {
     "device": "127.0.0.1",
     "key": "743ae854b061d328623bf4b16578d870d03fe5e1cba08fe7f626ac8eecba4fca",
     "product": "INDOV01",
     "reference": "20190222120058",
     "subscriber": "301000020360",
     "user": "SAFERI00120991",
     "version": "1.0.2",
     "inquiry_num": "1000006474"
     }
     Response:
     {
     "device": "127.0.0.1",
     "key": "743ae854b061d328623bf4b16578d870d03fe5e1cba08fe7f626ac8eecba4fca",
     "product": "INDOV01",
     "reference": "20190222120058",
     "subscriber": "301000020360",
     "user": "SAFERI00120991",
     "version": "1.0.2",
     "inquiry_num": "1000006474",
     "rc": "00",
     "message": "Pembayaran INDOV01 Ke 301000020360 BERHASIL. Transaksi Sukses",
     "reffnum": "301800000368",
     "display": {
     "TANGGAL BAYAR": "22-02-2019 12:01:00",
     "NO. PEL\/TAGIHAN": "301000020360",
     "NAMA": "ERI DARMASETIAWAN",
     "BIAYA ADMIN": "2500",
     "BANK REFF": "301800000368",
     "TAGIHAN BULAN": "23012008-22022008",
     "TOTAL TAGIHAN": "229900",
     "HARGA": "232400"
     },
     "receipt": "https:\/\/mposdrc.clickmayora.com\/download.php?tid=101000006296&sid=301800000368&d=2019-02-22"
     }**/
    public String rc ;
    public String message;
    public String inquiry_num;
    @SerializedName("display")
    public Display display;
    public String receipt;

    public String device;
    public String key;
    public String product;
    public String reference;
    public String subscriber;
    public String user;
    public String version;

    //{"device","key","product","reference","subscriber","user","version","inquiry_num"}
    public Obj_Payment_Tv(String device, String key, String product, String reference,
                          String subscriber, String user, String version, String inquiry_num) {
        this.device = device;
        this.key = key;
        this.product = product;
        this.reference = reference;
        this.subscriber = subscriber;
        this.user = user;
        this.version = version;
        this.inquiry_num = inquiry_num;
    }

    public class Display{
        @SerializedName("TANGGAL BAYAR")
        public String TGL_BAYAR;
        @SerializedName("NO. PEL/TAGIHAN")
        public String NOPEL;
        public String NAMA;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        @SerializedName("BANK REFF")
        public String NOREF;
        @SerializedName("TAGIHAN BULAN")
        public String TAGIHAN_BULAN;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        public String HARGA;
    }
}
