package com.mayora.mypaydrc.model;

import java.util.List;

/**
 * Created by eroy on 25/07/2017.
 */

public class Obj_TopupHistory {

    public String rc ;
    public String message;
    public String total_data;
    public String total_paging;
    public List<Datum> data ;

    public String user;
    public String reference;
    public int page;
    public String key;
    public String version;
    public String device;

    public Obj_TopupHistory(String user, String reference, int page, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.page = page;
        this.key = key;
        this.version = version;
        this.device = device;
    }

    public class Datum
    {
        public String id;
        public String customer_id;
        public String customer_name;
        public String topup_date;
        public String topup_time;
        public String topup_nominal;
        public String topup_expire;
        public String topup_status;
    }
}
