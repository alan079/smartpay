package com.mayora.mypaydrc.model;

import java.util.List;

/**
 * Created by eroy on 14/06/2017.
 */

public class Obj_SubCategory {

    public String rc ;
    public String message;
    public List<Datum> data ;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;
    public String category;

    public Obj_SubCategory(String user, String reference, String category, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
        this.category = category;
    }

    public class Datum
    {
        public String product_id;
        public String product_code;
        public String name;
    }
}
