package com.mayora.mypaydrc.model;

/**
 * Created by eroy on 25/07/2017.
 */

public class Obj_Topup {
    public String rc ;
    public String message;

    public String user;
    public String reference;
    public String nominal;
    public String key;
    public String version;
    public String device;

    public Obj_Topup(String user, String reference, String nominal, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.nominal = nominal;
        this.key = key;
        this.version = version;
        this.device = device;
    }
}
