package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eroy on 15/11/2018.
 *
 * {
 * 	"device": "127.0.0.1",
 * 	"key": "8a80b7eb71f5b5c1e5172c0d8ab9ba364291f25122623bf9ba56cd2557fcb4a9",
 * 	"product": "PLNPOST01",
 * 	"reference": "1236500000",
 * 	"subscriber": "510400064594",
 * 	"user": "SAFERI00120991",
 * 	"version": "1.0.2",
 * 	"rc": "00",
 * 	"message": "Inquiry PLNPOST01 Ke 510400064594 BERHASIL. Inquiry Sukses",
 * 	"inquiry_num": "1000002132",
 * 	"display": {
 * 		"HARGA": "52000",
 * 		"NAMA": "DUMMY POSTPAID",
 * 		"TOTAL TAGIHAN": "50000",
 * 		"BIAYA ADMIN": "2000",
 * 		"TUNGGAKAN": "01",
 * 		"ADMIN CA": "2000",
 * 		"TAGIHAN BULAN": "JAN16",
 * 		"ID PEL": "510400064594"
 *        }
 * }
 */

public class Obj_Inquiry_PLNPOST {
    public String rc ;
    public String message;
    public String inquiry_num;
    @SerializedName("display")
    public Display display;

    public String device;
    public String key;
    public String reference;
    public String user;
    public String version;
    public String product;
    public String subscriber;

    public Obj_Inquiry_PLNPOST(String device, String key, String reference, String user, String version, String product,
                            String subscriber) {
        this.device = device;
        this.key = key;
        this.reference = reference;
        this.user = user;
        this.version = version;
        this.product = product;
        this.subscriber = subscriber;
    }

    public class Display
    {
        public String HARGA;
        public String NAMA;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        public String TUNGGAKAN;
        @SerializedName("TAGIHAN BULAN")
        public String TAGIHAN_BULAN;
        @SerializedName("ID PEL")
        public String IDPEL;
        @SerializedName("TIPE TRANSAKSI")
        public String TIPE_TRANS;
        @SerializedName("ADMIN CA")
        public String ADMIN_CA;

    }
}
