package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eroy on 25/07/2017.
 */

public class Obj_NominalTopup {
    public String rc ;
    public String message;
    @SerializedName("data")
    public Datum data;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;

    public Obj_NominalTopup(String user, String reference, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
    }

    public class Datum
    {
        public List<String> nominal;
    }
}
