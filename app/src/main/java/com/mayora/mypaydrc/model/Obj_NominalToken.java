package com.mayora.mypaydrc.model;

import java.util.List;

/**
 * Created by eroy on 15/11/2017.
 */

/*{
	"device": "",
	"key": "65ba992451c2f4cee272bc61ccf842f4465c39400524b2bd7d12375b27e4a45a",
	"reference": "20180906184134",
	"user": "ALMER000010190",
	"version": "1.0.2",
	"rc": "00",
	"message": "Sukses",
	"data": [{
		"product_id": "P212210PL0020",
		"name": "PLN PREPAID 20K",
		"nominal": 20000,
		"price": 20050
	}, {
		"product_id": "P212210PL0050",
		"name": "PLN PREPAID 50K",
		"nominal": 50000,
		"price": 50050
	}],
	"nomormeter": "10077764594",
	"namapelangan": "budi",
	"tarifdaya": "B1  \/000001300",
    "inquiry_reff":"00000012612200000000000000000000"
}*/

public class Obj_NominalToken {

    public String rc ;
    public String message;
    public List<Datum> data;
    public String namapelanggan;
    public String tarifdaya;
    public String inquiry_reff;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;
    public String nomormeter;

    public Obj_NominalToken(String user, String reference, String key, String version, String device, String nomormeter) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
        this.nomormeter = nomormeter;
    }

    public class Datum
    {
        public String product_id;
        public String name;
        public String nominal;
        public String price;
    }
}
