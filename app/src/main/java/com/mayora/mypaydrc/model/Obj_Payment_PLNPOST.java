package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eroy on 15/11/2018.
 *
 * {"device":"d5mMy2HY18U:APA91bEv1IhewHx1sdweXdLFy56j91n0d6zCa1AzpaTW4tIV8NKy8M9FobWevm3sA7_wsBnyVEzrTke3DpcaGZchY7C1FY4nVYM_7OgFBLQvdIDrQrBpOhq7726uAi2wwn-PuW8U0AZS","inquiry_num":"1000002428","key":"6ebb2f73e1faed242cc058850c736f4ea10a0c5b74816d940e5b0c11151ea1be",
 * "product":"PLNPOST01","reference":"301700000719","subscriber":"510400064594","user":"EROYTEST1507","version":"1.0.2","rc":"00",
 * "message":"Pembayaran PLNPOST01 Ke 510400064594 BERHASIL. Transaksi Sukses",
 * "display":{"SERIAL NUMBER":"0553180000000016426E","HARGA":"52000","NAMA":"DUMMY POSTPAID","TOTAL TAGIHAN":"50000",
 * "BIAYA ADMIN":"2000","TARIF\/DAYA":"R1  \/000000900","NO REFF":"0553180000000016426E","ADMIN CA":"2000",
 * "TAGIHAN BULAN":"JAN16","ID PEL":"510400064594"},
 * "receipt":"https:\/\/mposdrc.clickmayora.com\/download.php?tid=101000003941&sid=301700000719&d=2018-12-05"}
 */

public class Obj_Payment_PLNPOST{
    public String rc ;
    public String message;
    @SerializedName("display")
    public Display display;
    public String receipt;

    public String device;
    public String key;
    public String reference;
    public String user;
    public String version;
    public String product;
    public String subscriber;
    public String inquiry_num;

    public Obj_Payment_PLNPOST(String device, String key, String reference, String user, String version, String product, String subscriber,
                            String inquiry_num) {
        this.device = device;
        this.key = key;
        this.reference = reference;
        this.user = user;
        this.version = version;
        this.product = product;
        this.subscriber = subscriber;
        this.inquiry_num = inquiry_num;
    }

    public class Display
    {
        public String HARGA;
        public String NAMA;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        public String TUNGGAKAN;
        @SerializedName("TAGIHAN BULAN")
        public String TAGIHAN_BULAN;
        @SerializedName("ID PEL")
        public String IDPEL;
        @SerializedName("SERIAL NUMBER")
        public String SN;
        @SerializedName("NO REFF")
        public String NOREF;
        @SerializedName("TARIF/DAYA")
        public String TARIF;

    }
}
