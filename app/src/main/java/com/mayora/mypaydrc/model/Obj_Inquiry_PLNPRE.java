package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Obj_Inquiry_PLNPRE {
    /**
     * req:
     * {"device":"127.0.0.1",
     * "key":"4662535557afa6397c1ee28b3526a3b27a9dfd7b3b111472a4b92dc16312d1b6",
     * "product":"P212210PU0000",
     * "reference":"20190724114210",
     * "subscriber":"520077764594",
     * "user":"SAFERI00120991",
     * "month":"0",
     * version":"1.0.2"}
     * <p>
     * res:
     * {
     * "device": "127.0.0.1",
     * "key": "4662535557afa6397c1ee28b3526a3b27a9dfd7b3b111472a4b92dc16312d1b6",
     * "product": "P212210PU0000",
     * "reference": "20190724114210",
     * "subscriber": "520077764594",
     * "user": "SAFERI00120991",
     * "month": "0",
     * "version": "1.0.2",
     * "rc": "00",
     * "message": "Inquiry P212210PU0000 Ke 520077764594 BERHASIL. Inquiry Sukses",
     * "data": [{
     * "product_id": "P212210PU0020",
     * "name": "PLN PREPAID INQ 20K",
     * "nominal": 20000,
     * "price": 22500
     * }],
     * "nomormeter": "520077764594",
     * "namapelanggan": "Jo Mar\"i@a;GaS'i?\/*re^la",
     * "tarifdaya": "B1  \/000001300 VA VA",
     * "inquiry_reff": "1000016594"
     * }
     **/

    public String rc;
    public String message;
    public String[] data;

    public String device;
    public String key;
    public String product;
    public String reference;
    public String subscriber;
    public String user;
    public String version;
    public String nomormeter;
    public String namapelanggan;
    public String tarifdaya;
    public String inquiry_reff;

    public Obj_Inquiry_PLNPRE(String user, String reference, String key, String version, String device, String nomormeter) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
        this.nomormeter = nomormeter;
    }

    public Obj_Inquiry_PLNPRE(String device, String key, String product, String reference, String subscriber, String user, String version) {
        this.device = device;
        this.key = key;
        this.product = product;
        this.reference = reference;
        this.subscriber = subscriber;
        this.user = user;
        this.version = version;
    }
}
