package com.mayora.mypaydrc.model;

import java.util.List;

/**
 * Created by eroy on 05/07/2017.
 */

public class Obj_TransHistory {

    public String rc ;
    public String message;
    public String total_data;
    public String total_paging;
    public List<Datum> data ;

    public String user;
    public String reference;
    public String date;
    public int page;
    public String destination;
    public String key;
    public String version;
    public String device;

    public Obj_TransHistory(String user, String reference, String date, int page, String destination, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.date = date;
        this.page = page;
        this.destination = destination;
        this.key = key;
        this.version = version;
        this.device = device;
    }

    public class Datum
    {
        public String id;
        public String date;
        public String time;
        public String product;
        public String destination;
        public String price;
        public String status;
        public String info;
        public String reference;
    }
}
