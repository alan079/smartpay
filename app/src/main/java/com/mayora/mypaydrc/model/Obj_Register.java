package com.mayora.mypaydrc.model;

import java.util.List;

/**
 * Created by Eroy 28/03/2017.
 */

public class Obj_Register
{
    public String rc;
    public String info;
    public List<Datum> data;

    public String hashid;
    public String userid;
    public String pin;
    public String devid;
    public String name;
    public String email;
    public String hp;
    public String address;
    public String reqid;

    public Obj_Register(String hashid, String reqid, String userid, String pin, String devid, String name, String email,
                        String hp, String address) {
        this.hashid = hashid;
        this.reqid = reqid;
        this.userid = userid;
        this.pin = pin;
        this.devid = devid;
        this.name = name;
        this.email = email;
        this.hp = hp;
        this.address = address;
    }

    public class Datum
    {

    }


}