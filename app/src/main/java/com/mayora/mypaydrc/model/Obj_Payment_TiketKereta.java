package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

/**
 * KAI  TICKET PAYMENT:
 * Request :
 * {
 * "device": "127.0.0.1",
 * "key": "fd3de1d5b35d927e0662f67b98803caa650583cc28a85619627eae46947a3289",
 * "product": "KAIPAY01",
 * "reference": "20190220133146",
 * "subscriber": "1999666666001",
 * "user": "SAFERI00120991",
 * "version": "1.0.2",
 * "inquiry_num": "1000004563"
 * }
 * <p>
 * Response:
 * {
 * "device": "127.0.0.1",
 * "key": "fd3de1d5b35d927e0662f67b98803caa650583cc28a85619627eae46947a3289",
 * "product": "KAIPAY01",
 * "reference": "20190220133146",
 * "subscriber": "1999666666001",
 * "user": "SAFERI00120991",
 * "version": "1.0.2",
 * "inquiry_num": "1000004563",
 * "rc": "00",
 * "message": "Pembayaran KAIPAY01 Ke 1999666666001 BERHASIL. Transaksi Sukses",
 * "reffnum": "301800000350",
 * "display": {
 * "TANGGAL BAYAR": "20-02-2019 13:31:48",
 * "NO. PEL\/TAGIHAN": "1999666666001",
 * "NAMA": "PAK URRAY001",
 * "BIAYA ADMIN": "250",
 * "BANK REFF": "301800000350",
 * "JML PENUMPANG": "1",
 * "TGL BERANGKAT": "0043",
 * "JAM BERANGKAT": "1962",
 * "DARI": "JKT",
 * "TUJUAN": "SBY",
 * "NAMA KERETA": "PARAHIYANGAN",
 * "NO KERETA": "11111",
 * "NO KURSI": "EXA1-2A,2B,EXA2-2A,2B",
 * "TOTAL TAGIHAN": "431962",
 * "HARGA": "432212"
 * },
 * "receipt": "https:\/\/mposdrc.clickmayora.com\/download.php?tid=101000005896&sid=301800000350&d=2019-02-20"
 * }
 **/

public class Obj_Payment_TiketKereta {
    public String rc;
    public String message;
    @SerializedName("display")
    public Display display;
    public String receipt;

    public String device;
    public String key;
    public String product;
    public String reference;
    public String subscriber;
    public String user;
    public String version;
    public String inquiry_num;
    public String reffnum;

    public Obj_Payment_TiketKereta(String device, String key, String product, String reference,
                                   String subscriber, String user, String version, String inquiry_num) {
        this.device = device;
        this.key = key;
        this.product = product;
        this.reference = reference;
        this.subscriber = subscriber;
        this.user = user;
        this.version = version;
        this.inquiry_num = inquiry_num;
    }

    public class Display {
        @SerializedName("TANGGAL BAYAR")
        public String TGL_BAYAR;
        @SerializedName("NO. PEL/TAGIHAN")
        public String NOPEL;
        @SerializedName("BANK REFF")
        public String BANK_REFF;
        public String HARGA;
        public String NAMA;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        @SerializedName("JML PENUMPANG")
        public String JML_PENUMPANG;
        @SerializedName("TGL BERANGKAT")
        public String TGL_BRGKT;
        @SerializedName("JAM BERANGKAT")
        public String JAM_BRGKT;
        public String DARI;
        public String TUJUAN;
        @SerializedName("NAMA KERETA")
        public String NAMA_KERETA;
        @SerializedName("NO KERETA")
        public String NO_KERETA;
        @SerializedName("NO KURSI")
        public String NO_KURSI;

    }
}