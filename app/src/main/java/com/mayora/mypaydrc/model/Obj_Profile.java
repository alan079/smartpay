package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by eroy on 06/07/2017.
 */

public class Obj_Profile {
    public String rc ;
    public String message;
    @SerializedName("data")
    public Datum data;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;

    public Obj_Profile(String user, String reference, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
    }

    public class Datum
    {
        public String name;
        public String contact;
        public String address;
        public String email;
        public String balance;
        public String password_change;
        public String register_date;
        public String account;
    }
}
