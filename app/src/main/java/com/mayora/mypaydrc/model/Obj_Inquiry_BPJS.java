package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eroy on 15/11/2018.
 *
 * {
 * 	"device": "127.0.0.1",
 * 	"key": "d213a7911139d00bdc7e7a3196c8dc736d72d764eb56be99cb369d4ecd7b0b54",
 * 	"product": "P212240KSKLS1",
 * 	"reference": "20181128151043",
 * 	"subscriber": "0001260835569",
 * 	"user": "SAFERI00120991",
 * 	"month": "1",
 * 	"version": "1.0.2",
 * 	"rc": "00",
 * 	"message": "Inquiry P212240KSKLS1 Ke 0001260835569 BERHASIL. Inquiry Sukses",
 * 	"inquiry_num": "1000002346",
 * 	"display": {
 * 		"HARGA": "104500",
 * 		"NAMA": "SHAHNAAZ",
 * 		"TOTAL TAGIHAN": "102000",
 * 		"BIAYA ADMIN": "2500",
 * 		"TUNGGAKAN": "0",
 * 		"JUMLAH PESERTA": "1",
 * 		"ADMIN CA": "2500",
 * 		"TAGIHAN BULAN": "1",
 * 		"ID PEL": "0001260835569"
 *        }
 * }
 */

public class Obj_Inquiry_BPJS {
    public String rc ;
    public String message;
    public String inquiry_num;
    @SerializedName("display")
    public Display display;

    public String device;
    public String key;
    public String reference;
    public String user;
    public String version;
    public String product;
    public String subscriber;
    public String month;

    public Obj_Inquiry_BPJS(String device, String key, String reference, String user, String version, String product,
                            String subscriber, String month) {
        this.device = device;
        this.key = key;
        this.reference = reference;
        this.user = user;
        this.version = version;
        this.product = product;
        this.subscriber = subscriber;
        this.month = month;
    }

    public class Display
    {
        public String HARGA;
        public String NAMA;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        public String TUNGGAKAN;
        @SerializedName("TAGIHAN BULAN")
        public String TAGIHAN_BULAN;
        @SerializedName("JUMLAH PESERTA")
        public String JUMLAH_PESERTA;
        @SerializedName("ID PEL")
        public String IDPEL;

    }
}
