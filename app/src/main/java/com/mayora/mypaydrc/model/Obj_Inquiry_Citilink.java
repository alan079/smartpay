package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

/**
 * {
 * "device": "127.0.0.1",
 * "key": "8a80b7eb71f5b5c1e5172c0d8ab9ba364291f25122623bf9ba56cd2557fcb4a9",
 * "product": "KAIPAY01",
 * "reference": "1236500000",
 * "subscriber": "1396000000001",
 * "user": "SAFERI00120991",
 * "version": "1.0.2"
 * }
 * Response:
 * {
 * 	"device": "127.0.0.1",
 * 	"key": "5752ddefb68a79c54ecbc4c02ab52e9cb00695414c439fc59334b1d357613b33",
 * 	"product": "CITI01",
 * 	"reference": "20190222174544",
 * 	"subscriber": "K45SWI",
 * 	"user": "SAFERI00120991",
 * 	"version": "1.0.2",
 * 	"rc": "00",
 * 	"message": "Inquiry CITI01 Ke K45SWI BERHASIL. Transaksi Sukses",
 * 	"inquiry_num": "1000006574",
 * 	"display": {
 * 		"HARGA": "615000",
 * 		"NAMA": "Tes  Mayora",
 * 		"TOTAL TAGIHAN": "613700",
 * 		"BIAYA ADMIN": "1300",
 * 		"JML PENUMPANG": "01",
 * 		"DARI": "HLP",
 * 		"TUJUAN": "SUB",
 * 		"JML PENERBANGAN": "1",
 * 		"NO PENERBANGAN": "0345",
 * 		"CARRIER": "QG",
 * 		"TGL BERANGKAT": "2502",
 * 		"JAM BERANGKAT": "0200",
 * 		"KELAS": "Y",
 * 		"KODE PEMBAYARAN": "212628021017"
 *        }
 * }
 * }
 ***/

public class Obj_Inquiry_Citilink {
    public String rc ;
    public String message;
    public String inquiry_num;
    @SerializedName("display")
    public Display display;

    public String device;
    public String key;
    public String product;
    public String reference;
    public String subscriber;
    public String user;
    public String version;

    public Obj_Inquiry_Citilink(String device, String key, String product, String reference,
                                   String subscriber, String user, String version) {
        this.device = device;
        this.key = key;
        this.product = product;
        this.reference = reference;
        this.subscriber = subscriber;
        this.user = user;
        this.version = version;
    }

    public class Display{
        public String HARGA;
        public String NAMA;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        @SerializedName("JML PENUMPANG")
        public String JML_PENUMPANG;
        public String DARI;
        public String TUJUAN;
        @SerializedName("JML PENERBANGAN")
        public String JML_PENERBANGAN;
        @SerializedName("NO PENERBANGAN")
        public String NO_PENERBANGAN;
        public String CARRIER;
        @SerializedName("TGL BERANGKAT")
        public String TGL_BRGKT;
        @SerializedName("JAM BERANGKAT")
        public String JAM_BRGKT;
        public String KELAS;
        @SerializedName("KODE PEMBAYARAN")
        public String KODE_PEMBAYARAN;
    }
}
