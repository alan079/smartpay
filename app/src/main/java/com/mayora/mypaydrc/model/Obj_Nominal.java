package com.mayora.mypaydrc.model;

import java.util.List;

/**
 * Created by eroy on 14/06/2017.
 */

public class Obj_Nominal {

    public String rc ;
    public String message;
    public List<Datum> data ;

    public String user;
    public String reference;
    public String prefix;
    public String key;
    public String version;
    public String device;
    public String type;

    public Obj_Nominal(String user, String reference, String prefix, String key, String version, String device, String type) {
        this.user = user;
        this.reference = reference;
        this.prefix = prefix;
        this.key = key;
        this.version = version;
        this.device = device;
        this.type = type;
    }

    public class Datum
    {
        public String product_id;
        public String name;
        public String nominal;
        public String price;
    }
}
