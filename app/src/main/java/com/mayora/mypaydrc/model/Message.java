package com.mayora.mypaydrc.model;

/**
 * Created by eroy on 17/01/2018.
 */

public class Message {
    public int id;
    public String title;
    public String body;
    public String type;
    public String date;

    public Message(){
    }

    public Message(int id, String title, String body, String type, String date) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.type = type;
        this.date = date;
    }

    public Message(String title, String body, String type, String date) {
        this.title = title;
        this.body = body;
        this.type = type;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

