package com.mayora.mypaydrc.model;

import android.view.Display;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eroy on 17/10/2018.
 */

/**Request:
 {
 "device": "127.0.0.1",
 "key": "48b3cf1693a7705f387b454cbb59a6db47dfcfb600115d99e7f169a5b8f546bf",
 "product": "P212210PU0050",
 "reference": "20190123161435",
 "subscriber": "10077764594",
 "user": "SAFERI00120991",
 "price": "52500",
 "version": "1.0.2",
 "inquiry_num": "1000003674"
 }
 Response:
 {
 "device": "127.0.0.1",
 "key": "48b3cf1693a7705f387b454cbb59a6db47dfcfb600115d99e7f169a5b8f546bf",
 "product": "P212210PU0050",
 "reference": "20190123161435",
 "subscriber": "10077764594",
 "user": "SAFERI00120991",
 "version": "1.0.2",
 "inquiry_num": "1000003674",
 "rc": "00",
 "message": "Pembayaran P212210PU0050 Ke 10077764594 BERHASIL. Transaksi Sukses",
 "reffnum": "301700000969",
 "display": {
 "SERIAL NUMBER": "27783001548234877144",
 "TANGGAL BAYAR": "23-01-2019 16:14:38",
 "NO. PEL\/TAGIHAN": "10077764594",
 "NAMA": "Jo Mar\"i@a;GaS'i?\/*re^la",
 "BIAYA ADMIN": "2500",
 "TARIF\/DAYA": "B1  \/000001300",
 "NO REFF": "055318000000000362E2",
 "TOKEN": "27783001548234877144",
 "RP BAYAR": "52.500",
 "PPN": "0,00",
 "PPJ": "1.200,00",
 "ANGSURAN": "0,00",
 "RP STROOM": "48.800,00",
 "JUMLAH KWH": "61,0",
 "ADMIN CA": "2500",
 "ID PEL": "520077764594",
 "TOTAL TAGIHAN": "50000",
 "HARGA": "52500"
 },
 "receipt": "https:\/\/mposdrc.clickmayora.com\/download.php?tid=101000004801&sid=301700000969&d=2019-01-23"
 }
 **/

public class Obj_Payment_PLNPRE {
    public String rc;
    public String message;
    //public String serial;
    public String balance;
    public String receipt;

    @SerializedName("display")
    public Display display;

    public String device;
    public String key;
    public String reference;
    public String user;
    public String version;
    public String product;
    public String subscriber;
    public String inquiry_num;
    public String price;

    //public String inquiry_reff;
    //public String nominal;
    //public String transactionType;


    public Obj_Payment_PLNPRE(String device, String key, String product, String reference, String subscriber,
                              String user, String price, String version, String inquiry_num){
        this.device = device;
        this.key = key;
        this.product = product;
        this.reference = reference;
        this.subscriber = subscriber;
        this.user = user;
        this.price = price;
        this.version = version;
        this.inquiry_num = inquiry_num;
    }

    public class Display{
        @SerializedName("SERIAL NUMBER")
        public String SN;
        @SerializedName("TANGGAL BAYAR")
        public String TGL_BAYAR;
        @SerializedName("NO. PEL/TAGIHAN")
        public String NO_PEL;
        public String NAMA;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        @SerializedName("TARIF/DAYA")
        public String TARIF;
        @SerializedName("NO REFF")
        public String NOREF;
        public String TOKEN;
        @SerializedName("RP BAYAR")
        public String RP_BAYAR;
        @SerializedName("NO METER")
        public String NOMETER;
        public String PPN;
        public String PPJ;
        public String ANGSURAN;
        @SerializedName("RP STROOM")
        public String RP_STROOM;
        @SerializedName("JUMLAH KWH")
        public String KWH;
        @SerializedName("ADMIN CA")
        public String ADMIN;
        @SerializedName("ID PEL")
        public String IDPEL;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        public String HARGA;
    }
}
