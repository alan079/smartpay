package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eroy on 19/12/2018.
 *
 * {
 * 	"device": "127.0.0.1",
 * 	"key": "94e401c71c58b33a399e3f150492abf54f2c457a45eee86f855ee753fd7e5a15",
 * 	"product": "PAMJKT02",
 * 	"reference": "301700000749",
 * 	"subscriber": "90900001",
 * 	"user": "SAFERI00120991",
 * 	"month": "1",
 * 	"version": "1.0.2",
 * 	"advice": "1",
 * 	"inquiry_num": "301700000749",
 * 	"rc": "00",
 * 	"message": "Advice_PDAM PAMJKT02 Ke 90900001 BERHASIL. Transaksi Sukses",
 * 	"display": {
 * 		"HARGA": "37500",
 * 		"NAMA": "Aetra #90900001",
 * 		"TOTAL TAGIHAN": "35000",
 * 		"BIAYA ADMIN": "2500"
 *        },
 * 	"receipt": "https://mposdrc.clickmayora.com/download.php?tid=101000004061&sid=301700000749&d=2018-12-18"
 * }
 */

public class Obj_Advice_PDAM {
    public String rc ;
    public String message;
    @SerializedName("display")
    public Display display;
    public String receipt;
    public String reffnum;

    public String device;
    public String key;
    public String reference;
    public String user;
    public String version;
    public String product;
    public String subscriber;
    public String inquiry_num;
    public String advice;
    public String month;

    public Obj_Advice_PDAM(String device, String key, String reference, String user, String version, String product, String subscriber, String inquiry_num, String advice, String month) {
        this.device = device;
        this.key = key;
        this.reference = reference;
        this.user = user;
        this.version = version;
        this.product = product;
        this.subscriber = subscriber;
        this.inquiry_num = inquiry_num;
        this.advice = advice;
        this.month = month;
    }

    public class Display
    {
        @SerializedName("SERIAL NUMBER")
        public String SN;
        @SerializedName("TANGGAL BAYAR")
        public String TGL_BAYAR;
        @SerializedName("NO. PEL/TAGIHAN")
        public String NOPEL;
        public String NAMA;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        @SerializedName("NO REFF")
        public String NOREF;
        public String TAGIHAN;
        public String REGION;
        public String MLPO;
        public String PERIODE;
        @SerializedName("NO REFF BILLER")
        public String NOREF_BILLER;
        @SerializedName("ID PEL")
        public String IDPEL;
        @SerializedName("STAND METER")
        public String STAND_METER;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        public String HARGA;
        public String DENDA;

    }
    /**"SERIAL NUMBER": "154892087962326709",
            "TANGGAL BAYAR": "31-01-2019 14:50:12",
            "NO. PEL\/TAGIHAN": "7868020",
            "NAMA": "Palyja #007868020",
            "BIAYA ADMIN": "1200",
            "NO REFF": "154892087962326709",
            "TAGIHAN": "35000",
            "REGION": "Jakarta Palyja",
            "MLPO": "154892087962326709",
            "PERIODE": "JAN19,",
            "NO REFF BILLER": "20190131144922000000000000001302",
            "ID PEL": "007868020",
            "STAND METER": "30M3,",
            "TOTAL TAGIHAN": "35000",
            "HARGA": "37500" **/
}
