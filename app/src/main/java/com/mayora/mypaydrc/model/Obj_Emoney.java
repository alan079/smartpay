package com.mayora.mypaydrc.model;

import java.util.List;

/**
 * Created by eroy on 14/06/2017.
 */

public class Obj_Emoney {

    public String rc ;
    public String message;
    public List<Datum> data ;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;

    public Obj_Emoney(String user, String reference, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
    }

    public class Datum
    {
        public String product_category_code;
        public String name;
    }
}
