package com.mayora.mypaydrc.model;

import java.util.List;

/**
 * Created by eroy on 06/07/2017.
 */

public class Obj_Price {
    public String rc ;
    public String message;
    public List<Datum> data ;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;

    public Obj_Price(String user, String reference, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
    }

    public class Datum
    {
        public String product_id;
        public String name;
        public String nominal;
        public String price;
    }
}
