package com.mayora.mypaydrc.model;

import java.util.List;

/**
 * Created by eroy on 18/09/2018.
 */

public class Obj_ListFavorite {

    public String rc ;
    public String message;
    public List<Datum> data ;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;
    public String action;

    public Obj_ListFavorite(String user, String reference, String key, String version, String device, String action) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
        this.action = action;
    }

    public class Datum
    {
        public String code;
        public String category;
        public String desc;
        public String product_id;
        public String product_name;
        public String nominal;
        public String destination;
        public String destination_name;
    }
}
