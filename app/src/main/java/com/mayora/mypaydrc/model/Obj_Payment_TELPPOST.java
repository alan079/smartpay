package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eroy on 15/11/2018.
 *
 * {
 * 	"device": "127.0.0.1",
 * 	"key": "fbc0b8c8a76d292e9fbd0e902c155260caec83caa4c58658b2296384270a28cd",
 * 	"product": "P211110PPHALO",
 * 	"reference": "1980000010",
 * 	"subscriber": "08125550002",
 * 	"user": "SAFERI00120991",
 * 	"version": "1.0.2",
 * 	"inquiry_num": "301800000201",
 * 	"rc": "00",
 * 	"message": "Pembayaran P211110PPHALO Ke 08125550002 BERHASIL. Transaksi Sukses",
 * 	"display": {
 * 		"HARGA": "11050",
 * 		"NAMA": "IDHXXXXXXXXXXXXXXXXXIBI",
 * 		"TOTAL TAGIHAN": "10000",
 * 		"BIAYA ADMIN": "1050",
 * 		"NO REFF": "81176628424",
 * 		"TUNGGAKAN": "1"
 *        }
 * }
 */

public class Obj_Payment_TELPPOST {
    public String rc ;
    public String message;
    @SerializedName("display")
    public Display display;
    public String receipt;

    public String device;
    public String key;
    public String reference;
    public String user;
    public String version;
    public String product;
    public String subscriber;
    public String inquiry_num;

    public Obj_Payment_TELPPOST(String device, String key, String reference, String user, String version, String product, String subscriber, String inquiry_num) {
        this.device = device;
        this.key = key;
        this.reference = reference;
        this.user = user;
        this.version = version;
        this.product = product;
        this.subscriber = subscriber;
        this.inquiry_num = inquiry_num;
    }

    public class Display
    {
        public String HARGA;
        public String NAMA;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        public String TUNGGAKAN;
        @SerializedName("NO REFF")
        public String NOREF;

    }
}
