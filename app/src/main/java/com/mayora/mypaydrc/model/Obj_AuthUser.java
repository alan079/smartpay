package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Eroy 28/03/2017.
 */

public class Obj_AuthUser
{
    public String rc;
    public String message;

    @SerializedName("data")
    public Data data;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;

    public Obj_AuthUser(String user, String reference, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
    }

    public class Data
    {
        public String name;
        public String last_login;
    }

}