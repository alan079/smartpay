package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

/**
 * {
 * "device": "127.0.0.1",
 * "key": "8a80b7eb71f5b5c1e5172c0d8ab9ba364291f25122623bf9ba56cd2557fcb4a9",
 * "product": "KAIPAY01",
 * "reference": "1236500000",
 * "subscriber": "1396000000001",
 * "user": "SAFERI00120991",
 * "version": "1.0.2"
 * }
 * Response:
 * {
 * "device": "127.0.0.1",
 * "key": "8a80b7eb71f5b5c1e5172c0d8ab9ba364291f25122623bf9ba56cd2557fcb4a9",
 * "product": "KAIPAY01",
 * "reference": "1236500000",
 * "subscriber": "1396000000001",
 * "user": "SAFERI00120991",
 * "version": "1.0.2",
 * "rc": "00",
 * "message": "Inquiry KAIPAY01 Ke 1396000000001 BERHASIL. Transaksi Sukses",
 * "inquiry_num": "1000002136",
 * "display": {
 * "HARGA": "249753",
 * "NAMA": "PAK URRAY001",
 * "TOTAL TAGIHAN": "249503",
 * "BIAYA ADMIN": "250",
 * "JML PENUMPANG": "1",
 * "TGL BERANGKAT": "0024",
 * "JAM BERANGKAT": "9503",
 * "DARI": "JKT",
 * "TUJUAN": "SBY",
 * "NAMA KERETA": "PARAHIYANGAN",
 * "NO KERETA": "11111",
 * "NO KURSI": "EXA1-2A,2B,EXA2-2A,2B"
 * }
 * }
 ***/

public class Obj_Inquiry_TiketKereta {
    public String rc ;
    public String message;
    public String inquiry_num;
    @SerializedName("display")
    public Display display;

    public String device;
    public String key;
    public String product;
    public String reference;
    public String subscriber;
    public String user;
    public String version;

    public Obj_Inquiry_TiketKereta(String device, String key, String product, String reference,
                                   String subscriber, String user, String version) {
        this.device = device;
        this.key = key;
        this.product = product;
        this.reference = reference;
        this.subscriber = subscriber;
        this.user = user;
        this.version = version;
    }

    public class Display{
        public String HARGA;
        public String NAMA;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        @SerializedName("JML PENUMPANG")
        public String JML_PENUMPANG;
        @SerializedName("TGL BERANGKAT")
        public String TGL_BRGKT;
        @SerializedName("JAM BERANGKAT")
        public String JAM_BRGKT;
        public String DARI;
        public String TUJUAN;
        @SerializedName("NAMA KERETA")
        public String NAMA_KERETA;
        @SerializedName("NO KERETA")
        public String NO_KERETA;
        @SerializedName("NO KURSI")
        public String NO_KURSI;
    }
}
