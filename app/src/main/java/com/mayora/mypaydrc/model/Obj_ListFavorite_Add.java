package com.mayora.mypaydrc.model;

import java.util.List;

/**
 * Created by eroy on 20/09/2018.
 */

public class Obj_ListFavorite_Add {

    public String rc ;
    public String message;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;
    public String action;
    public String category;
    public String product;
    public String nominal;
    public String destination;
    public String destination_name;

    public Obj_ListFavorite_Add(String user, String reference, String key, String version, String device, String action,
                                String category, String product, String nominal, String destination, String destination_name) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
        this.action = action;
        this.category = category;
        this.product = product;
        this.nominal = nominal;
        this.destination = destination;
        this.destination_name = destination_name;
    }

}
