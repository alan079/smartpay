package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eroy on 15/11/2018.
 *
 * {
 * 	"device": "127.0.0.1",
 * 	"key": "6456e9d9d7df8165d04d3dfa0bae1cac48597196a570e17e34f0dc1a3dbadad9",
 * 	"product": "P212240KSKLS1",
 * 	"reference": "301700000700",
 * 	"subscriber": "0001260835569",
 * 	"user": "SAFERI00120991",
 * 	"month": "1",
 * 	"version": "1.0.2",
 * 	"inquiry_num": "1000002345",
 * 	"rc": "00",
 * 	"message": "Pembayaran P212240KSKLS1 Ke 0001260835569 BERHASIL. Transaksi Sukses",
 * 	"display": {
 * 		"SERIAL NUMBER": "96EEA623123EAA26",
 * 		"HARGA": "104500",
 * 		"NAMA": "SHAHNAAZ",
 * 		"TOTAL TAGIHAN": "102000",
 * 		"BIAYA ADMIN": "2500",
 * 		"NO REFF": "154339078964918081",
 * 		"TUNGGAKAN": "0",
 * 		"JUMLAH PESERTA": "1",
 * 		"ADMIN CA": "2500",
 * 		"TAGIHAN BULAN": "1",
 * 		"ID PEL": "0001260835569"
 *        },
 * 	"receipt": "https:\/\/mposdrc.clickmayora.com\/download.php?tid=101000003818&sid=301700000700&d=2018-11-28"
 * }
 */

public class Obj_Payment_BPJS{
    public String rc ;
    public String message;
    @SerializedName("display")
    public Display display;
    public String receipt;

    public String device;
    public String key;
    public String reference;
    public String user;
    public String version;
    public String product;
    public String subscriber;
    public String inquiry_num;
    public String month;

    public String reffnum;

    public Obj_Payment_BPJS(String device, String key, String reference, String user, String version, String product, String subscriber,
                            String inquiry_num, String month) {
        this.device = device;
        this.key = key;
        this.reference = reference;
        this.user = user;
        this.version = version;
        this.product = product;
        this.subscriber = subscriber;
        this.inquiry_num = inquiry_num;
        this.month = month;
    }

    public class Display
    {
        public String HARGA;
        public String NAMA;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        public String TUNGGAKAN;
        @SerializedName("TAGIHAN BULAN")
        public String TAGIHAN_BULAN;
        @SerializedName("JUMLAH PESERTA")
        public String JUMLAH_PESERTA;
        @SerializedName("ID PEL")
        public String IDPEL;
        @SerializedName("SERIAL NUMBER")
        public String SN;
        @SerializedName("NO REFF")
        public String NOREF;

    }
}
