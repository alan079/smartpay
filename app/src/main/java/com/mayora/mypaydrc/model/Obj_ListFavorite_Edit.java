package com.mayora.mypaydrc.model;

import java.util.List;

/**
 * Created by eroy on 20/09/2018.
 */

public class Obj_ListFavorite_Edit {

    public String rc ;
    public String message;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;
    public String action;
    public String favorit_code;
    public String desc;

    public Obj_ListFavorite_Edit(String user, String reference, String key, String version, String device, String action, String favorit_code, String desc) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
        this.action = action;
        this.favorit_code = favorit_code;
        this.desc = desc;
    }

}
