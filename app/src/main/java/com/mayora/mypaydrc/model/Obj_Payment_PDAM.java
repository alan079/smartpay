package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eroy on 15/11/2018.
 *
 * {
 * 	"device": "127.0.0.1",
 * 	"key": "f1416188a18cc17868805a19b8a6a2ac970cc291de9365fc0f21b17cd4ba8efc",
 * 	"product": "PAMJKT01",
 * 	"reference": "1980000000",
 * 	"subscriber": "9990312305",
 * 	"user": "SAFERI00120991",
 * 	"version": "1.0.2",
 * 	"inquiry_num": "302100000172",
 * 	"rc": "00",
 * 	"message": "Pembayaran PAMJKT01 Ke 9990312305 BERHASIL. Transaksi Sukses",
 * 	"display": {
 * 		"HARGA": "51670",
 * 		"NAMA": "KOSASIH H",
 * 		"TOTAL TAGIHAN": "50000",
 * 		"BIAYA ADMIN": "1670",
 * 		"TAGIHAN": "50000",
 * 		"TAGIHAN BULAN": "201808"
 * 	               "SERIAL NUMBER": "2130OO6QF5AS"
 *        }
 * }
 */

public class Obj_Payment_PDAM {
    public String rc ;
    public String message;
    @SerializedName("display")
    public Display display;
    public String receipt;

    public String device;
    public String key;
    public String reference;
    public String user;
    public String version;
    public String product;
    public String subscriber;
    public String inquiry_num;

    public String reffnum;

    public Obj_Payment_PDAM(String device, String key, String reference, String user, String version, String product, String subscriber, String inquiry_num) {
        this.device = device;
        this.key = key;
        this.reference = reference;
        this.user = user;
        this.version = version;
        this.product = product;
        this.subscriber = subscriber;
        this.inquiry_num = inquiry_num;
    }

    public class Display
    {
        public String HARGA;
        public String NAMA;
        @SerializedName("TOTAL TAGIHAN")
        public String TOTAL_TAGIHAN;
        @SerializedName("BIAYA ADMIN")
        public String BIAYA_ADMIN;
        public String TAGIHAN;
        public String PERIODE;
        @SerializedName("TAGIHAN BULAN")
        public String TAGIHAN_BULAN;
        @SerializedName("SERIAL NUMBER")
        public String SN;

    }
}
