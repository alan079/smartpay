package com.mayora.mypaydrc.model;

/**
 * Created by eroy on 24/08/2017.
 */

public class Obj_SmsDevice {

    public String rc ;
    public String message;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;

    public Obj_SmsDevice(String user, String reference, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
    }
}
