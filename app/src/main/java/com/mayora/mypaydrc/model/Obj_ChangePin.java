package com.mayora.mypaydrc.model;

/**
 * Created by eroy on 06/07/2017.
 */

public class Obj_ChangePin {

    public String rc ;
    public String message;

    public String user;
    public String reference;
    public String new_password;
    public String key;
    public String version;
    public String device;

    public Obj_ChangePin(String user, String reference, String new_password, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.new_password = new_password;
        this.key = key;
        this.version = version;
        this.device = device;
    }

}
