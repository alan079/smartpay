package com.mayora.mypaydrc.model;

/**
 * Created by eroy on 24/08/2017.
 */

public class Obj_VerificationDevice {

    public String rc ;
    public String message;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;
    public String code;

    public Obj_VerificationDevice(String user, String reference, String key, String version, String device, String code) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
        this.code = code;
    }
}
