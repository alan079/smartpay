package com.mayora.mypaydrc.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eroy on 14/06/2017.
 */

public class Obj_SummaryTrx {

    public String rc;
    public String message;

    @SerializedName("data")
    public Data data;

    public String user;
    public String reference;
    public String key;
    public String version;
    public String device;

    public Obj_SummaryTrx(String user, String reference, String key, String version, String device) {
        this.user = user;
        this.reference = reference;
        this.key = key;
        this.version = version;
        this.device = device;
    }

    public class Data
    {
        public String nominal;
        public String count;
    }
}
