package com.mayora.mypaydrc.model;

/**
 * Created by eroy on 2/05/2017.
 */

public class ItemNominalModel {
    private String nominal, harga;

    public ItemNominalModel() {
    }

    public ItemNominalModel(String nominal, String harga) {
        this.nominal = nominal;
        this.harga = harga;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }


}

