package com.mayora.mypaydrc.fcm;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mayora.mypaydrc.activity.Login;
import com.mayora.mypaydrc.activity.Main;
import com.mayora.mypaydrc.app.AppConfig;
import com.mayora.mypaydrc.app.AppController;
import com.mayora.mypaydrc.model.Obj_AuthUser;

public class MyAndroidFirebaseMsgService extends FirebaseMessagingService {
    private static final String TAG = "MyPayFCMService";
    Intent intent;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        AppConfig.IMEI = s;
        Log.d("FirebaseToken", s);

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            //Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            sendPushNotification("Mypay Notification",remoteMessage.getNotification().getBody(),"","");
        }

        //Log data to Log Cat
        if (remoteMessage.getData().size() > 0) {
            //Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                //JSONObject json = new JSONObject(remoteMessage.getData().toString());
                String title = remoteMessage.getData().get("title");
                String message = remoteMessage.getData().get("message");
                String type = remoteMessage.getData().get("type");
                String imageUrl = remoteMessage.getData().get("imageUrl");

                AppController.getInstance().getSessionManager().putStringData(AppConfig.PARAM_FCM,title);
                sendPushNotification(title,message,imageUrl,type);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void sendPushNotification(String title, String message, String imageUrl, String type) {
        //optionally we can display the json into log
        try {
            //getting the json data
            //creating MyNotificationManager object
            Obj_AuthUser authUser = AppController.getInstance().getSessionManager().getUserProfile();
            MyNotificationManager mNotificationManager = new MyNotificationManager(getApplicationContext());

            if(authUser != null){
                intent = new Intent(getApplicationContext(), Main.class);
                intent.putExtra("type", type);
            }else{
                intent = new Intent(getApplicationContext(), Login.class);
                intent.putExtra("type", type);
            }

            //if there is no image
            if(imageUrl == null){
                //displaying small notification
                mNotificationManager.showSmallNotification(title, message, intent);

            }else{
                //if there is an image
                //displaying a big notification
                mNotificationManager.showBigNotification(title, message, imageUrl, intent);
            }

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

}