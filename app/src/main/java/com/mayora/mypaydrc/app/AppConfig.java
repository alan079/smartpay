package com.mayora.mypaydrc.app;

import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by ipnetsoft1 on 29/04/2017.
 */

public final class AppConfig {

    /* Editor for Shared preferences ----------------------------------------------------------- */
    public static SharedPreferences pref;
    public static final String EMPTY_STRING = "";
    public static final String PARAM_FCM = "smartpaydrc_param_fcm";
    public static final String PUSH_NOTIFICATION = "smartpaydrc_push_notification";

    public static String HASHID ;
    public static String REQID ;
    public static String USERID = "" ;
    public static String USERNAME ;
    public static String EMAIL = "";
    public static String HP = "";
    public static String ADDRESS = "";
    public static String PIN;
    public static String IMEI = "";
    public static String KEY ;

    public static String NO_PELANGGAN = "";
    public static String NOMINAL = "";
    public static String HARGA = "";
    public static String BALANCE = "";

    public static String NOTIF_COUNTER = "notif_counter" ;

    public static String SIGN = "1pn3tM4y0r4" ;

    /* DOMAIN URL  DEV ---------------------------------------------------------------------------- */
    /* public static final String DOMAIN_URL = "https://mposdrc.clickmayora.com/mayora_trans"; */
    /* DOMAIN URL  PROD ---------------------------------------------------------------------------- */
    public static final String DOMAIN_URL = "https://mpos.clickmayora.com/mayora_trans";
    public final static String API_VERSION = "/API/secure.php/";

    public static String FOLDER_DOWNLOAD = "/Mayora/SmartPayDRC/Download";
    public static String STORAGE_CARD = Environment.getExternalStorageDirectory().toString() + "/Mayora/SmartPayDRC";
    public static String PDF_FILENAME = "";

}

