package com.mayora.mypaydrc.net;

import com.mayora.mypaydrc.model.Obj_Advice_BPJS;
import com.mayora.mypaydrc.model.Obj_Advice_PDAM;
import com.mayora.mypaydrc.model.Obj_AuthUser;
import com.mayora.mypaydrc.model.Obj_CekSaldo;
import com.mayora.mypaydrc.model.Obj_ChangePin;
import com.mayora.mypaydrc.model.Obj_Emoney;
import com.mayora.mypaydrc.model.Obj_Emoney_Nominal;
import com.mayora.mypaydrc.model.Obj_Inquiry_BPJS;
import com.mayora.mypaydrc.model.Obj_Inquiry_Citilink;
import com.mayora.mypaydrc.model.Obj_Inquiry_PDAM;
import com.mayora.mypaydrc.model.Obj_Inquiry_PLNPOST;
import com.mayora.mypaydrc.model.Obj_Inquiry_PLNPRE;
import com.mayora.mypaydrc.model.Obj_Inquiry_TELPPOST;
import com.mayora.mypaydrc.model.Obj_Inquiry_TiketKereta;
import com.mayora.mypaydrc.model.Obj_Inquiry_Tv;
import com.mayora.mypaydrc.model.Obj_ListFavorite;
import com.mayora.mypaydrc.model.Obj_ListFavorite_Add;
import com.mayora.mypaydrc.model.Obj_ListFavorite_Delete;
import com.mayora.mypaydrc.model.Obj_ListFavorite_Edit;
import com.mayora.mypaydrc.model.Obj_Nominal;
import com.mayora.mypaydrc.model.Obj_NominalToken;
import com.mayora.mypaydrc.model.Obj_NominalTopup;
import com.mayora.mypaydrc.model.Obj_Payment_BPJS;
import com.mayora.mypaydrc.model.Obj_Payment_Citilink;
import com.mayora.mypaydrc.model.Obj_Payment_PLNPOST;
import com.mayora.mypaydrc.model.Obj_Payment_PLNPRE;
import com.mayora.mypaydrc.model.Obj_Payment_PDAM;
import com.mayora.mypaydrc.model.Obj_Payment_TELPPOST;
import com.mayora.mypaydrc.model.Obj_Payment_TiketKereta;
import com.mayora.mypaydrc.model.Obj_Payment_Tv;
import com.mayora.mypaydrc.model.Obj_Price;
import com.mayora.mypaydrc.model.Obj_Profile;
import com.mayora.mypaydrc.model.Obj_Promo;
import com.mayora.mypaydrc.model.Obj_Purchase;
import com.mayora.mypaydrc.model.Obj_Register;
import com.mayora.mypaydrc.model.Obj_SmsDevice;
import com.mayora.mypaydrc.model.Obj_SubCategory;
import com.mayora.mypaydrc.model.Obj_SummaryTrx;
import com.mayora.mypaydrc.model.Obj_Test;
import com.mayora.mypaydrc.model.Obj_Topup;
import com.mayora.mypaydrc.model.Obj_TopupHistory;
import com.mayora.mypaydrc.model.Obj_TransHistory;
import com.mayora.mypaydrc.model.Obj_VerificationDevice;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by eroy on 29/05/2017.
 */
public interface NetworkService {

    @PUT("login/")
    Call<Obj_AuthUser> loginUser(@Body Obj_AuthUser params);

    @PUT("requestsms/")
    Call<Obj_SmsDevice> reqSmsDevice(@Body Obj_SmsDevice params);

    @POST("user/register")
    Call<Obj_Register> regUser(@Body Obj_Register params);

    @PUT("password/")
    Call<Obj_ChangePin> changePass(@Body Obj_ChangePin params);

    @POST("updatedevice/")
    Call<Obj_VerificationDevice> verifikasiDevice(@Body Obj_VerificationDevice params);

    @POST("saldo/")
    Call<Obj_CekSaldo> cekSaldo(@Body Obj_CekSaldo params);

    @POST("prefix/")
    Call<Obj_Nominal> getNominal(@Body Obj_Nominal params);

    @PUT("purchase/")
    Call<Obj_Purchase> purchase(@Body Obj_Purchase params);

    @POST("history/")
    Call<Obj_TransHistory> getTransHistory(@Body Obj_TransHistory params);

    @POST("profile/")
    Call<Obj_Profile> getProfile(@Body Obj_Profile params);

    @POST("price/")
    Call<Obj_Price> getPrice(@Body Obj_Price params);

    @POST("promosi/")
    Call<Obj_Promo> getPromo (@Body Obj_Promo params);

    @POST("summary/")
    Call<Obj_SummaryTrx> getSummaryTrx(@Body Obj_SummaryTrx params);

    @POST("ticketnominal/")
    Call<Obj_NominalTopup> getNomTopup(@Body Obj_NominalTopup params);

    @PUT("topup/")
    Call<Obj_Topup> topup(@Body Obj_Topup params);

    @POST("historytopup/")
    Call<Obj_TopupHistory> getTopupHistory(@Body Obj_TopupHistory params);

    @POST("priceplnprepaid/")   
    Call<Obj_Test> inquiryTest(@Body Obj_Test params);

    @POST("priceplnprepaid/")
    Call<Obj_NominalToken> getNomToken(@Body Obj_NominalToken params);

    @POST("plnpriceinquiry")
    Call<Obj_Inquiry_PLNPRE> inquiryPLNPRE (@Body Obj_Inquiry_PLNPRE params);

    @PUT("inquiry/")
    Call<Obj_Inquiry_PDAM> inquiry (@Body Obj_Inquiry_PDAM params);

    @PUT("inquiry/")
    Call<Obj_Inquiry_PDAM> inquiryPDAM (@Body Obj_Inquiry_PDAM params);

    @PUT("inquiry/")
    Call<Obj_Inquiry_BPJS> inquiryBPJS (@Body Obj_Inquiry_BPJS params);

    @PUT("inquiry/")
    Call<Obj_Inquiry_PLNPOST> inquiryPLNPOST (@Body Obj_Inquiry_PLNPOST params);

    @PUT("inquiry/")
    Call<Obj_Inquiry_TELPPOST> inquiryTELPPOST (@Body Obj_Inquiry_TELPPOST params);

    @PUT("inquiry/")
    Call<Obj_Inquiry_TiketKereta> inquiryTiketKereta(@Body Obj_Inquiry_TiketKereta params);

    @PUT("inquiry/")
    Call<Obj_Inquiry_Tv> inquiryTv (@Body Obj_Inquiry_Tv params);

    @PUT("inquiry/")
    Call<Obj_Inquiry_Citilink> inquiryCitilink (@Body Obj_Inquiry_Citilink params);

    @PUT("payment/")
    Call<Obj_Payment_Citilink> paymentCitilink (@Body Obj_Payment_Citilink params);

    @PUT("payment/")
    Call<Obj_Payment_Tv> paymentTv (@Body Obj_Payment_Tv params);

    @PUT("payment/")
    Call<Obj_Payment_TiketKereta> paymentTiketKereta (@Body Obj_Payment_TiketKereta params);

    @PUT("payment/")
    Call<Obj_Payment_PLNPRE> payment (@Body Obj_Payment_PLNPRE params);

    @PUT("payment/")
    Call<Obj_Payment_PDAM> paymentPDAM (@Body Obj_Payment_PDAM params);

    @PUT("payment/")
    Call<Obj_Advice_PDAM> advicePDAM (@Body Obj_Advice_PDAM params);

    @PUT("payment/")
    Call<Obj_Payment_BPJS> paymentBPJS (@Body Obj_Payment_BPJS params);

    @PUT("payment/")
    Call<Obj_Advice_BPJS> adviceBPJS (@Body Obj_Advice_BPJS params);

    @PUT("payment/")
    Call<Obj_Payment_PLNPOST> paymentPLNPOST (@Body Obj_Payment_PLNPOST params);

    @PUT("payment/")
    Call<Obj_Payment_TELPPOST> paymentTelpPOST (@Body Obj_Payment_TELPPOST params);

    @POST("productinquiry/")
    Call<Obj_SubCategory> getSubCategoryProd (@Body Obj_SubCategory params);

    @POST("emoney/")
    Call<Obj_Emoney> getEmoney(@Body Obj_Emoney params);

    @POST("emoney/")
    Call<Obj_Emoney_Nominal> getEmoneyNominal(@Body Obj_Emoney_Nominal params);

    @PUT("favorit/")
    Call<Obj_ListFavorite> getListFavorite(@Body Obj_ListFavorite params);

    @PUT("favorit/")
    Call<Obj_ListFavorite_Delete> getListFavoriteDelete(@Body Obj_ListFavorite_Delete params);

    @PUT("favorit/")
    Call<Obj_ListFavorite_Edit> getListFavoriteEdit(@Body Obj_ListFavorite_Edit params);

    @PUT("favorit/")
    Call<Obj_ListFavorite_Add> getListFavoriteAdd(@Body Obj_ListFavorite_Add params);

}

