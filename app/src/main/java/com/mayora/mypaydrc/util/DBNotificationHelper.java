package com.mayora.mypaydrc.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mayora.mypaydrc.model.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eroy on 17/01/2018.
 */

public class DBNotificationHelper extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "db_smartpaydrc";

    // Contacts table name
    private static final String TABLE_MESSAGE = "message";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_BODY = "body";
    private static final String KEY_TYPE = "type";
    private static final String KEY_DATE = "date";

    public DBNotificationHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_MESSAGE + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TITLE + " TEXT,"
                + KEY_BODY + " TEXT," + KEY_TYPE +" TEXT, " +  KEY_DATE +" TEXT " + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGE);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    public void addMessage(Message message) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE,message.getTitle());
        values.put(KEY_BODY,message.getBody());
        values.put(KEY_TYPE,message.getType());
        values.put(KEY_DATE,message.getDate());
        // Inserting Row
        db.insert(TABLE_MESSAGE, null, values);
        db.close(); // Closing database connection
    }

    // Getting single contact
    public Message getMessage(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_MESSAGE, new String[] { KEY_ID,
                        KEY_TITLE, KEY_BODY, KEY_TYPE, KEY_DATE }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Message message = new Message(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4));
        // return contact
        return message;
    }

    // Getting All Contacts
    public List<Message> getAllMessages(int start, int end) {
        List<Message> messageList = new ArrayList<Message>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_MESSAGE + " ORDER BY " + KEY_ID + " DESC LIMIT " + start +", " + end;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                Message message= new Message();
                message.setId(Integer.parseInt(cursor.getString(0)));
                message.setTitle(cursor.getString(1));
                message.setBody(cursor.getString(2));
                message.setType(cursor.getString(3));
                message.setDate(cursor.getString(4));
                messageList.add(message);
            } while (cursor.moveToNext());
        }

        // return contact list
        return messageList;
    }

    // Updating single contact
    public int updateMessage(Message message) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TYPE, message.getTitle());
        values.put(KEY_BODY, message.getBody());
        values.put(KEY_TYPE, message.getType());

        // updating row
        return db.update(TABLE_MESSAGE, values, KEY_ID + " = ?",
                new String[] { String.valueOf(message.getId()) });
    }

    // Deleting single contact
    public void deleteMessage(Message message) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MESSAGE, KEY_ID + " = ?",
                new String[] { String.valueOf(message.getId()) });
        db.close();
    }

    // Deleting all message
    public void deleteAllMessage() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MESSAGE, null, null);
        db.close();
    }


    // Getting contacts Count
    public int getMessagesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_MESSAGE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

}
